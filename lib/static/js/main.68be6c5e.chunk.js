(this.webpackJsonpfrontend = this.webpackJsonpfrontend || []).push([[0], {
  274: function (e, t, a) {},
  275: function (e, t, a) {},
  311: function (e, t) {},
  313: function (e, t) {},
  343: function (e, t) {},
  344: function (e, t) {},
  424: function (e, t) {},
  571: function (e, t, a) {},
  572: function (e, t, a) {
    "use strict";

    a.r(t);
    var r = a(2),
      n = a(42),
      c = a.n(n),
      s = a(8),
      i = a(73),
      o = a(249),
      l = a(248),
      d = a(16),
      j = "PRODUCT_LIST_REQUEST",
      u = "PRODUCT_LIST_SUCCESS",
      b = "PRODUCT_LIST_FAIL",
      h = "PRODUCT_DETAILS_REQUEST",
      p = "PRODUCT_DETAILS_SUCCESS",
      O = "PRODUCT_DETAILS_FAIL",
      x = "PRODUCT_DELETE_REQUEST",
      m = "PRODUCT_DELETE_SUCCESS",
      g = "PRODUCT_DELETE_FAIL",
      f = "PRODUCT_CREATE_REQUEST",
      v = "PRODUCT_CREATE_SUCCESS",
      y = "PRODUCT_CREATE_FAIL",
      S = "PRODUCT_CREATE_RESET",
      N = "PRODUCT_UPDATE_REQUEST",
      E = "PRODUCT_UPDATE_SUCCESS",
      k = "PRODUCT_UPDATE_FAIL",
      C = "PRODUCT_UPDATE_RESET",
      I = "PRODUCT_CREATE_REVIEW_REQUEST",
      _ = "PRODUCT_CREATE_REVIEW_SUCCESS",
      L = "PRODUCT_CREATE_REVIEW_FAIL",
      w = "PRODUCT_CREATE_REVIEW_RESET",
      R = "PRODUCT_TOP_REQUEST",
      T = "PRODUCT_TOP_SUCCESS",
      A = "PRODUCT_TOP_FAIL",
      D = "CATEGORY_LIST_REQUEST",
      P = "CATEGORY_LIST_SUCCESS",
      U = "CATEGORY_LIST_FAIL",
      z = "CATEGORY_DETAILS_REQUEST",
      F = "CATEGORY_DETAILS_SUCCESS",
      G = "CATEGORY_DETAILS_FAIL",
      M = "CATEGORY_DELETE_REQUEST",
      B = "CATEGORY_DELETE_SUCCESS",
      V = "CATEGORY_DELETE_FAIL",
      Y = "CATEGORY_CREATE_REQUEST",
      Q = "CATEGORY_CREATE_SUCCESS",
      W = "CATEGORY_CREATE_FAIL",
      q = "CATEGORY_CREATE_RESET",
      H = "CATEGORY_UPDATE_REQUEST",
      K = "CATEGORY_UPDATE_SUCCESS",
      J = "CATEGORY_UPDATE_FAIL",
      $ = "CATEGORY_UPDATE_RESET",
      X = a(52),
      Z = "CART_ADD_ITEM",
      ee = "CART_CLEAR_ITEMS",
      te = "CART_REMOVE_ITEM",
      ae = "CART_SAVE_SHIPPING_ADDRESS",
      re = "CART_SAVE_PAYMENT_METHOD",
      ne = "EMAIL_SEND_REQUEST",
      ce = "EMAIL_SEND_SUCCESS",
      se = "EMAIL_SEND_FAIL",
      ie = "PDF_REQUEST",
      oe = "PDF_SUCCESS",
      le = "PDF_FAIL",
      de = "USER_LOGIN_REQUEST",
      je = "USER_LOGIN_SUCCESS",
      ue = "USER_LOGIN_FAIL",
      be = "USER_LOGOUT",
      he = "USER_REGISTER_REQUEST",
      pe = "USER_REGISTER_SUCCESS",
      Oe = "USER_REGISTER_FAIL",
      xe = "USER_DETAILS_REQUEST",
      me = "USER_DETAILS_SUCCESS",
      ge = "USER_DETAILS_FAIL",
      fe = "USER_DETAILS_RESET",
      ve = "REFERRAL_DETAILS_REQUEST",
      ye = "REFERRAL_DETAILS_SUCCESS",
      Se = "REFERRAL_DETAILS_FAIL",
      Ne = "USER_UPDATE_PROFILE_REQUEST",
      Ee = "USER_UPDATE_PROFILE_SUCCESS",
      ke = "USER_UPDATE_PROFILE_FAIL",
      Ce = "USER_UPDATE_PROFILE_RESET",
      Ie = "USER_LIST_REQUEST",
      _e = "USER_LIST_SUCCESS",
      Le = "USER_LIST_FAIL",
      we = "USER_LIST_RESET",
      Re = "USER_DELETE_REQUEST",
      Te = "USER_DELETE_SUCCESS",
      Ae = "USER_DELETE_FAIL",
      De = "USER_UPDATE_REQUEST",
      Pe = "USER_UPDATE_SUCCESS",
      Ue = "USER_UPDATE_FAIL",
      ze = "USER_UPDATE_RESET",
      Fe = "ORDER_CREATE_REQUEST",
      Ge = "ORDER_CREATE_SUCCESS",
      Me = "ORDER_CREATE_FAIL",
      Be = "ORDER_CREATE_RESET",
      Ve = "ORDER_DETAILS_REQUEST",
      Ye = "ORDER_DETAILS_SUCCESS",
      Qe = "ORDER_DETAILS_FAIL",
      We = "ORDER_PAY_REQUEST",
      qe = "ORDER_PAY_SUCCESS",
      He = "ORDER_PAY_FAIL",
      Ke = "ORDER_PAY_RESET",
      Je = "ORDER_LIST_MY_REQUEST",
      $e = "ORDER_LIST_MY_SUCCESS",
      Xe = "ORDER_LIST_MY_FAIL",
      Ze = "ORDER_LIST_MY_RESET",
      et = "ORDER_LIST_MONTH_REQUEST",
      tt = "ORDER_LIST_MONTH_SUCCESS",
      at = "ORDER_LIST_MONTH_FAIL",
      rt = "ORDER_LIST_YEAR_REQUEST",
      nt = "ORDER_LIST_YEAR_SUCCESS",
      ct = "ORDER_LIST_YEAR_FAIL",
      st = "ORDER_LIST_YEAR_BY_PRODUCT_REQUEST",
      it = "ORDER_LIST_YEAR_BY_PRODUCT_SUCCESS",
      ot = "ORDER_LIST_YEAR_BY_PRODUCT_FAIL",
      lt = "ORDER_LIST_REQUEST",
      dt = "ORDER_LIST_SUCCESS",
      jt = "ORDER_LIST_FAIL",
      ut = "ORDER_DELIVER_REQUEST",
      bt = "ORDER_DELIVER_SUCCESS",
      ht = "ORDER_DELIVER_FAIL",
      pt = "ORDER_DELIVER_RESET",
      Ot = "ORDER_LAST_NO_REQUEST",
      xt = "ORDER_LAST_NO_SUCCESS",
      mt = "ORDER_LAST_NO_FAIL",
      gt = Object(i.combineReducers)({
        productList: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              products: []
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case j:
              return {
                loading: !0,
                products: []
              };
            case u:
              return {
                loading: !1,
                products: t.payload.products,
                pages: t.payload.pages,
                page: t.payload.page
              };
            case b:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        productDetails: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              product: {
                reviews: []
              }
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case h:
              return Object(d.a)({
                loading: !0
              }, e);
            case p:
              return {
                loading: !1,
                product: t.payload
              };
            case O:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        categoryList: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              categories: []
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case D:
              return {
                loading: !0,
                categories: []
              };
            case P:
              return {
                loading: !1,
                categories: t.payload.categories,
                pages: t.payload.pages,
                page: t.payload.page
              };
            case U:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        categoryDetails: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              category: {}
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case z:
              return Object(d.a)({
                loading: !0
              }, e);
            case F:
              return {
                loading: !1,
                category: t.payload
              };
            case G:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        cart: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              cartItems: [],
              shippingAddress: {}
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Z:
              var a = t.payload,
                r = e.cartItems.find(function (e) {
                  return e.product === a.product;
                });
              return r ? Object(d.a)(Object(d.a)({}, e), {}, {
                cartItems: e.cartItems.map(function (e) {
                  return e.product === r.product ? a : e;
                })
              }) : Object(d.a)(Object(d.a)({}, e), {}, {
                cartItems: [].concat(Object(X.a)(e.cartItems), [a])
              });
            case te:
              return Object(d.a)(Object(d.a)({}, e), {}, {
                cartItems: e.cartItems.filter(function (e) {
                  return e.product !== t.payload;
                })
              });
            case ae:
              return Object(d.a)(Object(d.a)({}, e), {}, {
                shippingAddress: t.payload
              });
            case re:
              return Object(d.a)(Object(d.a)({}, e), {}, {
                paymentMethod: t.payload
              });
            case ee:
              return Object(d.a)(Object(d.a)({}, e), {}, {
                cartItems: []
              });
            default:
              return e;
          }
        },
        userLogin: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case de:
              return {
                loading: !0
              };
            case je:
              return {
                loading: !1,
                userInfo: t.payload
              };
            case ue:
              return {
                loading: !1,
                error: t.payload
              };
            case be:
              return {};
            default:
              return e;
          }
        },
        userRegister: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case he:
              return {
                loading: !0
              };
            case pe:
              return {
                loading: !1,
                userInfo: t.payload
              };
            case Oe:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        userDetails: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              user: {}
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case xe:
              return Object(d.a)(Object(d.a)({}, e), {}, {
                loading: !0
              });
            case me:
              return {
                loading: !1,
                user: t.payload
              };
            case ge:
              return {
                loading: !1,
                error: t.payload
              };
            case fe:
              return {
                user: {}
              };
            default:
              return e;
          }
        },
        referralDetails: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              referral: {}
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case ve:
              return Object(d.a)(Object(d.a)({}, e), {}, {
                loading: !0
              });
            case ye:
              return {
                loading: !1,
                referral: t.payload
              };
            case Se:
              return {
                loading: !1,
                error: t.payload
              };
            case "REFERRAL_DETAILS_RESET":
              return {
                referral: {}
              };
            default:
              return e;
          }
        },
        userUpdateProfile: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Ne:
              return {
                loading: !0
              };
            case Ee:
              return {
                loading: !1,
                success: !0,
                userInfo: t.payload
              };
            case ke:
              return {
                loading: !1,
                error: t.payload
              };
            case Ce:
              return {};
            default:
              return e;
          }
        },
        userList: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              users: []
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Ie:
              return {
                loading: !0
              };
            case _e:
              return {
                loading: !1,
                users: t.payload
              };
            case Le:
              return {
                loading: !1,
                error: t.payload
              };
            case we:
              return {
                users: []
              };
            default:
              return e;
          }
        },
        top5Users: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              users: []
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Ie:
              return {
                loading: !0
              };
            case _e:
              return {
                loading: !1,
                users: t.payload
              };
            case Le:
              return {
                loading: !1,
                error: t.payload
              };
            case we:
              return {
                users: []
              };
            default:
              return e;
          }
        },
        userUpdate: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              user: {}
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case De:
              return {
                loading: !0
              };
            case Pe:
              return {
                loading: !1,
                success: !0
              };
            case Ue:
              return {
                loading: !1,
                error: t.payload
              };
            case ze:
              return {
                user: {}
              };
            default:
              return e;
          }
        },
        userDelete: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Re:
              return {
                loading: !0
              };
            case Te:
              return {
                loading: !1,
                success: !0
              };
            case Ae:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        orderCreate: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Fe:
              return {
                loading: !0
              };
            case Ge:
              return {
                loading: !1,
                success: !0,
                order: t.payload
              };
            case Me:
              return {
                loading: !1,
                error: t.payload
              };
            case Be:
              return {};
            default:
              return e;
          }
        },
        orderDetails: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              loading: !0,
              orderItems: [],
              shippingAddress: {}
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Ve:
              return Object(d.a)(Object(d.a)({}, e), {}, {
                loading: !0
              });
            case Ye:
              return {
                loading: !1,
                order: t.payload
              };
            case Qe:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        orderPay: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case We:
              return {
                loading: !0
              };
            case qe:
              return {
                loading: !1,
                success: !0
              };
            case He:
              return {
                loading: !1,
                error: t.payload
              };
            case Ke:
              return {};
            default:
              return e;
          }
        },
        orderListMy: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              orders: []
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Je:
              return {
                loading: !0
              };
            case $e:
              return {
                loading: !1,
                orders: t.payload
              };
            case Xe:
              return {
                loading: !1,
                error: t.payload
              };
            case Ze:
              return {
                orders: []
              };
            default:
              return e;
          }
        },
        orderListUser: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              orders: []
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case "ORDER_LIST_REQUEST":
              return {
                loading: !0
              };
            case "ORDER_LIST_SUCCESS":
              return {
                loading: !1,
                orders: t.payload
              };
            case "ORDER_LIST_FAIL":
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        orderThisMonth: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              total: {
                sum: -1,
                total: 0
              }
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case et:
              return {
                loading: !0
              };
            case tt:
              return {
                loading: !1,
                total: t.payload
              };
            case at:
              return {
                loading: !1,
                error: t.payload
              };
            case "ORDER_LIST_MONTH_RESET":
              return {
                total: {
                  sum: 0,
                  total: 0
                }
              };
            default:
              return e;
          }
        },
        orderThisYear: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              items: [{
                sum: -1,
                total: 0
              }]
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case rt:
              return {
                loading: !0
              };
            case nt:
              return {
                loading: !1,
                total: t.payload
              };
            case ct:
              return {
                loading: !1,
                error: t.payload
              };
            case "ORDER_LIST_YEAR_RESET":
              return {
                total: {
                  sum: 0,
                  total: 0
                }
              };
            default:
              return e;
          }
        },
        orderThisYearByProduct: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              items: []
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case st:
              return {
                loading: !0
              };
            case it:
              return {
                loading: !1,
                items: t.payload
              };
            case ot:
              return {
                loading: !1,
                error: t.payload
              };
            case "ORDER_LIST_YEAR_BY_PRODUCT_RESET":
              return {
                total: []
              };
            default:
              return e;
          }
        },
        orderDeliver: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case ut:
              return {
                loading: !0
              };
            case bt:
              return {
                loading: !1,
                success: !0
              };
            case ht:
              return {
                loading: !1,
                error: t.payload
              };
            case pt:
              return {};
            default:
              return e;
          }
        },
        orderLastNo: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              orderNo: ""
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Ot:
              return {
                loading: !0
              };
            case xt:
              return {
                loading: !1,
                orderNo: t.payload
              };
            case mt:
              return {
                loading: !1,
                error: t.payload
              };
            case "ORDER_LAST_NO_RESET":
              return {
                orderNo: ""
              };
            default:
              return e;
          }
        },
        productDelete: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case x:
              return {
                loading: !0
              };
            case m:
              return {
                loading: !1,
                success: !0
              };
            case g:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        productCreate: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case f:
              return {
                loading: !0
              };
            case v:
              return {
                loading: !1,
                success: !0,
                product: t.payload
              };
            case y:
              return {
                loading: !1,
                error: t.payload
              };
            case S:
              return {};
            default:
              return e;
          }
        },
        productUpdate: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              product: {}
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case N:
              return {
                loading: !0
              };
            case E:
              return {
                loading: !1,
                success: !0,
                product: t.payload
              };
            case k:
              return {
                loading: !1,
                error: t.payload
              };
            case C:
              return {
                product: {}
              };
            default:
              return e;
          }
        },
        categoryDelete: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case M:
              return {
                loading: !0
              };
            case B:
              return {
                loading: !1,
                success: !0
              };
            case V:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        categoryCreate: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case Y:
              return {
                loading: !0
              };
            case Q:
              return {
                loading: !1,
                success: !0,
                category: t.payload
              };
            case W:
              return {
                loading: !1,
                error: t.payload
              };
            case q:
              return {};
            default:
              return e;
          }
        },
        categoryUpdate: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              category: {}
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case H:
              return {
                loading: !0
              };
            case K:
              return {
                loading: !1,
                success: !0,
                category: t.payload
              };
            case J:
              return {
                loading: !1,
                error: t.payload
              };
            case $:
              return {
                category: {}
              };
            default:
              return e;
          }
        },
        orderList: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              orders: []
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case lt:
              return {
                loading: !0
              };
            case dt:
              return {
                loading: !1,
                orders: t.payload
              };
            case jt:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        productReviewCreate: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {},
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case I:
              return {
                loading: !0
              };
            case _:
              return {
                loading: !1,
                success: !0
              };
            case L:
              return {
                loading: !1,
                error: t.payload
              };
            case w:
              return {};
            default:
              return e;
          }
        },
        productTopRated: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              products: []
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case R:
              return {
                loading: !0,
                products: []
              };
            case T:
              return {
                loading: !1,
                products: t.payload
              };
            case A:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        emailSend: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              msg: ""
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case ne:
              return {
                loading: !0,
                msg: ""
              };
            case ce:
              return {
                loading: !1,
                msg: t.payload.msg
              };
            case se:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        },
        createPdf: function () {
          var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {
              msg: ""
            },
            t = arguments.length > 1 ? arguments[1] : void 0;
          switch (t.type) {
            case ie:
              return {
                loading: !0,
                msg: ""
              };
            case oe:
              return {
                loading: !1,
                msg: t.payload.msg
              };
            case le:
              return {
                loading: !1,
                error: t.payload
              };
            default:
              return e;
          }
        }
      }),
      ft = localStorage.getItem("cartItems") ? JSON.parse(localStorage.getItem("cartItems")) : [],
      vt = localStorage.getItem("userInfo") ? JSON.parse(localStorage.getItem("userInfo")) : null,
      yt = {
        cart: {
          cartItems: ft,
          shippingAddress: localStorage.getItem("shippingAddress") ? JSON.parse(localStorage.getItem("shippingAddress")) : {},
          paymentMethod: localStorage.getItem("paymentMethod") ? JSON.parse(localStorage.getItem("paymentMethod")) : {}
        },
        userLogin: {
          userInfo: vt
        }
      },
      St = [o.a],
      Nt = Object(i.createStore)(gt, yt, Object(l.composeWithDevTools)(i.applyMiddleware.apply(void 0, St))),
      Et = (a(274), a(275), a(17)),
      kt = a(25),
      Ct = a(579),
      It = a(19),
      _t = a(587),
      Lt = a(589),
      wt = a(584),
      Rt = a(10),
      Tt = a(585),
      At = a(252),
      Dt = a(1),
      Pt = a(12),
      Ut = a(15),
      zt = a(18),
      Ft = a.n(zt),
      Gt = function () {
        return function (e) {
          localStorage.removeItem("userInfo"), localStorage.removeItem("cartItems"), localStorage.removeItem("shippingAddress"), localStorage.removeItem("paymentMethod"), e({
            type: be
          }), e({
            type: fe
          }), e({
            type: Ze
          }), e({
            type: we
          }), document.location.href = "/login";
        };
      },
      Mt = function (e) {
        return function () {
          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
            var n, c, s, i, o;
            return Object(Pt.a)().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return t.prev = 0, a({
                    type: xe
                  }), n = r(), c = n.userLogin.userInfo, s = {
                    headers: {
                      "Content-Type": "application/json",
                      Authorization: "Bearer ".concat(c.token)
                    }
                  }, t.next = 6, Ft.a.get("/api/users/".concat(e), s);
                case 6:
                  i = t.sent, o = i.data, a({
                    type: me,
                    payload: o
                  }), t.next = 14;
                  break;
                case 11:
                  t.prev = 11, t.t0 = t.catch(0), a({
                    type: ge,
                    payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                  });
                case 14:
                case "end":
                  return t.stop();
              }
            }, t, null, [[0, 11]]);
          }));
          return function (e, a) {
            return t.apply(this, arguments);
          };
        }();
      },
      Bt = function (e) {
        return function () {
          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
            var n, c, s, i, o;
            return Object(Pt.a)().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return t.prev = 0, a({
                    type: ve
                  }), n = r(), c = n.userLogin.userInfo, s = {
                    headers: {
                      "Content-Type": "application/json",
                      Authorization: "Bearer ".concat(c.token)
                    }
                  }, t.next = 6, Ft.a.get("/api/users/profile/".concat(e), s);
                case 6:
                  i = t.sent, o = i.data, a({
                    type: ye,
                    payload: o
                  }), t.next = 14;
                  break;
                case 11:
                  t.prev = 11, t.t0 = t.catch(0), a({
                    type: Se,
                    payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                  });
                case 14:
                case "end":
                  return t.stop();
              }
            }, t, null, [[0, 11]]);
          }));
          return function (e, a) {
            return t.apply(this, arguments);
          };
        }();
      },
      Vt = function (e) {
        return function () {
          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
            var n, c, s, i, o, l;
            return Object(Pt.a)().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return t.prev = 0, a({
                    type: De
                  }), n = r(), c = n.userLogin.userInfo, s = {
                    headers: {
                      "Content-Type": "application/json",
                      Authorization: "Bearer ".concat(c.token)
                    }
                  }, t.next = 6, Ft.a.put("/api/users/".concat(e._id), e, s);
                case 6:
                  i = t.sent, o = i.data, a({
                    type: Pe
                  }), a({
                    type: me,
                    payload: o
                  }), a({
                    type: fe
                  }), t.next = 18;
                  break;
                case 13:
                  t.prev = 13, t.t0 = t.catch(0), "Not authorized, token failed" === (l = t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message) && a(Gt()), a({
                    type: Ue,
                    payload: l
                  });
                case 18:
                case "end":
                  return t.stop();
              }
            }, t, null, [[0, 13]]);
          }));
          return function (e, a) {
            return t.apply(this, arguments);
          };
        }();
      },
      Yt = function () {
        var e = Object(s.b)(),
          t = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo;
        return Object(Dt.jsx)("header", {
          children: Object(Dt.jsx)(_t.a, {
            className: "text-uppercase",
            bg: "primary",
            variant: "dark",
            expand: "lg",
            collapseOnSelect: !0,
            children: Object(Dt.jsxs)(Ct.a, {
              children: [Object(Dt.jsx)(It.LinkContainer, {
                to: "/",
                children: Object(Dt.jsx)(_t.a.Brand, {
                  children: "MAPO"
                })
              }), Object(Dt.jsx)(_t.a.Toggle, {
                "aria-controls": "basic-navbar-nav"
              }), Object(Dt.jsx)(_t.a.Collapse, {
                id: "basic-navbar-nav",
                children: Object(Dt.jsxs)(Lt.a, {
                  className: "ml-auto",
                  children: [Object(Dt.jsx)(It.LinkContainer, {
                    to: "/products",
                    children: Object(Dt.jsx)(Lt.a.Link, {
                      children: "Proizvodi"
                    })
                  }), Object(Dt.jsx)(It.LinkContainer, {
                    to: "/cart",
                    children: Object(Dt.jsxs)(Lt.a.Link, {
                      children: [Object(Dt.jsx)("i", {
                        className: "fas fa-shopping-cart"
                      }), " Korpa"]
                    })
                  }), Object(Dt.jsx)(It.LinkContainer, {
                    to: "/about-us",
                    children: Object(Dt.jsx)(Lt.a.Link, {
                      children: "O nama"
                    })
                  }), Object(Dt.jsx)(It.LinkContainer, {
                    to: "/contact-us",
                    children: Object(Dt.jsx)(Lt.a.Link, {
                      children: "Kontakt"
                    })
                  }), t ? Object(Dt.jsxs)(wt.a, {
                    title: t.name,
                    id: "username",
                    children: [Object(Dt.jsx)(It.LinkContainer, {
                      to: "/profile",
                      children: Object(Dt.jsx)(wt.a.Item, {
                        children: "Profil"
                      })
                    }), Object(Dt.jsx)(wt.a.Item, {
                      onClick: function () {
                        e(Gt());
                      },
                      children: "Odjava"
                    })]
                  }) : Object(Dt.jsx)(It.LinkContainer, {
                    to: "/login",
                    children: Object(Dt.jsxs)(Lt.a.Link, {
                      children: [Object(Dt.jsx)("i", {
                        className: "fas fa-user"
                      }), " Prijava"]
                    })
                  }), t && t.isAdmin && Object(Dt.jsx)(It.LinkContainer, {
                    to: "/admin",
                    children: Object(Dt.jsx)(Lt.a.Link, {
                      children: "Admin"
                    })
                  })]
                })
              })]
            })
          })
        });
      },
      Qt = a(580),
      Wt = a(251),
      qt = function () {
        return Object(Dt.jsx)("footer", {
          children: Object(Dt.jsx)(Ct.a, {
            children: Object(Dt.jsx)(Qt.a, {
              children: Object(Dt.jsx)(Wt.a, {
                className: "text-center py-3",
                children: "Copyright \xa9 MAD Software Solutions"
              })
            })
          })
        });
      },
      Ht = a(590),
      Kt = function (e) {
        var t = e.product;
        return Object(Dt.jsxs)(Ht.a, {
          className: "my-3 p-3 rounded",
          children: [Object(Dt.jsx)(Et.Link, {
            to: "/product/".concat(t._id),
            children: Object(Dt.jsx)(Ht.a.Img, {
              src: t.image,
              variant: "top"
            })
          }), Object(Dt.jsxs)(Ht.a.Body, {
            children: [Object(Dt.jsx)(Et.Link, {
              to: "/product/".concat(t._id),
              children: Object(Dt.jsx)(Ht.a.Title, {
                as: "div",
                children: Object(Dt.jsx)("strong", {
                  children: t.name
                })
              })
            }), Object(Dt.jsx)(Ht.a.Text, {
              as: "div"
            }), Object(Dt.jsxs)(Ht.a.Text, {
              as: "div",
              className: "price-marker",
              children: [t.price, " din"]
            })]
          })]
        });
      },
      Jt = function (e) {
        var t = e.category;
        return Object(Dt.jsxs)(Ht.a, {
          className: "my-3 p-3 rounded",
          children: [Object(Dt.jsx)(Et.Link, {
            to: "/category/".concat(t._id),
            children: Object(Dt.jsx)(Ht.a.Img, {
              src: t.image,
              variant: "top"
            })
          }), Object(Dt.jsx)(Ht.a.Body, {
            children: Object(Dt.jsx)(Et.Link, {
              to: "/category/".concat(t._id),
              children: Object(Dt.jsx)(Ht.a.Title, {
                as: "div",
                children: Object(Dt.jsx)("strong", {
                  children: t.name
                })
              })
            })
          })]
        });
      },
      $t = a(588),
      Xt = function (e) {
        var t = e.variant,
          a = e.children,
          r = e.visible;
        return Object(Dt.jsx)($t.a, {
          visible: r,
          variant: t,
          children: a
        });
      };
    Xt.defaultProps = {
      variant: "info"
    };
    var Zt = Xt,
      ea = a(581),
      ta = function () {
        return Object(Dt.jsx)(ea.a, {
          animation: "border",
          role: "status",
          style: {
            width: "100px",
            height: "100px",
            margin: "auto",
            display: "block"
          },
          children: Object(Dt.jsx)("span", {
            className: "sr-only",
            children: "U\u010ditavanje..."
          })
        });
      },
      aa = a(591),
      ra = function (e) {
        var t = e.pages,
          a = e.page,
          r = e.isAdmin,
          n = void 0 !== r && r,
          c = e.keyword,
          s = void 0 === c ? "" : c,
          i = e.category,
          o = i && void 0 != i;
        return console.log(o), t > 1 && Object(Dt.jsx)(aa.a, {
          children: Object(X.a)(Array(t).keys()).map(function (e) {
            return Object(Dt.jsx)(It.LinkContainer, {
              to: n ? "/admin/productlist/".concat(e + 1) : o ? "/category/".concat(i, "/page/").concat(e + 1) : s ? "/search/".concat(s, "/page/").concat(e + 1) : "/page/".concat(e + 1),
              children: Object(Dt.jsx)(aa.a.Item, {
                active: e + 1 === a,
                children: e + 1
              })
            }, e + 1);
          })
        });
      },
      na = a(586),
      ca = a(582),
      sa = function () {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "",
          t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "",
          a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : "";
        return function () {
          var r = Object(Ut.a)(Object(Pt.a)().mark(function r(n) {
            var c, s;
            return Object(Pt.a)().wrap(function (r) {
              for (;;) switch (r.prev = r.next) {
                case 0:
                  return r.prev = 0, n({
                    type: j
                  }), r.next = 4, Ft.a.get("/api/products?keyword=".concat(e, "&pageNumber=").concat(t, "&id=").concat(a));
                case 4:
                  c = r.sent, s = c.data, console.log(s), n({
                    type: u,
                    payload: s
                  }), r.next = 13;
                  break;
                case 10:
                  r.prev = 10, r.t0 = r.catch(0), n({
                    type: b,
                    payload: r.t0.response && r.t0.response.data.message ? r.t0.response.data.message : r.t0.message
                  });
                case 13:
                case "end":
                  return r.stop();
              }
            }, r, null, [[0, 10]]);
          }));
          return function (e) {
            return r.apply(this, arguments);
          };
        }();
      },
      ia = function (e) {
        return function () {
          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a) {
            var r, n;
            return Object(Pt.a)().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return t.prev = 0, a({
                    type: h
                  }), t.next = 4, Ft.a.get("/api/products/".concat(e));
                case 4:
                  r = t.sent, n = r.data, a({
                    type: p,
                    payload: n
                  }), t.next = 12;
                  break;
                case 9:
                  t.prev = 9, t.t0 = t.catch(0), a({
                    type: O,
                    payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                  });
                case 12:
                case "end":
                  return t.stop();
              }
            }, t, null, [[0, 9]]);
          }));
          return function (e) {
            return t.apply(this, arguments);
          };
        }();
      },
      oa = function () {
        var e = Object(s.b)(),
          t = Object(s.c)(function (e) {
            return e.productTopRated;
          }),
          a = t.loading,
          n = t.error,
          c = t.products;
        return Object(r.useEffect)(function () {
          e(function () {
            var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t) {
              var a, r;
              return Object(Pt.a)().wrap(function (e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    return e.prev = 0, t({
                      type: R
                    }), e.next = 4, Ft.a.get("/api/products/top");
                  case 4:
                    a = e.sent, r = a.data, t({
                      type: T,
                      payload: r
                    }), e.next = 12;
                    break;
                  case 9:
                    e.prev = 9, e.t0 = e.catch(0), t({
                      type: A,
                      payload: e.t0.response && e.t0.response.data.message ? e.t0.response.data.message : e.t0.message
                    });
                  case 12:
                  case "end":
                    return e.stop();
                }
              }, e, null, [[0, 9]]);
            }));
            return function (t) {
              return e.apply(this, arguments);
            };
          }());
        }, [e]), a ? Object(Dt.jsx)(ta, {}) : n ? Object(Dt.jsx)(Zt, {
          variant: "danger",
          children: n
        }) : Object(Dt.jsx)(na.a, {
          pause: "hover",
          className: "bg-light",
          children: c.map(function (e) {
            return Object(Dt.jsx)(na.a.Item, {
              interval: 2e3,
              children: Object(Dt.jsxs)(Et.Link, {
                to: "/product/".concat(e._id),
                children: [Object(Dt.jsx)(ca.a, {
                  src: e.image,
                  alt: e.name,
                  fluid: !0
                }), Object(Dt.jsx)(na.a.Caption, {
                  className: "carousel-caption",
                  children: Object(Dt.jsxs)("h2", {
                    children: [e.name, " (", e.price, " din)"]
                  })
                })]
              })
            }, e._id);
          })
        });
      },
      la = a(254),
      da = function (e) {
        var t = e.title,
          a = e.description,
          r = e.keywords,
          n = e.author;
        return Object(Dt.jsx)(Dt.Fragment, {
          children: Object(Dt.jsxs)(la.a, {
            children: [Object(Dt.jsx)("title", {
              children: t
            }), Object(Dt.jsx)("meta", {
              name: "description",
              content: a
            }), Object(Dt.jsx)("meta", {
              name: "keywords",
              content: r
            }), Object(Dt.jsx)("meta", {
              name: "author",
              content: n
            })]
          })
        });
      };
    da.defaultProps = {
      title: "MAPO Prodavnica | Po\u010detna",
      description: "Best products at an affordable price",
      keywords: "buy, iphone, electronics, South, Africa",
      author: "MAD Software Solutions"
    };
    var ja = da,
      ua = function () {
        var e = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : "";
        return function () {
          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a) {
            var r, n;
            return Object(Pt.a)().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return t.prev = 0, a({
                    type: D
                  }), t.next = 4, Ft.a.get("/api/categories?keyword=".concat(e));
                case 4:
                  r = t.sent, n = r.data, a({
                    type: P,
                    payload: n
                  }), t.next = 12;
                  break;
                case 9:
                  t.prev = 9, t.t0 = t.catch(0), a({
                    type: U,
                    payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                  });
                case 12:
                case "end":
                  return t.stop();
              }
            }, t, null, [[0, 9]]);
          }));
          return function (e) {
            return t.apply(this, arguments);
          };
        }();
      },
      ba = function (e) {
        return function () {
          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a) {
            var r, n;
            return Object(Pt.a)().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return t.prev = 0, a({
                    type: z
                  }), t.next = 4, Ft.a.get("/api/categories/".concat(e));
                case 4:
                  r = t.sent, n = r.data, a({
                    type: F,
                    payload: n
                  }), t.next = 12;
                  break;
                case 9:
                  t.prev = 9, t.t0 = t.catch(0), a({
                    type: G,
                    payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                  });
                case 12:
                case "end":
                  return t.stop();
              }
            }, t, null, [[0, 9]]);
          }));
          return function (e) {
            return t.apply(this, arguments);
          };
        }();
      },
      ha = function (e) {
        var t = e.match,
          a = t.params.keyword,
          n = t.params.pageNumber || 1,
          c = Object(s.b)(),
          i = Object(s.c)(function (e) {
            return e.productList;
          }),
          o = i.loading,
          l = i.error,
          d = i.products,
          j = i.page,
          u = i.pages,
          b = Object(s.c)(function (e) {
            return e.categoryList;
          }),
          h = b.loading,
          p = b.error,
          O = b.categories;
        return Object(r.useEffect)(function () {
          c(sa(a, n)), c(ua(a));
        }, [c, a, n]), Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(ja, {}), a ? Object(Dt.jsx)(Et.Link, {
            className: "btn btn-light",
            to: "/",
            children: "Nazad"
          }) : Object(Dt.jsx)(oa, {}), Object(Dt.jsxs)("div", {
            className: "container center",
            children: [Object(Dt.jsx)("h2", {
              className: "title",
              children: "Najnoviji proizvodi"
            }), o ? Object(Dt.jsx)(ta, {}) : l ? Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: l
            }) : Object(Dt.jsxs)(Dt.Fragment, {
              children: [Object(Dt.jsx)(Qt.a, {
                children: d.map(function (e) {
                  return Object(Dt.jsx)(Wt.a, {
                    sm: 12,
                    md: "6",
                    lg: 4,
                    xl: 3,
                    children: Object(Dt.jsx)(Kt, {
                      product: e
                    })
                  }, e._id);
                })
              }), Object(Dt.jsx)(ra, {
                pages: u,
                page: j,
                keyword: a || ""
              })]
            })]
          }), Object(Dt.jsxs)("div", {
            className: "container center",
            children: [Object(Dt.jsx)("h2", {
              className: "title",
              children: "Pregled po kategorijama"
            }), h ? Object(Dt.jsx)(ta, {}) : p ? Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: p
            }) : Object(Dt.jsx)(Dt.Fragment, {
              children: Object(Dt.jsx)(Qt.a, {
                children: O.map(function (e) {
                  return Object(Dt.jsx)(Wt.a, {
                    sm: 12,
                    md: "6",
                    lg: 4,
                    xl: 3,
                    children: Object(Dt.jsx)(Jt, {
                      category: e
                    })
                  }, e._id);
                })
              })
            })]
          })]
        });
      },
      pa = a(592),
      Oa = function (e) {
        var t = e.value,
          a = e.text,
          r = e.color;
        return Object(Dt.jsxs)("div", {
          className: "rating",
          children: [Object(Dt.jsx)("span", {
            children: Object(Dt.jsx)("i", {
              style: {
                color: r
              },
              className: t >= 1 ? "fas fa-star" : t >= .5 ? "fas fa-star-half-alt" : "far fa-star"
            })
          }), Object(Dt.jsx)("span", {
            children: Object(Dt.jsx)("i", {
              style: {
                color: r
              },
              className: t >= 2 ? "fas fa-star" : t >= 1.5 ? "fas fa-star-half-alt" : "far fa-star"
            })
          }), Object(Dt.jsx)("span", {
            children: Object(Dt.jsx)("i", {
              style: {
                color: r
              },
              className: t >= 3 ? "fas fa-star" : t >= 2.5 ? "fas fa-star-half-alt" : "far fa-star"
            })
          }), Object(Dt.jsx)("span", {
            children: Object(Dt.jsx)("i", {
              style: {
                color: r
              },
              className: t >= 4 ? "fas fa-star" : t >= 3.5 ? "fas fa-star-half-alt" : "far fa-star"
            })
          }), Object(Dt.jsx)("span", {
            children: Object(Dt.jsx)("i", {
              style: {
                color: r
              },
              className: t >= 5 ? "fas fa-star" : t >= 4.5 ? "fas fa-star-half-alt" : "far fa-star"
            })
          }), Object(Dt.jsx)("span", {
            children: a && a
          })]
        });
      };
    Oa.defaultProps = {
      color: "#f8e825"
    };
    var xa = Oa,
      ma = function (e) {
        var t = e.history,
          a = e.match,
          n = Object(r.useState)(1),
          c = Object(Rt.a)(n, 2),
          i = c[0],
          o = c[1],
          l = Object(r.useState)(0),
          d = Object(Rt.a)(l, 2),
          j = d[0],
          u = d[1],
          b = Object(r.useState)(""),
          h = Object(Rt.a)(b, 2),
          p = h[0],
          O = h[1],
          x = Object(s.b)(),
          m = Object(s.c)(function (e) {
            return e.productDetails;
          }),
          g = m.loading,
          f = m.error,
          v = m.product,
          y = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          S = Object(s.c)(function (e) {
            return e.productReviewCreate;
          }),
          N = S.success,
          E = S.loading,
          k = S.error;
        Object(r.useEffect)(function () {
          N && (u(0), O(""), x(ia(a.params.id)), x({
            type: w
          })), v._id && v._id === a.params.id || (x(ia(a.params.id)), x({
            type: w
          }));
        }, [x, a, N, v._id]);
        return Object(Dt.jsx)(Dt.Fragment, {
          children: Object(Dt.jsxs)("div", {
            className: "container",
            children: [Object(Dt.jsx)(Et.Link, {
              className: "btn btn-light my-3",
              to: "/",
              children: "Nazad"
            }), g ? Object(Dt.jsx)(ta, {}) : f ? Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: f
            }) : Object(Dt.jsxs)(Dt.Fragment, {
              children: [Object(Dt.jsx)(ja, {
                title: v.name
              }), Object(Dt.jsxs)(Qt.a, {
                children: [Object(Dt.jsx)(Wt.a, {
                  md: "6",
                  children: Object(Dt.jsx)(ca.a, {
                    src: v.image,
                    alt: v.name,
                    fluid: !0
                  })
                }), Object(Dt.jsx)(Wt.a, {
                  md: "3",
                  children: Object(Dt.jsxs)(pa.a, {
                    variant: "flush",
                    children: [Object(Dt.jsx)(pa.a.Item, {
                      children: Object(Dt.jsx)("h3", {
                        children: v.name
                      })
                    }), Object(Dt.jsxs)(pa.a.Item, {
                      children: ["Cena: ", v.price, " din"]
                    }), Object(Dt.jsxs)(pa.a.Item, {
                      children: ["Opis: ", v.description]
                    })]
                  })
                }), Object(Dt.jsx)(Wt.a, {
                  md: "3",
                  children: Object(Dt.jsx)(Ht.a, {
                    children: Object(Dt.jsxs)(pa.a, {
                      variant: "flush",
                      children: [Object(Dt.jsx)(pa.a.Item, {
                        children: Object(Dt.jsxs)(Qt.a, {
                          children: [Object(Dt.jsx)(Wt.a, {
                            children: "Cena:"
                          }), Object(Dt.jsx)(Wt.a, {
                            children: Object(Dt.jsxs)("strong", {
                              children: [v.price, " din"]
                            })
                          })]
                        })
                      }), Object(Dt.jsx)(pa.a.Item, {
                        children: Object(Dt.jsxs)(Qt.a, {
                          children: [Object(Dt.jsx)(Wt.a, {
                            children: "Status:"
                          }), Object(Dt.jsx)(Wt.a, {
                            children: v.countInStock > 0 ? "Na stanju" : "Nema na stanju"
                          })]
                        })
                      }), v.countInStock > 0 && Object(Dt.jsx)(pa.a.Item, {
                        children: Object(Dt.jsxs)(Qt.a, {
                          children: [Object(Dt.jsx)(Wt.a, {
                            children: "Koli\u010dina"
                          }), Object(Dt.jsx)(Wt.a, {
                            children: Object(Dt.jsx)(Tt.a.Control, {
                              as: "select",
                              value: i,
                              onChange: function (e) {
                                return o(e.target.value);
                              },
                              children: Object(X.a)(Array(v.countInStock).keys()).map(function (e) {
                                return Object(Dt.jsx)("option", {
                                  value: e + 1,
                                  children: e + 1
                                }, e + 1);
                              })
                            })
                          })]
                        })
                      }), Object(Dt.jsx)(pa.a.Item, {
                        children: v.countInStock > 0 ? Object(Dt.jsxs)(At.a, {
                          onClick: function () {
                            t.push("/cart/".concat(a.params.id, "?qty=").concat(i));
                          },
                          className: "btn-block",
                          type: "button",
                          children: [Object(Dt.jsx)("i", {
                            className: "fas fa-plus"
                          }), Object(Dt.jsx)("span", {
                            className: "plus-sign-margin",
                            children: Object(Dt.jsx)("i", {
                              className: "fas fa-shopping-cart"
                            })
                          }), "Dodaj u korpu"]
                        }) : Object(Dt.jsx)(At.a, {
                          className: "btn-block",
                          type: "button",
                          disabled: 0 === v.countInStock,
                          children: "Rasprodato"
                        })
                      })]
                    })
                  })
                })]
              }), Object(Dt.jsx)(Qt.a, {
                children: Object(Dt.jsxs)(Wt.a, {
                  style: {
                    display: "none"
                  },
                  md: 6,
                  children: [Object(Dt.jsx)("h2", {
                    children: "Reviews"
                  }), 0 === v.reviews.length && Object(Dt.jsx)(Zt, {
                    children: "No Reviews"
                  }), Object(Dt.jsxs)(pa.a, {
                    variant: "flush",
                    children: [v.reviews.map(function (e) {
                      return Object(Dt.jsxs)(pa.a.Item, {
                        children: [Object(Dt.jsx)("strong", {
                          children: e.name
                        }), Object(Dt.jsx)(xa, {
                          value: e.rating
                        }), Object(Dt.jsx)("p", {
                          children: e.createdAt.substring(0, 10)
                        }), Object(Dt.jsx)("p", {
                          children: e.comment
                        })]
                      }, e._id);
                    }), Object(Dt.jsxs)(pa.a.Item, {
                      children: [Object(Dt.jsx)("h2", {
                        children: "Write a Customer Review"
                      }), N && Object(Dt.jsx)(Zt, {
                        variant: "success",
                        children: "Review submitted successfully"
                      }), E && Object(Dt.jsx)(ta, {}), k && Object(Dt.jsx)(Zt, {
                        variant: "danger",
                        children: k
                      }), y ? Object(Dt.jsxs)(Tt.a, {
                        className: "push-to-right",
                        onSubmit: function (e) {
                          var t, r;
                          e.preventDefault(), x((t = a.params.id, r = {
                            rating: j,
                            comment: p
                          }, function () {
                            var e = Object(Ut.a)(Object(Pt.a)().mark(function e(a, n) {
                              var c, s, i;
                              return Object(Pt.a)().wrap(function (e) {
                                for (;;) switch (e.prev = e.next) {
                                  case 0:
                                    return e.prev = 0, a({
                                      type: I
                                    }), c = n(), s = c.userLogin.userInfo, i = {
                                      headers: {
                                        "Content-Type": "application/json",
                                        Authorization: "Bearer ".concat(s.token)
                                      }
                                    }, e.next = 6, Ft.a.post("/api/products/".concat(t, "/reviews"), r, i);
                                  case 6:
                                    a({
                                      type: _
                                    }), e.next = 12;
                                    break;
                                  case 9:
                                    e.prev = 9, e.t0 = e.catch(0), a({
                                      type: L,
                                      payload: e.t0.response && e.t0.response.data.message ? e.t0.response.data.message : e.t0.message
                                    });
                                  case 12:
                                  case "end":
                                    return e.stop();
                                }
                              }, e, null, [[0, 9]]);
                            }));
                            return function (t, a) {
                              return e.apply(this, arguments);
                            };
                          }()));
                        },
                        children: [Object(Dt.jsxs)(Tt.a.Group, {
                          controlId: "rating",
                          children: [Object(Dt.jsx)(Tt.a.Label, {
                            children: "Rating"
                          }), Object(Dt.jsxs)(Tt.a.Control, {
                            as: "select",
                            value: j,
                            onChange: function (e) {
                              return u(e.target.value);
                            },
                            children: [Object(Dt.jsx)("option", {
                              value: "",
                              children: "Select..."
                            }), Object(Dt.jsx)("option", {
                              value: "1",
                              children: "1 - Poor"
                            }), Object(Dt.jsx)("option", {
                              value: "2",
                              children: "2 - Fair"
                            }), Object(Dt.jsx)("option", {
                              value: "3",
                              children: "3 - Good"
                            }), Object(Dt.jsx)("option", {
                              value: "4",
                              children: "4 - Very Good"
                            }), Object(Dt.jsx)("option", {
                              value: "5",
                              children: "5 - Excellent"
                            })]
                          })]
                        }), Object(Dt.jsx)(Tt.a.Group, {
                          controlId: "comment",
                          children: Object(Dt.jsx)(Tt.a.Control, {
                            as: "textarea",
                            required: !0,
                            row: "3",
                            onChange: function (e) {
                              return O(e.target.value);
                            }
                          })
                        }), Object(Dt.jsx)(At.a, {
                          type: "submit",
                          disabled: E,
                          variant: "primary",
                          children: "Submit"
                        })]
                      }) : Object(Dt.jsxs)(Zt, {
                        children: ["Please ", Object(Dt.jsx)(Et.Link, {
                          to: "/login",
                          children: "sign in"
                        }), " to write a review"]
                      })]
                    })]
                  })]
                })
              })]
            })]
          })
        });
      },
      ga = function (e, t) {
        return function () {
          var a = Object(Ut.a)(Object(Pt.a)().mark(function a(r, n) {
            var c, s;
            return Object(Pt.a)().wrap(function (a) {
              for (;;) switch (a.prev = a.next) {
                case 0:
                  return a.next = 2, Ft.a.get("/api/products/".concat(e));
                case 2:
                  c = a.sent, s = c.data, r({
                    type: Z,
                    payload: {
                      product: s._id,
                      name: s.name,
                      image: s.image,
                      price: s.price,
                      countInStock: s.countInStock,
                      qty: t
                    }
                  }), localStorage.setItem("cartItems", JSON.stringify(n().cart.cartItems));
                case 6:
                case "end":
                  return a.stop();
              }
            }, a);
          }));
          return function (e, t) {
            return a.apply(this, arguments);
          };
        }();
      },
      fa = function (e) {
        var t = e.match,
          a = e.location,
          n = e.history,
          c = t.params.id,
          i = a.search ? Number(a.search.split("=")[1]) : 1,
          o = Object(s.b)(),
          l = Object(s.c)(function (e) {
            return e.cart;
          }).cartItems;
        Object(r.useEffect)(function () {
          c && o(ga(c, i));
        }, [o, c, i]);
        var d,
          j = function (e) {
            o(function (e) {
              return function () {
                var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                  return Object(Pt.a)().wrap(function (t) {
                    for (;;) switch (t.prev = t.next) {
                      case 0:
                        a({
                          type: te,
                          payload: e
                        }), localStorage.setItem("cartItems", JSON.stringify(r().cart.cartItems));
                      case 2:
                      case "end":
                        return t.stop();
                    }
                  }, t);
                }));
                return function (e, a) {
                  return t.apply(this, arguments);
                };
              }();
            }(e));
          };
        return Object(Dt.jsx)("div", {
          className: "container top20",
          children: Object(Dt.jsxs)(Qt.a, {
            children: [Object(Dt.jsxs)(Wt.a, {
              md: 8,
              children: [Object(Dt.jsx)("h1", {
                children: "Va\u0161a korpa"
              }), 0 === l.length ? Object(Dt.jsxs)(Zt, {
                children: ["Va\u0161a korpa je prazna ", Object(Dt.jsx)(Et.Link, {
                  to: "/products",
                  children: "Nazad"
                })]
              }) : Object(Dt.jsx)(pa.a, {
                variant: "flush",
                children: l.map(function (e) {
                  return Object(Dt.jsx)(pa.a.Item, {
                    children: Object(Dt.jsxs)(Qt.a, {
                      children: [Object(Dt.jsx)(Wt.a, {
                        md: 2,
                        children: Object(Dt.jsx)(Et.Link, {
                          to: "/product/".concat(e.product),
                          children: Object(Dt.jsx)(ca.a, {
                            src: e.image,
                            alt: e.name,
                            fluid: !0,
                            rounded: !0
                          })
                        })
                      }), Object(Dt.jsx)(Wt.a, {
                        md: 3,
                        children: Object(Dt.jsx)(Et.Link, {
                          to: "/product/".concat(e.product),
                          children: e.name
                        })
                      }), Object(Dt.jsxs)(Wt.a, {
                        md: 2,
                        children: [e.price, " din"]
                      }), Object(Dt.jsx)(Wt.a, {
                        md: 2,
                        children: Object(Dt.jsx)(Tt.a.Control, {
                          as: "select",
                          value: e.qty,
                          onChange: function (t) {
                            return o(ga(e.product, Number(t.target.value)));
                          },
                          children: Object(X.a)(Array(e.countInStock).keys()).map(function (e) {
                            return Object(Dt.jsx)("option", {
                              value: e + 1,
                              children: e + 1
                            }, e + 1);
                          })
                        })
                      }), Object(Dt.jsx)(Wt.a, {
                        md: 2,
                        children: Object(Dt.jsx)(At.a, {
                          type: "button",
                          variant: "light",
                          onClick: function () {
                            return j(e.product);
                          },
                          children: Object(Dt.jsx)("i", {
                            className: "fas fa-trash"
                          })
                        })
                      })]
                    })
                  }, e.product);
                })
              })]
            }), Object(Dt.jsx)(Wt.a, {
              md: 4,
              children: Object(Dt.jsx)(Ht.a, {
                children: Object(Dt.jsxs)(pa.a, {
                  variant: "flush",
                  children: [Object(Dt.jsxs)(pa.a.Item, {
                    children: [Object(Dt.jsxs)("h2", {
                      children: ["Me\u0111uzbir (", l.reduce(function (e, t) {
                        return e + t.qty;
                      }, 0), ") proizvoda"]
                    }), Object(Dt.jsxs)("span", {
                      className: "push-to-right",
                      children: [(d = l.reduce(function (e, t) {
                        return e + t.qty * t.price;
                      }, 0).toFixed(2), (Math.round(100 * d) / 100).toFixed(2)), " ", "din"]
                    })]
                  }), Object(Dt.jsx)(pa.a.Item, {
                    children: Object(Dt.jsx)(At.a, {
                      type: "button",
                      className: "btn-block",
                      disabled: 0 === l.length,
                      onClick: function () {
                        n.push("/login?redirect=shipping");
                      },
                      children: "Nastavite na pla\u0107anje"
                    })
                  })]
                })
              })
            })]
          })
        });
      },
      va = function (e) {
        var t = e.children;
        return Object(Dt.jsx)(Ct.a, {
          children: Object(Dt.jsx)(Qt.a, {
            className: "justify-content-md-center",
            children: Object(Dt.jsx)(Wt.a, {
              xs: 12,
              md: 6,
              children: t
            })
          })
        });
      },
      ya = function (e) {
        var t = e.location,
          a = e.history,
          n = Object(r.useState)(""),
          c = Object(Rt.a)(n, 2),
          i = c[0],
          o = c[1],
          l = Object(r.useState)(""),
          d = Object(Rt.a)(l, 2),
          j = d[0],
          u = d[1],
          b = Object(s.b)(),
          h = Object(s.c)(function (e) {
            return e.userLogin;
          }),
          p = h.loading,
          O = h.error,
          x = h.userInfo,
          m = t.search ? t.search.split("=")[1] : "/";
        Object(r.useEffect)(function () {
          x && a.push(m);
        }, [a, x, m]);
        return Object(Dt.jsxs)(va, {
          children: [Object(Dt.jsx)("h1", {
            children: "Prijava"
          }), O && Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: O
          }), p && Object(Dt.jsx)(ta, {}), Object(Dt.jsxs)(Tt.a, {
            onSubmit: function (e) {
              e.preventDefault(), b(function (e, t) {
                return function () {
                  var a = Object(Ut.a)(Object(Pt.a)().mark(function a(r) {
                    var n, c, s;
                    return Object(Pt.a)().wrap(function (a) {
                      for (;;) switch (a.prev = a.next) {
                        case 0:
                          return a.prev = 0, r({
                            type: de
                          }), n = {
                            headers: {
                              "Content-Type": "application/json"
                            }
                          }, a.next = 5, Ft.a.post("/api/users/login", {
                            email: e,
                            password: t
                          }, n);
                        case 5:
                          c = a.sent, s = c.data, r({
                            type: je,
                            payload: s
                          }), localStorage.setItem("userInfo", JSON.stringify(s)), a.next = 14;
                          break;
                        case 11:
                          a.prev = 11, a.t0 = a.catch(0), r({
                            type: ue,
                            payload: a.t0.response && a.t0.response.data.message ? a.t0.response.data.message : a.t0.message
                          });
                        case 14:
                        case "end":
                          return a.stop();
                      }
                    }, a, null, [[0, 11]]);
                  }));
                  return function (e) {
                    return a.apply(this, arguments);
                  };
                }();
              }(i, j));
            },
            children: [Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "email",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Email Adresa"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "email",
                placeholder: "Unesite email adresu",
                value: i,
                onChange: function (e) {
                  return o(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "password",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Lozinka"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "password",
                placeholder: "Unesite lozinku",
                value: j,
                onChange: function (e) {
                  return u(e.target.value);
                }
              })]
            }), Object(Dt.jsx)(At.a, {
              type: "submit",
              variant: "primary",
              children: "Prijava"
            })]
          }), Object(Dt.jsx)(Qt.a, {
            className: "py-3",
            children: Object(Dt.jsxs)(Wt.a, {
              children: ["Nemate nalog?", " ", Object(Dt.jsx)(Et.Link, {
                to: m ? "/register?redirect=".concat(m) : "/register",
                children: "Registracija"
              })]
            })
          })]
        });
      },
      Sa = a(303),
      Na = function (e) {
        var t = e.location,
          a = e.history,
          n = Object(r.useState)(""),
          c = Object(Rt.a)(n, 2),
          i = c[0],
          o = c[1],
          l = Object(r.useState)(""),
          d = Object(Rt.a)(l, 2),
          j = d[0],
          u = d[1],
          b = Object(r.useState)(""),
          h = Object(Rt.a)(b, 2),
          p = h[0],
          O = h[1],
          x = Object(r.useState)(""),
          m = Object(Rt.a)(x, 2),
          g = m[0],
          f = m[1],
          v = Object(r.useState)(""),
          y = Object(Rt.a)(v, 2),
          S = y[0],
          N = y[1],
          E = Object(r.useState)(""),
          k = Object(Rt.a)(E, 2),
          C = k[0],
          I = k[1],
          _ = Object(r.useState)(""),
          L = Object(Rt.a)(_, 2),
          w = L[0],
          R = L[1],
          T = Object(r.useState)(""),
          A = Object(Rt.a)(T, 2),
          D = A[0],
          P = A[1],
          U = Object(r.useState)(""),
          z = Object(Rt.a)(U, 2),
          F = z[0],
          G = z[1],
          M = Object(r.useState)(""),
          B = Object(Rt.a)(M, 2),
          V = B[0],
          Y = B[1],
          Q = Object(r.useState)(null),
          W = Object(Rt.a)(Q, 2),
          q = W[0],
          H = W[1],
          K = Object(s.b)(),
          J = Object(s.c)(function (e) {
            return e.userRegister;
          }),
          $ = J.loading,
          X = J.error,
          Z = J.userInfo,
          ee = t.search ? t.search.split("=")[1] : "/";
        Object(r.useEffect)(function () {
          if (Z) a.push(ee);else {
            var e = Sa.generate({
              length: 8,
              count: 1,
              charset: "0123456789"
            });
            G(e);
          }
        }, [a, Z, ee]);
        return Object(Dt.jsxs)(va, {
          children: [Object(Dt.jsx)("h1", {
            children: "Registracija"
          }), q && Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: q
          }), X && Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: X
          }), $ && Object(Dt.jsx)(ta, {}), Object(Dt.jsxs)(Tt.a, {
            onSubmit: function (e) {
              e.preventDefault(), w !== V ? H("Lozinke se ne poklapaju!") : (console.log(g), K(function (e, t, a, r, n, c, s, i, o) {
                return function () {
                  var l = Object(Ut.a)(Object(Pt.a)().mark(function l(d) {
                    var j, u, b;
                    return Object(Pt.a)().wrap(function (l) {
                      for (;;) switch (l.prev = l.next) {
                        case 0:
                          return l.prev = 0, d({
                            type: he
                          }), j = {
                            headers: {
                              "Content-Type": "application/json"
                            }
                          }, l.next = 5, Ft.a.post("/api/users", {
                            name: e,
                            address: t,
                            city: a,
                            postcode: r,
                            phone: n,
                            email: c,
                            password: s,
                            referralCode: i,
                            personalCode: o
                          }, j);
                        case 5:
                          u = l.sent, b = u.data, d({
                            type: pe,
                            payload: b
                          }), d({
                            type: je,
                            payload: b
                          }), localStorage.setItem("userInfo", JSON.stringify(b)), l.next = 15;
                          break;
                        case 12:
                          l.prev = 12, l.t0 = l.catch(0), d({
                            type: Oe,
                            payload: l.t0.response && l.t0.response.data.message ? l.t0.response.data.message : l.t0.message
                          });
                        case 15:
                        case "end":
                          return l.stop();
                      }
                    }, l, null, [[0, 12]]);
                  }));
                  return function (e) {
                    return l.apply(this, arguments);
                  };
                }();
              }(i, j, p, g, S, C, w, D, F)));
            },
            children: [Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "personalCode",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Li\u010dni kod"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Va\u0161 li\u010dni kod",
                disabled: !0,
                value: F
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "name",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Ime i prezime"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Unesite ime i prezime",
                value: i,
                onChange: function (e) {
                  return o(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "address",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Adresa"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Unesite adresu",
                value: j,
                onChange: function (e) {
                  return u(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "city",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Grad"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Unesite grad",
                value: p,
                onChange: function (e) {
                  return O(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "postcode",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Po\u0161tanski broj"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Unesite po\u0161tanski broj",
                value: g,
                onChange: function (e) {
                  return f(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "phone",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Broj telefona"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Unesite broj telefona",
                value: S,
                onChange: function (e) {
                  return N(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "email",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Email adresa"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "email",
                placeholder: "Unesite email adresu",
                value: C,
                onChange: function (e) {
                  return I(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "referralCode",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Referal kod"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Va\u0161 referal",
                value: D,
                onChange: function (e) {
                  return P(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "password",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Lozinka"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "password",
                placeholder: "Unesite lozinku",
                value: w,
                onChange: function (e) {
                  return R(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "confirmPassword",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Potvrda lozinke"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "password",
                placeholder: "Unesite potvrdu lozinke",
                value: V,
                onChange: function (e) {
                  return Y(e.target.value);
                }
              })]
            }), Object(Dt.jsx)(At.a, {
              type: "submit",
              variant: "primary",
              children: "Registracija"
            })]
          }), Object(Dt.jsx)(Qt.a, {
            className: "py-3",
            children: Object(Dt.jsxs)(Wt.a, {
              children: ["Ve\u0107 ste registrovani?", " ", Object(Dt.jsx)(Et.Link, {
                to: ee ? "/login?redirect=".concat(ee) : "/login",
                children: "Prijava"
              })]
            })
          })]
        });
      },
      Ea = a(583),
      ka = function (e) {
        return function () {
          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
            var n, c, s, i, o;
            return Object(Pt.a)().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return t.prev = 0, a({
                    type: Ve
                  }), n = r(), c = n.userLogin.userInfo, s = {
                    headers: {
                      Authorization: "Bearer ".concat(c.token)
                    }
                  }, t.next = 6, Ft.a.get("/api/orders/".concat(e), s);
                case 6:
                  i = t.sent, o = i.data, a({
                    type: Ye,
                    payload: o
                  }), t.next = 14;
                  break;
                case 11:
                  t.prev = 11, t.t0 = t.catch(0), a({
                    type: Qe,
                    payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                  });
                case 14:
                case "end":
                  return t.stop();
              }
            }, t, null, [[0, 11]]);
          }));
          return function (e, a) {
            return t.apply(this, arguments);
          };
        }();
      },
      Ca = function (e) {
        return function () {
          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
            var n, c, s, i, o;
            return Object(Pt.a)().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return t.prev = 0, a({
                    type: We
                  }), n = r(), c = n.userLogin.userInfo, s = {
                    headers: {
                      Authorization: "Bearer ".concat(c.token)
                    }
                  }, t.next = 6, Ft.a.put("/api/orders/".concat(e, "/pay"), {}, s);
                case 6:
                  i = t.sent, o = i.data, a({
                    type: qe,
                    payload: o
                  }), t.next = 14;
                  break;
                case 11:
                  t.prev = 11, t.t0 = t.catch(0), a({
                    type: He,
                    payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                  });
                case 14:
                case "end":
                  return t.stop();
              }
            }, t, null, [[0, 11]]);
          }));
          return function (e, a) {
            return t.apply(this, arguments);
          };
        }();
      },
      Ia = function (e) {
        return function () {
          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
            var n, c, s, i, o;
            return Object(Pt.a)().wrap(function (t) {
              for (;;) switch (t.prev = t.next) {
                case 0:
                  return t.prev = 0, a({
                    type: ut
                  }), n = r(), c = n.userLogin.userInfo, s = {
                    headers: {
                      Authorization: "Bearer ".concat(c.token)
                    }
                  }, t.next = 6, Ft.a.put("/api/orders/".concat(e._id, "/deliver"), {}, s);
                case 6:
                  i = t.sent, o = i.data, a({
                    type: bt,
                    payload: o
                  }), t.next = 14;
                  break;
                case 11:
                  t.prev = 11, t.t0 = t.catch(0), a({
                    type: ht,
                    payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                  });
                case 14:
                case "end":
                  return t.stop();
              }
            }, t, null, [[0, 11]]);
          }));
          return function (e, a) {
            return t.apply(this, arguments);
          };
        }();
      },
      _a = a(26),
      La = a.n(_a),
      wa = function (e) {
        var t = e.history,
          a = Object(r.useState)(""),
          n = Object(Rt.a)(a, 2),
          c = n[0],
          i = n[1],
          o = Object(r.useState)(""),
          l = Object(Rt.a)(o, 2),
          d = l[0],
          j = l[1],
          u = Object(r.useState)(""),
          b = Object(Rt.a)(u, 2),
          h = b[0],
          p = b[1],
          O = Object(r.useState)(""),
          x = Object(Rt.a)(O, 2),
          m = x[0],
          g = x[1],
          f = Object(r.useState)(""),
          v = Object(Rt.a)(f, 2),
          y = v[0],
          S = v[1],
          N = Object(r.useState)(""),
          E = Object(Rt.a)(N, 2),
          k = E[0],
          C = E[1],
          I = Object(r.useState)(""),
          _ = Object(Rt.a)(I, 2),
          L = _[0],
          w = _[1],
          R = Object(r.useState)(0),
          T = Object(Rt.a)(R, 2),
          A = T[0],
          D = T[1],
          P = Object(r.useState)(""),
          U = Object(Rt.a)(P, 2),
          z = U[0],
          F = U[1],
          G = Object(r.useState)(""),
          M = Object(Rt.a)(G, 2),
          B = M[0],
          V = M[1],
          Y = Object(r.useState)(null),
          Q = Object(Rt.a)(Y, 2),
          W = Q[0],
          q = Q[1],
          H = Object(s.b)(),
          K = Object(s.c)(function (e) {
            return e.userDetails;
          }),
          J = K.loading,
          $ = K.error,
          X = K.user,
          Z = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          ee = Object(s.c)(function (e) {
            return e.userUpdateProfile;
          }).success,
          te = Object(s.c)(function (e) {
            return e.orderListMy;
          }),
          ae = te.loading,
          re = te.error,
          ne = te.orders;
        Object(r.useEffect)(function () {
          Z ? X && X.name && !ee ? (i(X.name), C(X.email), j(X.address), p(X.city), g(X.postcode), S(X.phone), w(X.personalCode), D(X.referralPoints || 0)) : (H({
            type: Ce
          }), H(Mt("profile")), H(function () {
            var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t, a) {
              var r, n, c, s, i;
              return Object(Pt.a)().wrap(function (e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    return e.prev = 0, t({
                      type: Je
                    }), r = a(), n = r.userLogin.userInfo, c = {
                      headers: {
                        Authorization: "Bearer ".concat(n.token)
                      }
                    }, e.next = 6, Ft.a.get("/api/orders/myorders", c);
                  case 6:
                    s = e.sent, i = s.data, t({
                      type: $e,
                      payload: i
                    }), e.next = 14;
                    break;
                  case 11:
                    e.prev = 11, e.t0 = e.catch(0), t({
                      type: Xe,
                      payload: e.t0.response && e.t0.response.data.message ? e.t0.response.data.message : e.t0.message
                    });
                  case 14:
                  case "end":
                    return e.stop();
                }
              }, e, null, [[0, 11]]);
            }));
            return function (t, a) {
              return e.apply(this, arguments);
            };
          }())) : t.push("/login");
        }, [H, t, Z, X, ee, ne]);
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)("div", {
            className: "contact-us profile",
            children: Object(Dt.jsxs)("div", {
              className: "container center",
              children: [Object(Dt.jsx)("h2", {
                className: "",
                children: "Moj Profil"
              }), Object(Dt.jsxs)("h3", {
                children: ["Li\u010dni kod: ", L]
              }), Object(Dt.jsxs)("h3", {
                children: ["Va\u0161a trenutna zarada: ", A, " dinara"]
              })]
            })
          }), Object(Dt.jsx)("div", {
            className: "container",
            children: Object(Dt.jsxs)(Qt.a, {
              children: [Object(Dt.jsxs)(Wt.a, {
                md: 3,
                children: [Object(Dt.jsx)("p", {}), Object(Dt.jsx)("p", {}), W && Object(Dt.jsx)(Zt, {
                  variant: "danger",
                  children: W
                }), $ && Object(Dt.jsx)(Zt, {
                  variant: "danger",
                  children: $
                }), ee && Object(Dt.jsx)(Zt, {
                  variant: "success",
                  children: "Profil je a\u017euriran"
                }), J && Object(Dt.jsx)(ta, {}), Object(Dt.jsxs)(Tt.a, {
                  onSubmit: function (e) {
                    e.preventDefault(), z !== B ? q("Lozinke se ne poklapaju") : H(function (e) {
                      return function () {
                        var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                          var n, c, s, i, o;
                          return Object(Pt.a)().wrap(function (t) {
                            for (;;) switch (t.prev = t.next) {
                              case 0:
                                return t.prev = 0, a({
                                  type: Ne
                                }), n = r(), c = n.userLogin.userInfo, s = {
                                  headers: {
                                    "Content-Type": "application/json",
                                    Authorization: "Bearer ".concat(c.token)
                                  }
                                }, t.next = 6, Ft.a.put("/api/users/profile", e, s);
                              case 6:
                                i = t.sent, o = i.data, a({
                                  type: Ee,
                                  payload: o
                                }), a({
                                  type: je,
                                  payload: o
                                }), localStorage.setItem("userInfo", JSON.stringify(o)), t.next = 16;
                                break;
                              case 13:
                                t.prev = 13, t.t0 = t.catch(0), a({
                                  type: ke,
                                  payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                                });
                              case 16:
                              case "end":
                                return t.stop();
                            }
                          }, t, null, [[0, 13]]);
                        }));
                        return function (e, a) {
                          return t.apply(this, arguments);
                        };
                      }();
                    }({
                      id: X._id,
                      name: c,
                      address: d,
                      city: h,
                      postcode: m,
                      phone: y,
                      email: k,
                      password: z
                    }));
                  },
                  className: "push-to-right",
                  children: [Object(Dt.jsxs)(Tt.a.Group, {
                    controlId: "email",
                    children: [Object(Dt.jsx)(Tt.a.Label, {
                      children: "Ime i prezime"
                    }), Object(Dt.jsx)(Tt.a.Control, {
                      type: "name",
                      placeholder: "Unesite ime i prezime",
                      value: c,
                      onChange: function (e) {
                        return i(e.target.value);
                      }
                    })]
                  }), Object(Dt.jsxs)(Tt.a.Group, {
                    controlId: "address",
                    children: [Object(Dt.jsx)(Tt.a.Label, {
                      children: "Adresa"
                    }), Object(Dt.jsx)(Tt.a.Control, {
                      type: "text",
                      placeholder: "Unesite adresu",
                      value: d,
                      onChange: function (e) {
                        return j(e.target.value);
                      }
                    })]
                  }), Object(Dt.jsxs)(Tt.a.Group, {
                    controlId: "city",
                    children: [Object(Dt.jsx)(Tt.a.Label, {
                      children: "Grad"
                    }), Object(Dt.jsx)(Tt.a.Control, {
                      type: "text",
                      placeholder: "Unesite grad",
                      value: h,
                      onChange: function (e) {
                        return p(e.target.value);
                      }
                    })]
                  }), Object(Dt.jsxs)(Tt.a.Group, {
                    controlId: "postcode",
                    children: [Object(Dt.jsx)(Tt.a.Label, {
                      children: "Po\u0161tanski broj"
                    }), Object(Dt.jsx)(Tt.a.Control, {
                      type: "text",
                      placeholder: "Unesite po\u0161tanski broj",
                      value: m,
                      onChange: function (e) {
                        return g(e.target.value);
                      }
                    })]
                  }), Object(Dt.jsxs)(Tt.a.Group, {
                    controlId: "phone",
                    children: [Object(Dt.jsx)(Tt.a.Label, {
                      children: "Broj telefona"
                    }), Object(Dt.jsx)(Tt.a.Control, {
                      type: "text",
                      placeholder: "Unesite broj telefona",
                      value: y,
                      onChange: function (e) {
                        return S(e.target.value);
                      }
                    })]
                  }), Object(Dt.jsxs)(Tt.a.Group, {
                    controlId: "email",
                    children: [Object(Dt.jsx)(Tt.a.Label, {
                      children: "Email adresa"
                    }), Object(Dt.jsx)(Tt.a.Control, {
                      type: "email",
                      placeholder: "Unesite email adresu",
                      value: k,
                      onChange: function (e) {
                        return C(e.target.value);
                      }
                    })]
                  }), Object(Dt.jsxs)(Tt.a.Group, {
                    controlId: "password",
                    children: [Object(Dt.jsx)(Tt.a.Label, {
                      children: "Lozinka"
                    }), Object(Dt.jsx)(Tt.a.Control, {
                      type: "password",
                      placeholder: "Unesite lozinku",
                      value: z,
                      onChange: function (e) {
                        return F(e.target.value);
                      }
                    })]
                  }), Object(Dt.jsxs)(Tt.a.Group, {
                    controlId: "confirmPassword",
                    children: [Object(Dt.jsx)(Tt.a.Label, {
                      children: "Potvrda lozinke"
                    }), Object(Dt.jsx)(Tt.a.Control, {
                      type: "password",
                      placeholder: "Unesite potvrdu lozinke",
                      value: B,
                      onChange: function (e) {
                        return V(e.target.value);
                      }
                    })]
                  }), Object(Dt.jsx)(At.a, {
                    type: "submit",
                    variant: "primary",
                    children: "A\u017euriraj"
                  })]
                })]
              }), Object(Dt.jsxs)(Wt.a, {
                md: 9,
                children: [Object(Dt.jsx)("p", {}), Object(Dt.jsx)("h2", {
                  children: "Moje porud\u017ebine"
                }), ae ? Object(Dt.jsx)(ta, {}) : re ? Object(Dt.jsx)(Zt, {
                  variant: "danger",
                  children: re
                }) : Object(Dt.jsxs)(Ea.a, {
                  bordered: !0,
                  hover: !0,
                  responsive: !0,
                  className: "table-sm",
                  children: [Object(Dt.jsx)("thead", {
                    children: Object(Dt.jsxs)("tr", {
                      children: [Object(Dt.jsx)("th", {
                        children: "ID"
                      }), Object(Dt.jsx)("th", {
                        children: "Datum"
                      }), Object(Dt.jsx)("th", {
                        children: "Ukupno"
                      }), Object(Dt.jsx)("th", {
                        children: "Pla\u0107eno"
                      }), Object(Dt.jsx)("th", {
                        children: "Isporu\u010deno"
                      }), Object(Dt.jsx)("th", {
                        children: "Informacije"
                      })]
                    })
                  }), Object(Dt.jsx)("tbody", {
                    children: ne.map(function (e) {
                      return Object(Dt.jsxs)("tr", {
                        children: [Object(Dt.jsx)("td", {
                          children: e.orderNo
                        }), Object(Dt.jsx)("td", {
                          children: La()(e.createdAt).format("DD.MM.yyyy.")
                        }), Object(Dt.jsxs)("td", {
                          children: [e.totalPrice, " din"]
                        }), Object(Dt.jsx)("td", {
                          children: e.isPaid ? La()(e.paidAt).format("DD.MM.yyyy.") : Object(Dt.jsx)("i", {
                            className: "fas fa-times",
                            style: {
                              color: "red"
                            }
                          })
                        }), Object(Dt.jsx)("td", {
                          children: e.isDelivered ? La()(e.deliveredAt).format("DD.MM.yyyy.") : Object(Dt.jsx)("i", {
                            className: "fas fa-times",
                            style: {
                              color: "red"
                            }
                          })
                        }), Object(Dt.jsx)("td", {
                          children: Object(Dt.jsx)(It.LinkContainer, {
                            to: "/order/".concat(e._id),
                            children: Object(Dt.jsx)(At.a, {
                              className: "btn-sm",
                              variant: "light",
                              children: "Detalji"
                            })
                          })
                        })]
                      }, e._id);
                    })
                  })]
                })]
              })]
            })
          })]
        });
      },
      Ra = function (e) {
        var t = e.step1,
          a = e.step2,
          r = e.step3,
          n = e.step4;
        return Object(Dt.jsxs)(Lt.a, {
          className: "justify-content-center mb-4",
          children: [Object(Dt.jsx)(Lt.a.Item, {
            children: t ? Object(Dt.jsx)(It.LinkContainer, {
              to: "/login",
              children: Object(Dt.jsx)(Lt.a.Link, {
                children: "Prijava"
              })
            }) : Object(Dt.jsx)(Lt.a.Link, {
              disabled: !0,
              children: "Prijava"
            })
          }), Object(Dt.jsx)(Lt.a.Item, {
            children: a ? Object(Dt.jsx)(It.LinkContainer, {
              to: "/shipping",
              children: Object(Dt.jsx)(Lt.a.Link, {
                children: "Dostava"
              })
            }) : Object(Dt.jsx)(Lt.a.Link, {
              disabled: !0,
              children: "Dostava"
            })
          }), Object(Dt.jsx)(Lt.a.Item, {
            children: r ? Object(Dt.jsx)(It.LinkContainer, {
              to: "/payment",
              children: Object(Dt.jsx)(Lt.a.Link, {
                children: "Pla\u0107anje"
              })
            }) : Object(Dt.jsx)(Lt.a.Link, {
              disabled: !0,
              children: "Pla\u0107anje"
            })
          }), Object(Dt.jsx)(Lt.a.Item, {
            children: n ? Object(Dt.jsx)(It.LinkContainer, {
              to: "/placeorder",
              children: Object(Dt.jsx)(Lt.a.Link, {
                children: "Poru\u010divanje"
              })
            }) : Object(Dt.jsx)(Lt.a.Link, {
              disabled: !0,
              children: "Poru\u010divanje"
            })
          })]
        });
      },
      Ta = function (e) {
        var t = e.history,
          a = Object(s.c)(function (e) {
            return e.userDetails;
          }),
          n = (a.loading, a.error, a.user);
        console.log(n);
        var c = Object(s.c)(function (e) {
            return e.cart;
          }).shippingAddress,
          i = Object(r.useState)(c.address),
          o = Object(Rt.a)(i, 2),
          l = o[0],
          d = o[1],
          j = Object(r.useState)(c.city),
          u = Object(Rt.a)(j, 2),
          b = u[0],
          h = u[1],
          p = Object(r.useState)(c.postalCode),
          O = Object(Rt.a)(p, 2),
          x = O[0],
          m = O[1],
          g = Object(r.useState)(c.phone),
          f = Object(Rt.a)(g, 2),
          v = f[0],
          y = f[1],
          S = Object(r.useState)(c.country),
          N = Object(Rt.a)(S, 2),
          E = N[0],
          k = (N[1], Object(r.useState)(c.email)),
          C = Object(Rt.a)(k, 2),
          I = C[0],
          _ = C[1],
          L = Object(s.b)();
        Object(r.useEffect)(function () {
          a ? n && n.name && (_(n.email), d(n.address), h(n.city), m(n.postcode), y(n.phone)) : t.push("/login");
        }, [L, t, a, n]);
        return Object(Dt.jsxs)(va, {
          children: [Object(Dt.jsx)(Ra, {
            step1: !0,
            step2: !0
          }), Object(Dt.jsx)("h1", {
            children: "Podaci za dostavu"
          }), Object(Dt.jsxs)(Tt.a, {
            onSubmit: function (e) {
              var a;
              e.preventDefault(), L((a = {
                address: l,
                city: b,
                postalCode: x,
                phone: v,
                email: I,
                country: E
              }, function () {
                var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t) {
                  return Object(Pt.a)().wrap(function (e) {
                    for (;;) switch (e.prev = e.next) {
                      case 0:
                        t({
                          type: ae,
                          payload: a
                        }), localStorage.setItem("shippingAddress", JSON.stringify(a));
                      case 2:
                      case "end":
                        return e.stop();
                    }
                  }, e);
                }));
                return function (t) {
                  return e.apply(this, arguments);
                };
              }())), t.push("/payment");
            },
            children: [Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "address",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Adresa"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Unesite adresu za dostavu",
                value: l,
                required: !0,
                onChange: function (e) {
                  return d(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "city",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Grad"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Unesite naziv grada",
                value: b,
                required: !0,
                onChange: function (e) {
                  return h(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "postalCode",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Po\u0161tanski broj"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Unesite po\u0161tanski broj",
                value: x,
                required: !0,
                onChange: function (e) {
                  return m(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "phone",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Broj telefona"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "text",
                placeholder: "Unesite broj telefona",
                value: v,
                onChange: function (e) {
                  return y(e.target.value);
                }
              })]
            }), Object(Dt.jsxs)(Tt.a.Group, {
              controlId: "email",
              children: [Object(Dt.jsx)(Tt.a.Label, {
                children: "Email adresa"
              }), Object(Dt.jsx)(Tt.a.Control, {
                type: "email",
                placeholder: "Unesite email adresu",
                value: I,
                onChange: function (e) {
                  return _(e.target.value);
                }
              })]
            }), Object(Dt.jsx)(At.a, {
              type: "submit",
              variant: "primary",
              children: "Dalje"
            })]
          })]
        });
      },
      Aa = function (e) {
        var t = e.history;
        Object(s.c)(function (e) {
          return e.cart;
        }).shippingAddress || t.push("/shipping");
        var a = Object(r.useState)("Virman"),
          n = Object(Rt.a)(a, 2),
          c = n[0],
          i = n[1],
          o = Object(s.b)();
        return Object(Dt.jsxs)(va, {
          children: [Object(Dt.jsx)(Ra, {
            step1: !0,
            step2: !0,
            step3: !0
          }), Object(Dt.jsx)("h1", {
            children: "Na\u010din pla\u0107anja"
          }), Object(Dt.jsxs)(Tt.a, {
            onSubmit: function (e) {
              var a;
              e.preventDefault(), o((a = c, function () {
                var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t) {
                  return Object(Pt.a)().wrap(function (e) {
                    for (;;) switch (e.prev = e.next) {
                      case 0:
                        t({
                          type: re,
                          payload: a
                        }), localStorage.setItem("paymentMethod", JSON.stringify(a));
                      case 2:
                      case "end":
                        return e.stop();
                    }
                  }, e);
                }));
                return function (t) {
                  return e.apply(this, arguments);
                };
              }())), t.push("/placeorder");
            },
            children: [Object(Dt.jsxs)(Tt.a.Group, {
              children: [Object(Dt.jsx)(Tt.a.Label, {
                as: "legend",
                children: "Odaberite na\u010din pla\u0107anja"
              }), Object(Dt.jsx)(Wt.a, {
                children: Object(Dt.jsx)(Tt.a.Check, {
                  type: "radio",
                  label: "Uplata na ra\u010dun",
                  id: "PayPal",
                  name: "paymentMethod",
                  value: "Virman",
                  checked: !0,
                  onChange: function (e) {
                    return i(e.target.value);
                  }
                })
              })]
            }), Object(Dt.jsx)(At.a, {
              type: "submit",
              variant: "primary",
              children: "Dalje"
            })]
          })]
        });
      },
      Da = function (e) {
        var t = e.history,
          a = Object(s.b)(),
          n = Object(r.useState)(""),
          c = Object(Rt.a)(n, 2),
          i = (c[0], c[1], Object(s.c)(function (e) {
            return e.cart;
          })),
          o = Object(s.c)(function (e) {
            return e.userDetails;
          }),
          l = (o.loading, o.error, o.user, Object(s.c)(function (e) {
            return e.orderLastNo;
          })),
          d = (l.loading, l.success),
          j = (l.msg, function (e) {
            return (Math.round(100 * e) / 100).toFixed(2);
          });
        i.itemsPrice = j(i.cartItems.reduce(function (e, t) {
          return e + t.price * t.qty;
        }, 0)), i.shippingPrice = 0, i.taxPrice = 0, i.totalPrice = j((Number(i.itemsPrice) + Number(i.shippingPrice) + Number(i.taxPrice)).toFixed(2));
        var u = Object(s.c)(function (e) {
            return e.orderCreate;
          }),
          b = u.order,
          h = u.success,
          p = u.error;
        Object(r.useEffect)(function () {
          a(function () {
            var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t, a) {
              var r, n, c, s, i;
              return Object(Pt.a)().wrap(function (e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    return e.prev = 0, t({
                      type: Ot
                    }), r = a(), n = r.userLogin.userInfo, c = {
                      headers: {
                        Authorization: "Bearer ".concat(n.token)
                      }
                    }, e.next = 6, Ft.a.get("/api/orders/lastOrderNo", c);
                  case 6:
                    s = e.sent, i = s.data, t({
                      type: xt,
                      payload: i
                    }), e.next = 14;
                    break;
                  case 11:
                    e.prev = 11, e.t0 = e.catch(0), t({
                      type: mt,
                      payload: e.t0.response && e.t0.response.data.message ? e.t0.response.data.message : e.t0.message
                    });
                  case 14:
                  case "end":
                    return e.stop();
                }
              }, e, null, [[0, 11]]);
            }));
            return function (t, a) {
              return e.apply(this, arguments);
            };
          }()), h && (t.push("/order/".concat(b._id)), a({
            type: fe
          }), a({
            type: Be
          }));
        }, [t, h, d]);
        var O = function () {
          var e = new Date(),
            t = e.getFullYear(),
            a = String(l.orderNo);
          if (a && "" != a) {
            var r = a.split("/");
            return t == r[1] ? +r[0] + 1 + "/" + t : "1/" + t;
          }
          return "1/" + e.getFullYear();
        };
        return Object(Dt.jsx)(Dt.Fragment, {
          children: Object(Dt.jsxs)("div", {
            className: "container",
            children: [Object(Dt.jsx)(Ra, {
              step1: !0,
              step2: !0,
              step3: !0,
              step4: !0
            }), Object(Dt.jsxs)(Qt.a, {
              children: [Object(Dt.jsx)(Wt.a, {
                md: 8,
                children: Object(Dt.jsxs)(pa.a, {
                  variant: "flush",
                  children: [Object(Dt.jsxs)(pa.a.Item, {
                    children: [Object(Dt.jsx)("h2", {
                      children: "Podaci za dostavu"
                    }), Object(Dt.jsx)("p", {
                      children: Object(Dt.jsxs)("span", {
                        className: "push-to-right",
                        children: [Object(Dt.jsx)("strong", {
                          children: "Adresa: "
                        }), i.shippingAddress.address, ", ", i.shippingAddress.city, " ", i.shippingAddress.postalCode, ",", " ", i.shippingAddress.phone]
                      })
                    })]
                  }), Object(Dt.jsxs)(pa.a.Item, {
                    children: [Object(Dt.jsx)("h2", {
                      children: "Na\u010din pla\u0107anja"
                    }), Object(Dt.jsxs)("span", {
                      className: "push-to-right",
                      children: [Object(Dt.jsx)("strong", {
                        children: "Metod: "
                      }), i.paymentMethod]
                    })]
                  }), Object(Dt.jsxs)(pa.a.Item, {
                    children: [Object(Dt.jsx)("h2", {
                      children: "Proizvodi u korpi"
                    }), 0 === i.cartItems.length ? Object(Dt.jsx)(Zt, {
                      children: "Va\u0161a korpa je prazna"
                    }) : Object(Dt.jsx)(pa.a, {
                      variant: "flush",
                      children: i.cartItems.map(function (e, t) {
                        return Object(Dt.jsx)(pa.a.Item, {
                          children: Object(Dt.jsxs)(Qt.a, {
                            children: [Object(Dt.jsx)(Wt.a, {
                              md: 1,
                              children: Object(Dt.jsx)(ca.a, {
                                src: e.image,
                                alt: e.name,
                                fluid: !0,
                                rounded: !0
                              })
                            }), Object(Dt.jsx)(Wt.a, {
                              children: Object(Dt.jsx)(Et.Link, {
                                to: "/product/".concat(e.product),
                                children: e.name
                              })
                            }), Object(Dt.jsxs)(Wt.a, {
                              md: 4,
                              children: [e.qty, " x ", e.price, " din = ", e.qty * e.price, " din"]
                            })]
                          })
                        }, t);
                      })
                    })]
                  })]
                })
              }), Object(Dt.jsx)(Wt.a, {
                md: 4,
                children: Object(Dt.jsx)(Ht.a, {
                  children: Object(Dt.jsxs)(pa.a, {
                    variant: "flush",
                    children: [Object(Dt.jsx)(pa.a.Item, {
                      children: Object(Dt.jsx)("h2", {
                        children: "Pregled porud\u017ebine"
                      })
                    }), Object(Dt.jsx)(pa.a.Item, {
                      className: "push-to-right",
                      children: Object(Dt.jsxs)(Qt.a, {
                        children: [Object(Dt.jsx)(Wt.a, {
                          children: "Proizvodi"
                        }), Object(Dt.jsxs)(Wt.a, {
                          children: [i.itemsPrice, " din"]
                        })]
                      })
                    }), Object(Dt.jsx)(pa.a.Item, {
                      className: "push-to-right",
                      children: Object(Dt.jsxs)(Qt.a, {
                        children: [Object(Dt.jsx)(Wt.a, {
                          children: "Po\u0161tarina"
                        }), Object(Dt.jsxs)(Wt.a, {
                          children: [i.shippingPrice, " din"]
                        })]
                      })
                    }), Object(Dt.jsx)(pa.a.Item, {
                      className: "push-to-right",
                      children: Object(Dt.jsxs)(Qt.a, {
                        children: [Object(Dt.jsx)(Wt.a, {
                          children: Object(Dt.jsx)("strong", {
                            children: "Ukupno"
                          })
                        }), Object(Dt.jsx)(Wt.a, {
                          children: Object(Dt.jsxs)("strong", {
                            children: [i.totalPrice, " din"]
                          })
                        })]
                      })
                    }), Object(Dt.jsx)(pa.a.Item, {
                      children: p && Object(Dt.jsx)(Zt, {
                        variant: "danger",
                        children: p
                      })
                    }), Object(Dt.jsx)(pa.a.Item, {
                      children: Object(Dt.jsx)(At.a, {
                        type: "button",
                        className: "btn-block",
                        disabled: 0 === i.cartItems,
                        onClick: function () {
                          a(function (e) {
                            return function () {
                              var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                                var n, c, s, i, o;
                                return Object(Pt.a)().wrap(function (t) {
                                  for (;;) switch (t.prev = t.next) {
                                    case 0:
                                      return t.prev = 0, a({
                                        type: Fe
                                      }), n = r(), c = n.userLogin.userInfo, s = {
                                        headers: {
                                          "Content-Type": "application/json",
                                          Authorization: "Bearer ".concat(c.token)
                                        }
                                      }, t.next = 6, Ft.a.post("/api/orders", e, s);
                                    case 6:
                                      i = t.sent, o = i.data, a({
                                        type: Ge,
                                        payload: o
                                      }), a({
                                        type: ee,
                                        payload: o
                                      }), localStorage.removeItem("cartItems"), t.next = 16;
                                      break;
                                    case 13:
                                      t.prev = 13, t.t0 = t.catch(0), a({
                                        type: Me,
                                        payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                                      });
                                    case 16:
                                    case "end":
                                      return t.stop();
                                  }
                                }, t, null, [[0, 13]]);
                              }));
                              return function (e, a) {
                                return t.apply(this, arguments);
                              };
                            }();
                          }({
                            orderItems: i.cartItems,
                            shippingAddress: i.shippingAddress,
                            paymentMethod: i.paymentMethod,
                            itemsPrice: i.itemsPrice,
                            shippingPrice: i.shippingPrice,
                            taxPrice: i.taxPrice,
                            totalPrice: i.totalPrice,
                            orderNo: O()
                          }));
                        },
                        children: "Po\u0161aljite porud\u017ebinu"
                      })
                    })]
                  })
                })
              })]
            })]
          })
        });
      },
      Pa = a(151),
      Ua = a.n(Pa),
      za = a(14),
      Fa = a.p + "static/media/m-logo.19f0c793.png";
    za.Font.register({
      family: "Roboto",
      src: "https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-light-webfont.ttf"
    });
    var Ga = za.StyleSheet.create({
        page: {
          flexWrap: "wrap",
          flexDirection: "row",
          backgroundColor: "#FFF",
          fontFamily: "Roboto",
          margin: "10px",
          marginTop: 0,
          marginBottom: 0
        },
        section: {
          margin: 10,
          padding: 10,
          flexGrow: 1,
          textAlign: "left",
          fontSize: 12,
          width: "45%"
        },
        sectionHeader: {
          margin: 10,
          padding: 10,
          marginBottom: 0,
          paddingBottom: 0,
          flexGrow: 1,
          textAlign: "right",
          fontSize: 14,
          width: "45%"
        },
        full: {
          margin: 10,
          padding: 10,
          textAlign: "left",
          width: "100%",
          fontSize: 12
        },
        tright: {
          textAlign: "right"
        },
        strong: {
          fontWeight: "bold"
        },
        logo: {
          width: "120px"
        },
        table: {
          display: "table",
          width: "auto",
          borderStyle: "solid",
          borderWidth: 1,
          borderRightWidth: 0,
          borderBottomWidth: 0
        },
        borderlessTable: {
          display: "table",
          width: "auto",
          borderStyle: "solid",
          borderWidth: 0,
          borderRightWidth: 0,
          borderBottomWidth: 0
        },
        tableRow: {
          margin: "auto",
          flexDirection: "row"
        },
        tableCol: {
          width: "25%",
          borderStyle: "solid",
          borderWidth: 1,
          borderLeftWidth: 0,
          borderTopWidth: 0
        },
        tableColFirst: {
          width: "40%",
          borderStyle: "solid",
          borderWidth: 1,
          borderLeftWidth: 0,
          borderTopWidth: 0
        },
        tableColSecond: {
          width: "10%",
          borderStyle: "solid",
          borderWidth: 1,
          borderLeftWidth: 0,
          borderTopWidth: 0
        },
        tableColTotalFirst: {
          width: "50%",
          borderStyle: "solid",
          borderWidth: 0,
          borderLeftWidth: 0,
          borderRightWidth: 1,
          borderTopWidth: 0
        },
        tableCell: {
          margin: "auto",
          marginTop: 5,
          marginBottom: 5,
          fontSize: 10
        },
        tableCellLeft: {
          marginLeft: 5,
          marginTop: 5,
          marginBottom: 5,
          fontSize: 10
        }
      }),
      Ma = function (e) {
        var t = e.order,
          a = La()(t.createdAt).format("DD.MM.yyyy.");
        return Object(Dt.jsx)(za.Document, {
          children: Object(Dt.jsxs)(za.Page, {
            size: "A4",
            style: Ga.page,
            children: [Object(Dt.jsx)(za.View, {
              style: Ga.sectionHeader,
              children: Object(Dt.jsx)(za.Image, {
                style: Ga.logo,
                src: Fa
              })
            }), Object(Dt.jsxs)(za.View, {
              style: Ga.sectionHeader,
              children: [Object(Dt.jsx)(za.Text, {
                style: Ga.tright,
                children: "PEDRA\u010cUN"
              }), Object(Dt.jsx)(za.Text, {
                style: Ga.tright,
                children: " "
              }), Object(Dt.jsxs)(za.Text, {
                style: Ga.tright,
                children: ["Broj predra\u010duna: ", t.orderNo]
              }), Object(Dt.jsxs)(za.Text, {
                style: Ga.tright,
                children: ["Datum izdavanja: ", a]
              })]
            }), Object(Dt.jsx)(za.View, {
              style: Ga.full,
              children: Object(Dt.jsx)(za.Text, {})
            }), Object(Dt.jsxs)(za.View, {
              style: Ga.section,
              children: [Object(Dt.jsx)(za.Text, {
                children: "MAPO"
              }), Object(Dt.jsx)(za.Text, {
                children: "Radmile Savi\u0107evi\u0107 27, Donja Vre\u017eina"
              }), Object(Dt.jsx)(za.Text, {
                children: "Tel: 062447558"
              }), Object(Dt.jsx)(za.Text, {
                children: "Email: maponis2015@gmail.com"
              })]
            }), Object(Dt.jsxs)(za.View, {
              style: Ga.section,
              children: [Object(Dt.jsx)(za.Text, {
                children: " "
              }), Object(Dt.jsx)(za.Text, {
                children: "Mati\u010dni broj: 63940364"
              }), Object(Dt.jsx)(za.Text, {
                children: "PIB: 109117318"
              }), Object(Dt.jsx)(za.Text, {
                children: "Broj ra\u010duna: 325-9500700195189-23"
              })]
            }), Object(Dt.jsxs)(za.View, {
              style: Ga.section,
              children: [Object(Dt.jsx)(za.Text, {
                style: Ga.strong,
                children: "Podaci kupca"
              }), Object(Dt.jsx)(za.Text, {
                children: t.user.name
              }), Object(Dt.jsxs)(za.Text, {
                children: [t.shippingAddress.address, ", ", t.shippingAddress.postalCode, " ", t.shippingAddress.city]
              })]
            }), Object(Dt.jsxs)(za.View, {
              style: Ga.section,
              children: [Object(Dt.jsx)(za.Text, {
                children: " "
              }), Object(Dt.jsxs)(za.Text, {
                children: ["Telefon: ", t.shippingAddress.phone]
              }), Object(Dt.jsxs)(za.Text, {
                children: ["Email: ", t.user.email]
              })]
            }), Object(Dt.jsxs)(za.View, {
              style: Ga.full,
              children: [Object(Dt.jsxs)(za.View, {
                style: Ga.table,
                children: [Object(Dt.jsxs)(za.View, {
                  style: Ga.tableRow,
                  children: [Object(Dt.jsx)(za.View, {
                    style: Ga.tableColFirst,
                    children: Object(Dt.jsx)(za.Text, {
                      style: Ga.tableCellLeft,
                      children: "Naziv proizvoda"
                    })
                  }), Object(Dt.jsx)(za.View, {
                    style: Ga.tableColSecond,
                    children: Object(Dt.jsx)(za.Text, {
                      style: Ga.tableCell,
                      children: "Koli\u010dina"
                    })
                  }), Object(Dt.jsx)(za.View, {
                    style: Ga.tableCol,
                    children: Object(Dt.jsx)(za.Text, {
                      style: Ga.tableCell,
                      children: "Cena (din)"
                    })
                  }), Object(Dt.jsx)(za.View, {
                    style: Ga.tableCol,
                    children: Object(Dt.jsx)(za.Text, {
                      style: Ga.tableCell,
                      children: "Ukupno (din)"
                    })
                  })]
                }), t.orderItems.map(function (e, t) {
                  return Object(Dt.jsxs)(za.View, {
                    style: Ga.tableRow,
                    children: [Object(Dt.jsx)(za.View, {
                      style: Ga.tableColFirst,
                      children: Object(Dt.jsx)(za.Text, {
                        style: Ga.tableCellLeft,
                        children: e.name
                      })
                    }), Object(Dt.jsx)(za.View, {
                      style: Ga.tableColSecond,
                      children: Object(Dt.jsx)(za.Text, {
                        style: Ga.tableCell,
                        children: e.qty
                      })
                    }), Object(Dt.jsx)(za.View, {
                      style: Ga.tableCol,
                      children: Object(Dt.jsx)(za.Text, {
                        style: Ga.tableCell,
                        children: e.price
                      })
                    }), Object(Dt.jsx)(za.View, {
                      style: Ga.tableCol,
                      children: Object(Dt.jsx)(za.Text, {
                        style: Ga.tableCell,
                        children: e.qty * e.price
                      })
                    })]
                  }, "order-item-" + t);
                })]
              }), Object(Dt.jsx)(za.View, {
                style: Ga.borderlessTable,
                children: Object(Dt.jsxs)(za.View, {
                  style: Ga.tableRow,
                  children: [Object(Dt.jsx)(za.View, {
                    style: Ga.tableColTotalFirst,
                    children: Object(Dt.jsx)(za.Text, {
                      style: Ga.tableCellLeft,
                      children: " "
                    })
                  }), Object(Dt.jsx)(za.View, {
                    style: Ga.tableCol,
                    children: Object(Dt.jsx)(za.Text, {
                      style: Ga.tableCell,
                      children: "Ukupno za uplatu: "
                    })
                  }), Object(Dt.jsx)(za.View, {
                    style: Ga.tableCol,
                    children: Object(Dt.jsxs)(za.Text, {
                      style: Ga.tableCell,
                      children: [t.totalPrice, " dinara"]
                    })
                  })]
                })
              })]
            })]
          })
        });
      },
      Ba = function (e) {
        var t,
          a = e.match,
          n = e.history,
          c = a.params.id,
          i = Object(s.b)(),
          o = Ua.a.resolve(),
          l = (Ua.a.join(o, "/uploads"), Object(r.useState)(!1)),
          d = Object(Rt.a)(l, 2),
          j = (d[0], d[1]),
          u = Object(s.c)(function (e) {
            return e.orderDetails;
          }),
          b = u.order,
          h = u.loading,
          p = u.error,
          O = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          x = Object(s.c)(function (e) {
            return e.orderPay;
          }),
          m = x.loading,
          g = x.success,
          f = Object(s.c)(function (e) {
            return e.orderDeliver;
          }),
          v = f.loading,
          y = f.success;
        if (!h) {
          b.itemsPrice = (t = b.orderItems.reduce(function (e, t) {
            return e + t.price * t.qty;
          }, 0), (Math.round(100 * t) / 100).toFixed(2));
        }
        Object(r.useEffect)(function () {
          console.log(a), !b || g || y || b._id !== c ? (i({
            type: Ke
          }), i({
            type: pt
          }), i(ka(c))) : b.isPaid || j(!0);
        }, [i, c, g, y, b]);
        return h ? Object(Dt.jsx)(ta, {}) : p ? Object(Dt.jsx)(Zt, {
          variant: "danger",
          children: p
        }) : Object(Dt.jsx)(Dt.Fragment, {
          children: Object(Dt.jsxs)("div", {
            className: "container",
            children: [Object(Dt.jsx)(Et.Link, {
              to: O.isAdmin ? "/admin/orderlist" : "/profile",
              className: "btn btn-light my-3",
              onClick: function (e) {
                return function (e) {
                  e.preventDefault(), O.isAdmin ? n.goBack() : n.push("/profile");
                }(e);
              },
              children: "Nazad"
            }), Object(Dt.jsxs)("h1", {
              children: ["Porud\u017ebina ", b.orderNo]
            }), Object(Dt.jsxs)(Qt.a, {
              children: [Object(Dt.jsx)(Wt.a, {
                md: 8,
                children: Object(Dt.jsxs)(pa.a, {
                  variant: "flush",
                  children: [Object(Dt.jsxs)(pa.a.Item, {
                    children: [Object(Dt.jsx)("h2", {
                      children: "Podaci za dostavu"
                    }), Object(Dt.jsx)("p", {
                      children: Object(Dt.jsxs)("span", {
                        className: "push-to-right",
                        children: [Object(Dt.jsx)("strong", {
                          children: "Broj profakture: "
                        }), " ", b.orderNo]
                      })
                    }), Object(Dt.jsx)("p", {
                      children: Object(Dt.jsxs)("span", {
                        className: "push-to-right",
                        children: [Object(Dt.jsx)("strong", {
                          children: "Ime i prezime: "
                        }), " ", b.user.name]
                      })
                    }), Object(Dt.jsx)("p", {
                      children: Object(Dt.jsxs)("span", {
                        className: "push-to-right",
                        children: [Object(Dt.jsx)("strong", {
                          children: "Email: "
                        }), Object(Dt.jsx)("a", {
                          href: "mailto:".concat(b.user.email),
                          children: b.user.email
                        })]
                      })
                    }), Object(Dt.jsx)("p", {
                      children: Object(Dt.jsxs)("span", {
                        className: "push-to-right",
                        children: [Object(Dt.jsx)("strong", {
                          children: "Adresa: "
                        }), b.shippingAddress.address, ", ", b.shippingAddress.city, " ", b.shippingAddress.postalCode, ",", " ", b.shippingAddress.phone]
                      })
                    }), b.isDelivered ? Object(Dt.jsxs)(Zt, {
                      variant: "success",
                      children: ["Dostavljeno dana ", La()(b.deliveredAt).format("DD.MM.yyyy.")]
                    }) : Object(Dt.jsx)(Zt, {
                      variant: "danger",
                      children: "Nije dostavljeno"
                    })]
                  }), Object(Dt.jsxs)(pa.a.Item, {
                    children: [Object(Dt.jsx)("h2", {
                      children: "Na\u010din pla\u0107anja"
                    }), Object(Dt.jsx)("p", {
                      children: Object(Dt.jsxs)("span", {
                        className: "push-to-right",
                        children: [Object(Dt.jsx)("strong", {
                          children: "Metod: "
                        }), b.paymentMethod]
                      })
                    }), b.isPaid ? Object(Dt.jsxs)(Zt, {
                      variant: "success",
                      children: ["Pla\u0107eno dana ", La()(b.paidAt).format("DD.MM.yyyy.")]
                    }) : Object(Dt.jsx)(Zt, {
                      variant: "danger",
                      children: "Nije pla\u0107eno"
                    })]
                  }), Object(Dt.jsxs)(pa.a.Item, {
                    children: [Object(Dt.jsx)("h2", {
                      children: "Poru\u010deni proizvodi"
                    }), 0 === b.orderItems.length ? Object(Dt.jsx)(Zt, {
                      children: "Va\u0161a porud\u017ebina je prazna"
                    }) : Object(Dt.jsx)(pa.a, {
                      variant: "flush",
                      children: b.orderItems.map(function (e, t) {
                        return Object(Dt.jsx)(pa.a.Item, {
                          children: Object(Dt.jsxs)(Qt.a, {
                            children: [Object(Dt.jsx)(Wt.a, {
                              md: 1,
                              children: Object(Dt.jsx)(ca.a, {
                                src: e.image,
                                alt: e.name,
                                fluid: !0,
                                rounded: !0
                              })
                            }), Object(Dt.jsx)(Wt.a, {
                              children: Object(Dt.jsx)(Et.Link, {
                                to: "/product/".concat(e.product),
                                children: e.name
                              })
                            }), Object(Dt.jsxs)(Wt.a, {
                              md: 4,
                              children: [e.qty, " x ", e.price, " din = ", e.qty * e.price, " din"]
                            })]
                          })
                        }, t);
                      })
                    })]
                  }), Object(Dt.jsx)(pa.a.Item, {
                    children: Object(Dt.jsx)(Qt.a, {
                      children: Object(Dt.jsx)("h2", {
                        children: Object(Dt.jsx)(za.PDFDownloadLink, {
                          document: Object(Dt.jsx)(Ma, {
                            order: b
                          }),
                          fileName: c + ".pdf",
                          children: function (e) {
                            e.blob, e.url;
                            var t = e.loading;
                            e.error;
                            return t ? "U\u010ditavanje fakture..." : "Preuzmite fakturu";
                          }
                        })
                      })
                    })
                  })]
                })
              }), Object(Dt.jsx)(Wt.a, {
                md: 4,
                children: Object(Dt.jsx)(Ht.a, {
                  children: Object(Dt.jsxs)(pa.a, {
                    variant: "flush",
                    children: [Object(Dt.jsx)(pa.a.Item, {
                      children: Object(Dt.jsx)("h2", {
                        children: "Pregled porud\u017ebine"
                      })
                    }), Object(Dt.jsx)(pa.a.Item, {
                      className: "push-to-right",
                      children: Object(Dt.jsxs)(Qt.a, {
                        children: [Object(Dt.jsx)(Wt.a, {
                          children: "Proizvodi"
                        }), Object(Dt.jsxs)(Wt.a, {
                          children: [b.itemsPrice, " din"]
                        })]
                      })
                    }), Object(Dt.jsx)(pa.a.Item, {
                      className: "push-to-right",
                      children: Object(Dt.jsxs)(Qt.a, {
                        children: [Object(Dt.jsx)(Wt.a, {
                          children: "Po\u0161tarina"
                        }), Object(Dt.jsxs)(Wt.a, {
                          children: [b.shippingPrice, " din"]
                        })]
                      })
                    }), Object(Dt.jsx)(pa.a.Item, {
                      className: "push-to-right",
                      children: Object(Dt.jsxs)(Qt.a, {
                        children: [Object(Dt.jsx)(Wt.a, {
                          children: Object(Dt.jsx)("strong", {
                            children: "Ukupno"
                          })
                        }), Object(Dt.jsx)(Wt.a, {
                          children: Object(Dt.jsxs)("strong", {
                            children: [b.totalPrice, " din"]
                          })
                        })]
                      })
                    }), O.isAdmin && !b.isPaid && Object(Dt.jsxs)(pa.a.Item, {
                      children: [m && Object(Dt.jsx)(ta, {}), Object(Dt.jsx)(At.a, {
                        type: "button",
                        className: "btn btn-block",
                        onClick: function (e) {
                          i(Ca(c));
                        },
                        children: "Ozna\u010di kao pla\u0107eno"
                      })]
                    }), v && Object(Dt.jsx)(ta, {}), O.isAdmin && b.isPaid && !b.isDelivered && Object(Dt.jsx)(pa.a.Item, {
                      children: Object(Dt.jsx)(At.a, {
                        type: "button",
                        className: "btn btn-block",
                        onClick: function () {
                          i(Ia(b));
                        },
                        children: "Ozna\u010di kao dostavljeno"
                      })
                    })]
                  })
                })
              })]
            })]
          })
        });
      },
      Va = function (e) {
        var t = e.history,
          a = Object(r.useState)(!1),
          n = Object(Rt.a)(a, 2),
          c = n[0],
          i = n[1],
          o = Object(r.useState)(""),
          l = Object(Rt.a)(o, 2),
          d = l[0],
          j = l[1],
          u = Object(s.b)(),
          b = Object(s.c)(function (e) {
            return e.userList;
          }),
          h = b.loading,
          p = b.error,
          O = b.users,
          x = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          m = Object(s.c)(function (e) {
            return e.userDelete;
          }).success;
        Object(r.useEffect)(function () {
          var e;
          x && x.isAdmin ? u((e = d, function () {
            var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
              var n, c, s, i, o;
              return Object(Pt.a)().wrap(function (t) {
                for (;;) switch (t.prev = t.next) {
                  case 0:
                    return t.prev = 0, a({
                      type: Ie
                    }), n = r(), c = n.userLogin.userInfo, s = {
                      headers: {
                        Authorization: "Bearer ".concat(c.token)
                      }
                    }, t.next = 6, Ft.a.get("/api/users?keyword=".concat(e), s);
                  case 6:
                    i = t.sent, o = i.data, a({
                      type: _e,
                      payload: o
                    }), t.next = 14;
                    break;
                  case 11:
                    t.prev = 11, t.t0 = t.catch(0), a({
                      type: Le,
                      payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                    });
                  case 14:
                  case "end":
                    return t.stop();
                }
              }, t, null, [[0, 11]]);
            }));
            return function (e, a) {
              return t.apply(this, arguments);
            };
          }())) : t.push("/login");
        }, [u, x, t, m, c]);
        var g = function (e) {
          window.confirm("Are you sure?") && u(function (e) {
            return function () {
              var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                var n, c, s;
                return Object(Pt.a)().wrap(function (t) {
                  for (;;) switch (t.prev = t.next) {
                    case 0:
                      return t.prev = 0, a({
                        type: Re
                      }), n = r(), c = n.userLogin.userInfo, s = {
                        headers: {
                          Authorization: "Bearer ".concat(c.token)
                        }
                      }, t.next = 6, Ft.a.delete("/api/users/".concat(e), s);
                    case 6:
                      a({
                        type: Te
                      }), t.next = 12;
                      break;
                    case 9:
                      t.prev = 9, t.t0 = t.catch(0), a({
                        type: Ae,
                        payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                      });
                    case 12:
                    case "end":
                      return t.stop();
                  }
                }, t, null, [[0, 9]]);
              }));
              return function (e, a) {
                return t.apply(this, arguments);
              };
            }();
          }(e));
        };
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)("p", {}), Object(Dt.jsxs)("div", {
            className: "input-group",
            children: [Object(Dt.jsx)("input", {
              className: "form-control",
              type: "text",
              placeholder: "Pretraga kupaca...",
              "aria-label": "search input",
              onChange: function (e) {
                return j(e.target.value);
              },
              value: d
            }), Object(Dt.jsx)("button", {
              className: "btn btn-primary",
              type: "button",
              onClick: function () {
                i(!c);
              },
              children: Object(Dt.jsx)("i", {
                className: "fas fa-search fa-sm"
              })
            })]
          }), Object(Dt.jsx)("p", {}), h ? Object(Dt.jsx)(ta, {}) : p ? Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: p
          }) : Object(Dt.jsxs)(Ea.a, {
            bordered: !0,
            hover: !0,
            responsive: !0,
            className: "table-sm",
            children: [Object(Dt.jsx)("thead", {
              children: Object(Dt.jsxs)("tr", {
                children: [Object(Dt.jsx)("th", {
                  children: "Li\u010dni kod"
                }), Object(Dt.jsx)("th", {
                  children: "Ime i prezime"
                }), Object(Dt.jsx)("th", {
                  children: "Email"
                }), Object(Dt.jsx)("th", {
                  children: "Admin"
                }), Object(Dt.jsx)("th", {
                  children: "Izmena"
                }), Object(Dt.jsx)("th", {
                  children: "Brisanje"
                }), Object(Dt.jsx)("th", {
                  children: "Porud\u017ebine"
                })]
              })
            }), Object(Dt.jsx)("tbody", {
              children: O.map(function (e) {
                return Object(Dt.jsxs)("tr", {
                  children: [Object(Dt.jsx)("td", {
                    children: e.personalCode
                  }), Object(Dt.jsx)("td", {
                    children: e.name
                  }), Object(Dt.jsx)("td", {
                    children: Object(Dt.jsx)("a", {
                      href: "mailto:".concat(e.email),
                      children: e.email
                    })
                  }), Object(Dt.jsx)("td", {
                    children: e.isAdmin ? Object(Dt.jsx)("i", {
                      className: "fas fa-check",
                      style: {
                        color: "green"
                      }
                    }) : Object(Dt.jsx)("i", {
                      className: "fas fa-times",
                      style: {
                        color: "red"
                      }
                    })
                  }), Object(Dt.jsx)("td", {
                    children: Object(Dt.jsx)(It.LinkContainer, {
                      to: "/admin/user/".concat(e._id, "/edit"),
                      children: Object(Dt.jsx)(At.a, {
                        variant: "light",
                        className: "btn-sm",
                        children: Object(Dt.jsx)("i", {
                          className: "fas fa-edit"
                        })
                      })
                    })
                  }), Object(Dt.jsx)("td", {
                    children: Object(Dt.jsx)(At.a, {
                      variant: "danger",
                      className: "btn-sm",
                      onClick: function () {
                        return g(e._id);
                      },
                      children: Object(Dt.jsx)("i", {
                        className: "fas fa-trash"
                      })
                    })
                  }), Object(Dt.jsx)("td", {
                    children: Object(Dt.jsx)(It.LinkContainer, {
                      to: "/admin/userorders/".concat(e._id),
                      children: Object(Dt.jsx)(At.a, {
                        variant: "light",
                        className: "btn-sm",
                        children: "Detalji"
                      })
                    })
                  })]
                }, e._id);
              })
            })]
          })]
        });
      },
      Ya = function (e) {
        var t = e.match,
          a = e.history,
          n = t.params.id,
          c = Object(r.useState)(""),
          i = Object(Rt.a)(c, 2),
          o = i[0],
          l = i[1],
          d = Object(r.useState)(""),
          j = Object(Rt.a)(d, 2),
          u = j[0],
          b = j[1],
          h = Object(r.useState)(""),
          p = Object(Rt.a)(h, 2),
          O = p[0],
          x = p[1],
          m = Object(r.useState)(""),
          g = Object(Rt.a)(m, 2),
          f = g[0],
          v = g[1],
          y = Object(r.useState)(""),
          S = Object(Rt.a)(y, 2),
          N = S[0],
          E = S[1],
          k = Object(r.useState)(""),
          C = Object(Rt.a)(k, 2),
          I = C[0],
          _ = C[1],
          L = Object(r.useState)(!1),
          w = Object(Rt.a)(L, 2),
          R = w[0],
          T = w[1],
          A = Object(s.b)(),
          D = Object(s.c)(function (e) {
            return e.userDetails;
          }),
          P = D.loading,
          U = D.error,
          z = D.user,
          F = Object(s.c)(function (e) {
            return e.userUpdate;
          }),
          G = F.loading,
          M = F.error,
          B = F.success;
        Object(r.useEffect)(function () {
          B ? (A({
            type: ze
          }), a.push("/admin/userlist")) : z.name && z._id === n ? (l(z.name), b(z.address), x(z.city), v(z.postcode), E(z.phone), _(z.email), T(z.isAdmin)) : A(Mt(n));
        }, [B, A, a, z, n]);
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(Et.Link, {
            to: "/admin/userlist",
            className: "btn btn-light my-3",
            children: "Nazad"
          }), Object(Dt.jsxs)(va, {
            children: [Object(Dt.jsx)("h1", {
              children: "Promena podataka kupca"
            }), G && Object(Dt.jsx)(ta, {}), M && Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: M
            }), P ? Object(Dt.jsx)(ta, {}) : U ? Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: U
            }) : Object(Dt.jsxs)(Tt.a, {
              onSubmit: function (e) {
                e.preventDefault(), A(Vt({
                  _id: n,
                  name: o,
                  address: u,
                  city: O,
                  postcode: f,
                  phone: N,
                  email: I,
                  isAdmin: R
                }));
              },
              children: [Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "email",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Ime i prezime"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Ime i prezime",
                  value: o,
                  onChange: function (e) {
                    return l(e.target.value);
                  }
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "address",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Adresa"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Unesite adresu",
                  value: u,
                  onChange: function (e) {
                    return b(e.target.value);
                  }
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "city",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Grad"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Unesite grad",
                  value: O,
                  onChange: function (e) {
                    return x(e.target.value);
                  }
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "postcode",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Po\u0161tanski broj"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Unesite po\u0161tanski broj",
                  value: f,
                  onChange: function (e) {
                    return v(e.target.value);
                  }
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "phone",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Broj telefona"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Unesite broj telefona",
                  value: N,
                  onChange: function (e) {
                    return E(e.target.value);
                  }
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "email",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Email adresa"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "email",
                  placeholder: "Email",
                  value: I,
                  onChange: function (e) {
                    return _(e.target.value);
                  }
                })]
              }), Object(Dt.jsx)(Tt.a.Group, {
                controlId: "isadmin",
                children: Object(Dt.jsx)(Tt.a.Check, {
                  type: "checkbox",
                  label: "Administrator",
                  checked: R,
                  onChange: function (e) {
                    return T(e.target.checked);
                  }
                })
              }), Object(Dt.jsx)(At.a, {
                type: "submit",
                variant: "primary",
                children: "Sa\u010duvaj promene"
              })]
            })]
          })]
        });
      },
      Qa = function (e) {
        var t = e.history,
          a = e.match.params.pageNumber || 1,
          n = Object(s.b)(),
          c = Object(s.c)(function (e) {
            return e.categoryList;
          }).categories,
          i = Object(s.c)(function (e) {
            return e.productList;
          }),
          o = i.loading,
          l = i.error,
          d = i.products,
          j = i.page,
          u = i.pages,
          b = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          h = Object(s.c)(function (e) {
            return e.productDelete;
          }),
          p = h.loading,
          O = h.error,
          f = h.success,
          y = Object(s.c)(function (e) {
            return e.productCreate;
          }),
          N = y.loading,
          E = y.error,
          k = y.success,
          C = y.product;
        Object(r.useEffect)(function () {
          n({
            type: S
          }), b && b.isAdmin || t.push("/login"), k && null != C ? t.push("/admin/product/".concat(C._id, "/edit")) : k && null == C ? t.push("/admin/product/".concat(0, "/edit")) : (n(sa("", a)), n(ua("")));
        }, [n, b, t, f, k, C, a]);
        var I = function (e) {
            window.confirm("Are you sure?") && n(function (e) {
              return function () {
                var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                  var n, c, s;
                  return Object(Pt.a)().wrap(function (t) {
                    for (;;) switch (t.prev = t.next) {
                      case 0:
                        return t.prev = 0, a({
                          type: x
                        }), n = r(), c = n.userLogin.userInfo, s = {
                          headers: {
                            Authorization: "Bearer ".concat(c.token)
                          }
                        }, t.next = 6, Ft.a.delete("/api/products/".concat(e), s);
                      case 6:
                        a({
                          type: m
                        }), t.next = 12;
                        break;
                      case 9:
                        t.prev = 9, t.t0 = t.catch(0), a({
                          type: g,
                          payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                        });
                      case 12:
                      case "end":
                        return t.stop();
                    }
                  }, t, null, [[0, 9]]);
                }));
                return function (e, a) {
                  return t.apply(this, arguments);
                };
              }();
            }(e));
          },
          _ = function (e) {
            if (c && c.length > 0) {
              var t = c.find(function (t) {
                return t._id == e;
              });
              return console.log(t), t.name;
            }
            return "";
          };
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(Qt.a, {
            className: "align-items-center",
            children: Object(Dt.jsx)(Wt.a, {
              className: "text-right",
              children: Object(Dt.jsxs)(At.a, {
                className: "my-3",
                onClick: function () {
                  n({
                    type: v,
                    payload: null
                  });
                },
                children: [Object(Dt.jsx)("span", {
                  className: "plus-sign-margin",
                  children: Object(Dt.jsx)("i", {
                    className: "fas fa-plus"
                  })
                }), "Novi proizvod"]
              })
            })
          }), N && Object(Dt.jsx)(ta, {}), E && Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: E
          }), p && Object(Dt.jsx)(ta, {}), O && Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: O
          }), o ? Object(Dt.jsx)(ta, {}) : l ? Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: l
          }) : Object(Dt.jsxs)(Dt.Fragment, {
            children: [Object(Dt.jsxs)(Ea.a, {
              bordered: !0,
              hover: !0,
              responsive: !0,
              className: "table-sm",
              children: [Object(Dt.jsx)("thead", {
                children: Object(Dt.jsxs)("tr", {
                  children: [Object(Dt.jsx)("th", {
                    children: "Naziv"
                  }), Object(Dt.jsx)("th", {
                    children: "Cena"
                  }), Object(Dt.jsx)("th", {
                    children: "Kategorija"
                  }), Object(Dt.jsx)("th", {
                    children: "Brend"
                  }), Object(Dt.jsx)("th", {
                    children: "Izmena"
                  }), Object(Dt.jsx)("th", {
                    children: "Brisanje"
                  })]
                })
              }), Object(Dt.jsx)("tbody", {
                children: d.map(function (e) {
                  return Object(Dt.jsxs)("tr", {
                    children: [Object(Dt.jsx)("td", {
                      children: e.name
                    }), Object(Dt.jsxs)("td", {
                      children: [e.price, " din"]
                    }), Object(Dt.jsx)("td", {
                      children: _(e.category)
                    }), Object(Dt.jsx)("td", {
                      children: e.brand
                    }), Object(Dt.jsx)("td", {
                      children: Object(Dt.jsx)(It.LinkContainer, {
                        to: "/admin/product/".concat(e._id, "/edit"),
                        children: Object(Dt.jsx)(At.a, {
                          variant: "light",
                          className: "btn-sm",
                          children: Object(Dt.jsx)("i", {
                            className: "fas fa-edit"
                          })
                        })
                      })
                    }), Object(Dt.jsx)("td", {
                      children: Object(Dt.jsx)(At.a, {
                        variant: "danger",
                        className: "btn-sm",
                        onClick: function () {
                          return I(e._id);
                        },
                        children: Object(Dt.jsx)("i", {
                          className: "fas fa-trash"
                        })
                      })
                    })]
                  }, e._id);
                })
              })]
            }), Object(Dt.jsx)(ra, {
              pages: u,
              page: j,
              isAdmin: !0
            })]
          })]
        });
      },
      Wa = function (e) {
        var t = e.match,
          a = e.history,
          n = t.params.id,
          c = Object(r.useState)(""),
          i = Object(Rt.a)(c, 2),
          o = i[0],
          l = i[1],
          d = Object(r.useState)(0),
          j = Object(Rt.a)(d, 2),
          u = j[0],
          b = j[1],
          h = Object(r.useState)(""),
          p = Object(Rt.a)(h, 2),
          O = p[0],
          x = p[1],
          m = Object(r.useState)(""),
          g = Object(Rt.a)(m, 2),
          I = g[0],
          _ = g[1],
          L = Object(r.useState)(0),
          w = Object(Rt.a)(L, 2),
          R = w[0],
          T = w[1],
          A = Object(r.useState)(),
          D = Object(Rt.a)(A, 2),
          P = D[0],
          U = D[1],
          z = Object(r.useState)(),
          F = Object(Rt.a)(z, 2),
          G = F[0],
          M = F[1],
          B = Object(r.useState)(),
          V = Object(Rt.a)(B, 2),
          Y = (V[0], V[1]),
          Q = Object(r.useState)(""),
          W = Object(Rt.a)(Q, 2),
          q = W[0],
          H = W[1],
          K = Object(r.useState)(!1),
          J = Object(Rt.a)(K, 2),
          $ = J[0],
          X = J[1],
          Z = Object(s.b)(),
          ee = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          te = Object(s.c)(function (e) {
            return e.categoryList;
          }),
          ae = te.loading,
          re = (te.error, te.categories),
          ne = Object(s.c)(function (e) {
            return e.productDetails;
          }),
          ce = ne.loading,
          se = ne.error,
          ie = ne.product,
          oe = Object(s.c)(function (e) {
            return e.productUpdate;
          }),
          le = oe.loading,
          de = oe.error,
          je = oe.success,
          ue = Object(s.c)(function (e) {
            return e.productCreate;
          }),
          be = (ue.loading, ue.error, ue.success);
        Object(r.useEffect)(function () {
          if (je) Z({
            type: C
          }), Z(ua()), Z(ia(n)), a.push("/admin/productlist");else if (be) Z({
            type: S
          }), a.push("/admin/productlist");else {
            re && 0 != re.length || Z(ua(""));
            var e = [];
            re && (re.map(function (t) {
              return e.push({
                label: t.name,
                value: t._id
              });
            }), M(e)), ie.name && ie._id === n || "0" == n ? "0" == n ? (l(""), b(0), x(""), _(""), T(0), U(), H("")) : (l(ie.name), b(ie.price), x(ie.image), _(ie.brand), T(ie.countInStock), pe(ie.category), H(ie.description)) : Z(ia(n));
          }
        }, [Z, je, be, a, re, ie, n]);
        var he = function () {
            var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t) {
              var a, r, n, c, s;
              return Object(Pt.a)().wrap(function (e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    return a = t.target.files[0], (r = new FormData()).append("image", a), X(!0), e.prev = 4, n = {
                      headers: {
                        "Content-Type": "multipart/form-data"
                      }
                    }, e.next = 8, Ft.a.post("/api/upload", r, n);
                  case 8:
                    c = e.sent, s = c.data, x(s), X(!1), e.next = 18;
                    break;
                  case 14:
                    e.prev = 14, e.t0 = e.catch(4), console.error(e.t0), X(!1);
                  case 18:
                  case "end":
                    return e.stop();
                }
              }, e, null, [[4, 14]]);
            }));
            return function (t) {
              return e.apply(this, arguments);
            };
          }(),
          pe = function () {
            var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t) {
              var a, r;
              return Object(Pt.a)().wrap(function (e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    (a = re.filter(function (e) {
                      return e._id == t;
                    })[0]) && (r = {
                      label: a.name,
                      value: a._id
                    }, U(t), Y(r));
                  case 2:
                  case "end":
                    return e.stop();
                }
              }, e);
            }));
            return function (t) {
              return e.apply(this, arguments);
            };
          }();
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(Et.Link, {
            to: "/admin/productlist",
            className: "btn btn-light my-3",
            children: "Nazad"
          }), Object(Dt.jsxs)(va, {
            children: [Object(Dt.jsx)("h1", {
              children: "Podaci proizvoda"
            }), le && Object(Dt.jsx)(ta, {}), de && Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: de
            }), ce && ae ? Object(Dt.jsx)(ta, {}) : se ? Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: se
            }) : Object(Dt.jsxs)(Tt.a, {
              onSubmit: function (e) {
                e.preventDefault(), "0" != n ? Z(function (e) {
                  return function () {
                    var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                      var n, c, s, i, o;
                      return Object(Pt.a)().wrap(function (t) {
                        for (;;) switch (t.prev = t.next) {
                          case 0:
                            return t.prev = 0, a({
                              type: N
                            }), n = r(), c = n.userLogin.userInfo, s = {
                              headers: {
                                "Content-Type": "application/json",
                                Authorization: "Bearer ".concat(c.token)
                              }
                            }, t.next = 6, Ft.a.put("/api/products/".concat(e._id), e, s);
                          case 6:
                            i = t.sent, o = i.data, a({
                              type: E,
                              payload: o
                            }), t.next = 14;
                            break;
                          case 11:
                            t.prev = 11, t.t0 = t.catch(0), a({
                              type: k,
                              payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                            });
                          case 14:
                          case "end":
                            return t.stop();
                        }
                      }, t, null, [[0, 11]]);
                    }));
                    return function (e, a) {
                      return t.apply(this, arguments);
                    };
                  }();
                }({
                  _id: n,
                  name: o,
                  price: u,
                  image: O,
                  brand: I,
                  countInStock: R,
                  category: P,
                  description: q
                })) : (console.log(ie), Z(function (e) {
                  return function () {
                    var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                      var n, c, s, i, o;
                      return Object(Pt.a)().wrap(function (t) {
                        for (;;) switch (t.prev = t.next) {
                          case 0:
                            return t.prev = 0, a({
                              type: f
                            }), n = r(), c = n.userLogin.userInfo, s = {
                              headers: {
                                Authorization: "Bearer ".concat(c.token)
                              }
                            }, t.next = 6, Ft.a.post("/api/products", e, s);
                          case 6:
                            i = t.sent, o = i.data, a({
                              type: v,
                              payload: o
                            }), t.next = 14;
                            break;
                          case 11:
                            t.prev = 11, t.t0 = t.catch(0), a({
                              type: y,
                              payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                            });
                          case 14:
                          case "end":
                            return t.stop();
                        }
                      }, t, null, [[0, 11]]);
                    }));
                    return function (e, a) {
                      return t.apply(this, arguments);
                    };
                  }();
                }({
                  name: o,
                  price: u,
                  userInfo: ee,
                  image: O,
                  brand: I,
                  category: P,
                  countInStock: R,
                  description: q
                })));
              },
              children: [Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "email",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Naziv"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Unesite naziv proizvoda",
                  value: o,
                  onChange: function (e) {
                    return l(e.target.value);
                  }
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "price",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Cena"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "number",
                  min: "0",
                  step: "0.01",
                  placeholder: "Unesite cenu",
                  value: u,
                  onChange: function (e) {
                    return b(e.target.value);
                  }
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "image",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Fotografija"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Putanja do fotografije proizvoda",
                  value: O,
                  onChange: function (e) {
                    return x(e.target.value);
                  }
                }), Object(Dt.jsx)(Tt.a.File, {
                  id: "image-file",
                  accept: "image/*",
                  label: "Odaberite fotografiju",
                  custom: !0,
                  onChange: he
                }), $ && Object(Dt.jsx)(ta, {})]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "brand",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Brend"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Naziv brenda",
                  value: I,
                  onChange: function (e) {
                    return _(e.target.value);
                  }
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "countInStock",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Koli\u010dina na zalihama"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "number",
                  min: "0",
                  step: "1",
                  placeholder: "Unesite koli\u010dinu zaliha",
                  value: R,
                  onChange: function (e) {
                    return T(e.target.value);
                  }
                })]
              }), G && Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "category",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Kategorija"
                }), Object(Dt.jsx)("select", {
                  placeholder: "Odaberite kategoriju",
                  options: G,
                  value: P,
                  onChange: function (e) {
                    return pe(e.target.value);
                  },
                  children: G.map(function (e) {
                    return Object(Dt.jsx)("option", {
                      value: e.value,
                      children: e.label
                    }, e.value);
                  })
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "description",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Opis"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Opis proizvoda",
                  value: q,
                  onChange: function (e) {
                    return H(e.target.value);
                  }
                })]
              }), Object(Dt.jsx)(At.a, {
                type: "submit",
                variant: "primary",
                children: "Snimanje"
              })]
            })]
          })]
        });
      },
      qa = function (e) {
        var t = e.history,
          a = Object(s.b)(),
          n = Object(s.c)(function (e) {
            return e.orderList;
          }),
          c = n.loading,
          i = n.error,
          o = n.orders,
          l = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo;
        return Object(r.useEffect)(function () {
          l && l.isAdmin ? a(function () {
            var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t, a) {
              var r, n, c, s, i;
              return Object(Pt.a)().wrap(function (e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    return e.prev = 0, t({
                      type: lt
                    }), r = a(), n = r.userLogin.userInfo, c = {
                      headers: {
                        Authorization: "Bearer ".concat(n.token)
                      }
                    }, e.next = 6, Ft.a.get("/api/orders", c);
                  case 6:
                    s = e.sent, i = s.data, t({
                      type: dt,
                      payload: i
                    }), e.next = 14;
                    break;
                  case 11:
                    e.prev = 11, e.t0 = e.catch(0), t({
                      type: jt,
                      payload: e.t0.response && e.t0.response.data.message ? e.t0.response.data.message : e.t0.message
                    });
                  case 14:
                  case "end":
                    return e.stop();
                }
              }, e, null, [[0, 11]]);
            }));
            return function (t, a) {
              return e.apply(this, arguments);
            };
          }()) : t.push("/login");
        }, [a, l, t]), Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)("p", {}), c ? Object(Dt.jsx)(ta, {}) : i ? Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: i
          }) : Object(Dt.jsxs)(Ea.a, {
            bordered: !0,
            hover: !0,
            responsive: !0,
            className: "table-sm",
            children: [Object(Dt.jsx)("thead", {
              children: Object(Dt.jsxs)("tr", {
                children: [Object(Dt.jsx)("th", {
                  children: "Br. profakture"
                }), Object(Dt.jsx)("th", {
                  children: "Kupac"
                }), Object(Dt.jsx)("th", {
                  children: "Datum"
                }), Object(Dt.jsx)("th", {
                  children: "Ukupno"
                }), Object(Dt.jsx)("th", {
                  children: "Pla\u0107eno"
                }), Object(Dt.jsx)("th", {
                  children: "Dostavljeno"
                }), Object(Dt.jsx)("th", {
                  children: "Informacije"
                })]
              })
            }), Object(Dt.jsx)("tbody", {
              children: o.map(function (e) {
                return Object(Dt.jsxs)("tr", {
                  children: [Object(Dt.jsx)("td", {
                    children: e.orderNo
                  }), Object(Dt.jsx)("td", {
                    children: e.user && e.user.name
                  }), Object(Dt.jsx)("td", {
                    children: La()(e.createdAt).format("DD.MM.yyyy.")
                  }), Object(Dt.jsxs)("td", {
                    children: [e.totalPrice, " din"]
                  }), Object(Dt.jsx)("td", {
                    children: e.isPaid ? La()(e.paidAt).format("DD.MM.yyyy.") : Object(Dt.jsx)("i", {
                      className: "fas fa-times",
                      style: {
                        color: "red"
                      }
                    })
                  }), Object(Dt.jsx)("td", {
                    children: e.isDelivered ? La()(e.deliveredAt).format("DD.MM.yyyy.") : Object(Dt.jsx)("i", {
                      className: "fas fa-times",
                      style: {
                        color: "red"
                      }
                    })
                  }), Object(Dt.jsx)("td", {
                    children: Object(Dt.jsx)(It.LinkContainer, {
                      to: "/admin/order/".concat(e._id),
                      children: Object(Dt.jsx)(At.a, {
                        className: "btn-sm",
                        variant: "light",
                        children: "Detalji"
                      })
                    })
                  })]
                }, e._id);
              })
            })]
          })]
        });
      },
      Ha = (a(573), a(571), function (e) {
        var t = e.match,
          a = e.history,
          n = t.params.id,
          c = Object(r.useState)(""),
          i = Object(Rt.a)(c, 2),
          o = i[0],
          l = i[1],
          d = Object(r.useState)(""),
          j = Object(Rt.a)(d, 2),
          u = j[0],
          b = j[1],
          h = Object(r.useState)(""),
          p = Object(Rt.a)(h, 2),
          O = p[0],
          x = p[1],
          m = Object(r.useState)(""),
          g = Object(Rt.a)(m, 2),
          f = g[0],
          v = g[1],
          y = Object(r.useState)(0),
          S = Object(Rt.a)(y, 2),
          N = S[0],
          E = S[1],
          k = Object(r.useState)(""),
          C = Object(Rt.a)(k, 2),
          I = (C[0], C[1]),
          _ = Object(s.b)(),
          L = Object(s.c)(function (e) {
            return e.userDetails;
          }),
          w = L.loading,
          R = (L.error, L.user),
          T = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          A = (Object(s.c)(function (e) {
            return e.userUpdateProfile;
          }).success, Object(s.c)(function (e) {
            return e.referralDetails;
          })),
          D = (A.loading, A.success, A.referral),
          P = Object(s.c)(function (e) {
            return e.orderListUser;
          }),
          U = P.loading,
          z = P.error,
          F = P.orders;
        Object(r.useEffect)(function () {
          var e;
          T ? P && O == n ? (l(R.name), b(R.email), v(R.referralCode), v(R.personalCode), E(R.referralPoints || 0), I(""), D && D._id && R.referralCode) : (_(Mt(n)), _((e = n, function () {
            var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
              var n, c, s, i, o;
              return Object(Pt.a)().wrap(function (t) {
                for (;;) switch (t.prev = t.next) {
                  case 0:
                    return t.prev = 0, a({
                      type: lt
                    }), n = r(), c = n.userLogin.userInfo, s = {
                      headers: {
                        Authorization: "Bearer ".concat(c.token)
                      }
                    }, t.next = 6, Ft.a.get("/api/orders/".concat(e, "/userorders"), s);
                  case 6:
                    i = t.sent, o = i.data, a({
                      type: dt,
                      payload: o
                    }), t.next = 14;
                    break;
                  case 11:
                    t.prev = 11, t.t0 = t.catch(0), a({
                      type: jt,
                      payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                    });
                  case 14:
                  case "end":
                    return t.stop();
                }
              }, t, null, [[0, 11]]);
            }));
            return function (e, a) {
              return t.apply(this, arguments);
            };
          }())), x(n)) : a.push("/login");
        }, [_, O, L, A, P]);
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(Et.Link, {
            to: "/admin/userlist",
            className: "btn btn-light my-3",
            children: "Nazad"
          }), Object(Dt.jsxs)(Qt.a, {
            children: [Object(Dt.jsxs)(Wt.a, {
              md: 3,
              children: [Object(Dt.jsx)("h2", {
                children: "Detalji kupca"
              }), w && Object(Dt.jsx)(ta, {}), Object(Dt.jsxs)(Tt.a, {
                className: "push-to-right",
                children: [Object(Dt.jsxs)(Tt.a.Group, {
                  controlId: "email",
                  children: [Object(Dt.jsx)(Tt.a.Label, {
                    children: "Ime i prezime"
                  }), Object(Dt.jsx)(Tt.a.Control, {
                    type: "name",
                    placeholder: "Unesite ime i prezime",
                    value: o,
                    readOnly: !0,
                    onChange: function (e) {
                      return l(e.target.value);
                    }
                  })]
                }), Object(Dt.jsxs)(Tt.a.Group, {
                  controlId: "email",
                  children: [Object(Dt.jsx)(Tt.a.Label, {
                    children: "Email adresa"
                  }), Object(Dt.jsx)(Tt.a.Control, {
                    type: "email",
                    placeholder: "Unesite email adresu",
                    value: u,
                    readOnly: !0,
                    onChange: function (e) {
                      return b(e.target.value);
                    }
                  })]
                })]
              }), Object(Dt.jsxs)("h3", {
                children: ["Li\u010dni kod: ", f]
              }), Object(Dt.jsxs)("h3", {
                children: ["Trenutna zarada: ", N, " dinara"]
              })]
            }), Object(Dt.jsxs)(Wt.a, {
              md: 9,
              children: [Object(Dt.jsx)("h2", {
                children: "Sve porud\u017ebine"
              }), U ? Object(Dt.jsx)(ta, {}) : z ? Object(Dt.jsx)(Zt, {
                variant: "danger",
                children: z
              }) : Object(Dt.jsxs)(Ea.a, {
                bordered: !0,
                hover: !0,
                responsive: !0,
                className: "table-sm",
                children: [Object(Dt.jsx)("thead", {
                  children: Object(Dt.jsxs)("tr", {
                    children: [Object(Dt.jsx)("th", {
                      children: "Broj"
                    }), Object(Dt.jsx)("th", {
                      children: "Datum"
                    }), Object(Dt.jsx)("th", {
                      children: "Ukupno"
                    }), Object(Dt.jsx)("th", {
                      children: "Pla\u0107eno"
                    }), Object(Dt.jsx)("th", {
                      children: "Isporu\u010deno"
                    }), Object(Dt.jsx)("th", {
                      children: "Informacije"
                    })]
                  })
                }), Object(Dt.jsx)("tbody", {
                  children: F.map(function (e) {
                    return Object(Dt.jsxs)("tr", {
                      children: [Object(Dt.jsx)("td", {
                        children: e.orderNr
                      }), Object(Dt.jsx)("td", {
                        children: La()(e.createdAt).format("DD.MM.yyyy.")
                      }), Object(Dt.jsxs)("td", {
                        children: [e.totalPrice, " din"]
                      }), Object(Dt.jsx)("td", {
                        children: e.isPaid ? La()(e.paidAt).format("DD.MM.yyyy.") : Object(Dt.jsx)("i", {
                          className: "fas fa-times",
                          style: {
                            color: "red"
                          }
                        })
                      }), Object(Dt.jsx)("td", {
                        children: e.isDelivered ? La()(e.deliveredAt).format("DD.MM.yyyy.") : Object(Dt.jsx)("i", {
                          className: "fas fa-times",
                          style: {
                            color: "red"
                          }
                        })
                      }), Object(Dt.jsx)("td", {
                        children: Object(Dt.jsx)(It.LinkContainer, {
                          to: "/order/".concat(e._id),
                          children: Object(Dt.jsx)(At.a, {
                            className: "btn-sm",
                            variant: "light",
                            children: "Detalji"
                          })
                        })
                      })]
                    }, e._id);
                  })
                })]
              })]
            })]
          })]
        });
      }),
      Ka = function (e) {
        var t,
          a = e.match,
          n = e.history,
          c = a.params.id,
          i = Object(s.b)(),
          o = Object(s.c)(function (e) {
            return e.orderDetails;
          }),
          l = o.order,
          d = o.loading,
          j = o.error,
          u = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          b = Object(s.c)(function (e) {
            return e.referralDetails;
          }),
          h = (b.loading, b.success, b.referral),
          p = Object(s.c)(function (e) {
            return e.orderPay;
          }),
          O = p.loading,
          x = p.success,
          m = Object(s.c)(function (e) {
            return e.orderDeliver;
          }),
          g = m.loading,
          f = m.success,
          v = Object(r.useState)(""),
          y = Object(Rt.a)(v, 2),
          S = y[0],
          N = y[1],
          E = Object(r.useState)(!1),
          k = Object(Rt.a)(E, 2),
          C = k[0],
          I = k[1],
          _ = Object(r.useState)({}),
          L = Object(Rt.a)(_, 2),
          w = L[0],
          R = L[1],
          T = Object(r.useState)(0),
          A = Object(Rt.a)(T, 2),
          D = A[0],
          P = A[1];
        if (!d) {
          l.itemsPrice = (t = l.orderItems.reduce(function (e, t) {
            return e + t.price * t.qty;
          }, 0), (Math.round(100 * t) / 100).toFixed(2));
        }
        Object(r.useEffect)(function () {
          var e;
          !l || x || f || l._id !== c ? (i({
            type: Ke
          }), i({
            type: pt
          }), i(ka(c))) : h && h._id && C && (N(h.referralCode), F()), w.length > 0 && "" == S && 0 == D && (P(D + 1), i((e = w, function () {
            var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
              var n, c, s, i, o, l;
              return Object(Pt.a)().wrap(function (t) {
                for (;;) switch (t.prev = t.next) {
                  case 0:
                    return t.prev = 0, a({
                      type: De
                    }), n = r(), c = n.userLogin.userInfo, s = {
                      headers: {
                        "Content-Type": "application/json",
                        Authorization: "Bearer ".concat(c.token)
                      }
                    }, t.next = 6, Ft.a.put("/api/users/multiple", e, s);
                  case 6:
                    i = t.sent, o = i.data, a({
                      type: Pe
                    }), a({
                      type: me,
                      payload: o
                    }), a({
                      type: fe
                    }), t.next = 18;
                    break;
                  case 13:
                    t.prev = 13, t.t0 = t.catch(0), "Not authorized, token failed" === (l = t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message) && a(Gt()), a({
                      type: Ue,
                      payload: l
                    });
                  case 18:
                  case "end":
                    return t.stop();
                }
              }, t, null, [[0, 13]]);
            }));
            return function (e, a) {
              return t.apply(this, arguments);
            };
          }()))), l && l._id;
        }, [c, x, f, l, h, "" == S]);
        var U = function () {
            var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t) {
              return Object(Pt.a)().wrap(function (e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    i(Ca(c)), G();
                  case 2:
                  case "end":
                    return e.stop();
                }
              }, e);
            }));
            return function (t) {
              return e.apply(this, arguments);
            };
          }(),
          z = "",
          F = function () {
            var e = Object(Ut.a)(Object(Pt.a)().mark(function e() {
              var t, a, r, n;
              return Object(Pt.a)().wrap(function (e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    t = (2 * l.totalPrice / 100).toFixed(2), a = (h.referralPoints + parseFloat(t)).toFixed(2), h._id ? (r = {
                      _id: h._id,
                      referralPoints: a
                    }, n = [], w.length > 0 && w.forEach(function (e) {
                      n.push(e);
                    }), void 0 == n.find(function (e) {
                      return e._id == r._id;
                    }) && n.push(r), R(n), z != h.referralCode && (i(Bt(h.referralCode)), z = h.referralCode)) : h.referralCode;
                  case 3:
                  case "end":
                    return e.stop();
                }
              }, e);
            }));
            return function () {
              return e.apply(this, arguments);
            };
          }(),
          G = function () {
            I(!0), i(Bt(l.user.referralCode)).then(function (e) {
              F();
            }).then(function (e) {});
          };
        return d ? Object(Dt.jsx)(ta, {}) : j ? Object(Dt.jsx)(Zt, {
          variant: "danger",
          children: j
        }) : Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(Et.Link, {
            to: u.isAdmin ? "/admin/orderlist" : "/profile",
            className: "btn btn-light my-3",
            onClick: function (e) {
              return function (e) {
                e.preventDefault(), u.isAdmin ? n.goBack() : n.push("/profile");
              }(e);
            },
            children: "Nazad"
          }), Object(Dt.jsxs)("h1", {
            children: ["Porud\u017ebina ", l.orderNo]
          }), Object(Dt.jsxs)(Qt.a, {
            children: [Object(Dt.jsx)(Wt.a, {
              md: 8,
              children: Object(Dt.jsxs)(pa.a, {
                variant: "flush",
                children: [Object(Dt.jsxs)(pa.a.Item, {
                  children: [Object(Dt.jsx)("h2", {
                    children: "Podaci za dostavu"
                  }), Object(Dt.jsx)("p", {
                    children: Object(Dt.jsxs)("span", {
                      className: "push-to-right",
                      children: [Object(Dt.jsx)("strong", {
                        children: "Broj profakture: "
                      }), " ", l.orderNo]
                    })
                  }), Object(Dt.jsx)("p", {
                    children: Object(Dt.jsxs)("span", {
                      className: "push-to-right",
                      children: [Object(Dt.jsx)("strong", {
                        children: "Ime i prezime: "
                      }), " ", l.user.name]
                    })
                  }), Object(Dt.jsx)("p", {
                    children: Object(Dt.jsxs)("span", {
                      className: "push-to-right",
                      children: [Object(Dt.jsx)("strong", {
                        children: "Email: "
                      }), Object(Dt.jsx)("a", {
                        href: "mailto:".concat(l.user.email),
                        children: l.user.email
                      })]
                    })
                  }), Object(Dt.jsx)("p", {
                    children: Object(Dt.jsxs)("span", {
                      className: "push-to-right",
                      children: [Object(Dt.jsx)("strong", {
                        children: "Adresa: "
                      }), l.shippingAddress.address, ", ", l.shippingAddress.postalCode, " ", l.shippingAddress.city, ",", " ", l.shippingAddress.phone]
                    })
                  }), l.isDelivered ? Object(Dt.jsxs)(Zt, {
                    variant: "success",
                    children: ["Dostavljeno dana ", La()(l.deliveredAt).format("DD.MM.yyyy.")]
                  }) : Object(Dt.jsx)(Zt, {
                    variant: "danger",
                    children: "Nije dostavljeno"
                  })]
                }), Object(Dt.jsxs)(pa.a.Item, {
                  children: [Object(Dt.jsx)("h2", {
                    children: "Na\u010din pla\u0107anja"
                  }), Object(Dt.jsx)("p", {
                    children: Object(Dt.jsxs)("span", {
                      className: "push-to-right",
                      children: [Object(Dt.jsx)("strong", {
                        children: "Metod: "
                      }), l.paymentMethod]
                    })
                  }), l.isPaid ? Object(Dt.jsxs)(Zt, {
                    variant: "success",
                    children: ["Pla\u0107eno dana ", La()(l.paidAt).format("DD.MM.yyyy.")]
                  }) : Object(Dt.jsx)(Zt, {
                    variant: "danger",
                    children: "Nije pla\u0107eno"
                  })]
                }), Object(Dt.jsxs)(pa.a.Item, {
                  children: [Object(Dt.jsx)("h2", {
                    children: "Poru\u010deni proizvodi"
                  }), 0 === l.orderItems.length ? Object(Dt.jsx)(Zt, {
                    children: "Va\u0161a porud\u017ebina je prazna"
                  }) : Object(Dt.jsx)(pa.a, {
                    variant: "flush",
                    children: l.orderItems.map(function (e, t) {
                      return Object(Dt.jsx)(pa.a.Item, {
                        children: Object(Dt.jsxs)(Qt.a, {
                          children: [Object(Dt.jsx)(Wt.a, {
                            md: 1,
                            children: Object(Dt.jsx)(ca.a, {
                              src: e.image,
                              alt: e.name,
                              fluid: !0,
                              rounded: !0
                            })
                          }), Object(Dt.jsx)(Wt.a, {
                            children: Object(Dt.jsx)(Et.Link, {
                              to: "/product/".concat(e.product),
                              children: e.name
                            })
                          }), Object(Dt.jsxs)(Wt.a, {
                            md: 4,
                            children: [e.qty, " x R", e.price, " = R", e.qty * e.price]
                          })]
                        })
                      }, t);
                    })
                  })]
                }), Object(Dt.jsx)(pa.a.Item, {
                  children: l.orderItems.length > 0 && Object(Dt.jsx)(Qt.a, {
                    children: Object(Dt.jsx)("h2", {
                      children: Object(Dt.jsx)(za.PDFDownloadLink, {
                        document: Object(Dt.jsx)(Ma, {
                          order: l
                        }),
                        fileName: c + ".pdf",
                        children: function (e) {
                          e.blob, e.url;
                          var t = e.loading;
                          e.error;
                          return t ? "U\u010ditavanje fakture..." : "Preuzmite fakturu";
                        }
                      })
                    })
                  })
                })]
              })
            }), Object(Dt.jsx)(Wt.a, {
              md: 4,
              children: Object(Dt.jsx)(Ht.a, {
                children: Object(Dt.jsxs)(pa.a, {
                  variant: "flush",
                  children: [Object(Dt.jsx)(pa.a.Item, {
                    children: Object(Dt.jsx)("h2", {
                      children: "Pregled porud\u017ebine"
                    })
                  }), Object(Dt.jsx)(pa.a.Item, {
                    className: "push-to-right",
                    children: Object(Dt.jsxs)(Qt.a, {
                      children: [Object(Dt.jsx)(Wt.a, {
                        children: "Proizvodi"
                      }), Object(Dt.jsxs)(Wt.a, {
                        children: [l.itemsPrice, " din"]
                      })]
                    })
                  }), Object(Dt.jsx)(pa.a.Item, {
                    className: "push-to-right",
                    children: Object(Dt.jsxs)(Qt.a, {
                      children: [Object(Dt.jsx)(Wt.a, {
                        children: "Po\u0161tarina"
                      }), Object(Dt.jsxs)(Wt.a, {
                        children: [l.shippingPrice, " din"]
                      })]
                    })
                  }), Object(Dt.jsx)(pa.a.Item, {
                    className: "push-to-right",
                    children: Object(Dt.jsxs)(Qt.a, {
                      children: [Object(Dt.jsx)(Wt.a, {
                        children: Object(Dt.jsx)("strong", {
                          children: "Ukupno"
                        })
                      }), Object(Dt.jsx)(Wt.a, {
                        children: Object(Dt.jsxs)("strong", {
                          children: [l.totalPrice, " din"]
                        })
                      })]
                    })
                  }), !l.isPaid && Object(Dt.jsxs)(pa.a.Item, {
                    children: [O && Object(Dt.jsx)(ta, {}), Object(Dt.jsx)(At.a, {
                      type: "button",
                      className: "btn btn-block",
                      onClick: U,
                      children: "Ozna\u010di kao pla\u0107eno"
                    })]
                  }), g && Object(Dt.jsx)(ta, {}), u.isAdmin && l.isPaid && !l.isDelivered && Object(Dt.jsx)(pa.a.Item, {
                    children: Object(Dt.jsx)(At.a, {
                      type: "button",
                      className: "btn btn-block",
                      onClick: function () {
                        i(Ia(l));
                      },
                      children: "Ozna\u010di kao dostavljeno"
                    })
                  })]
                })
              })
            })]
          })]
        });
      },
      Ja = function (e) {
        var t = e.history,
          a = Object(s.b)(),
          n = Object(s.c)(function (e) {
            return e.orderThisMonth;
          }),
          c = (n.loading, n.error, n.success),
          i = n.total,
          o = Object(s.c)(function (e) {
            return e.orderThisYear;
          }),
          l = (o.loading, o.error, o.success),
          d = o.total,
          j = Object(s.c)(function (e) {
            return e.orderThisYearByProduct;
          }),
          u = (j.loading, j.error, j.success),
          b = (j.total, Object(s.c)(function (e) {
            return e.top5Users;
          })),
          h = b.loading,
          p = b.error,
          O = (b.success, b.users);
        Object(r.useEffect)(function () {
          if (i && -1 == i.sum) {
            var e = new Date(),
              t = e.getFullYear(),
              r = e.getMonth() + 1;
            console.log(r), a((c = r, function () {
              var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t, a) {
                var r, n, s, i, o;
                return Object(Pt.a)().wrap(function (e) {
                  for (;;) switch (e.prev = e.next) {
                    case 0:
                      return e.prev = 0, t({
                        type: et
                      }), r = a(), n = r.userLogin.userInfo, s = {
                        headers: {
                          Authorization: "Bearer ".concat(n.token)
                        }
                      }, e.next = 6, Ft.a.get("/api/orders/monthorders?month=".concat(c), s);
                    case 6:
                      i = e.sent, o = i.data, t({
                        type: tt,
                        payload: o
                      }), e.next = 14;
                      break;
                    case 11:
                      e.prev = 11, e.t0 = e.catch(0), t({
                        type: at,
                        payload: e.t0.response && e.t0.response.data.message ? e.t0.response.data.message : e.t0.message
                      });
                    case 14:
                    case "end":
                      return e.stop();
                  }
                }, e, null, [[0, 11]]);
              }));
              return function (t, a) {
                return e.apply(this, arguments);
              };
            }())), a({
              type: rt
            }), a((n = t, function () {
              var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t, a) {
                var r, c, s, i, o;
                return Object(Pt.a)().wrap(function (e) {
                  for (;;) switch (e.prev = e.next) {
                    case 0:
                      return e.prev = 0, t({
                        type: rt
                      }), r = a(), c = r.userLogin.userInfo, s = {
                        headers: {
                          Authorization: "Bearer ".concat(c.token)
                        }
                      }, e.next = 6, Ft.a.get("/api/orders/yearorders?year=".concat(n), s);
                    case 6:
                      i = e.sent, o = i.data, t({
                        type: nt,
                        payload: o
                      }), e.next = 14;
                      break;
                    case 11:
                      e.prev = 11, e.t0 = e.catch(0), t({
                        type: ct,
                        payload: e.t0.response && e.t0.response.data.message ? e.t0.response.data.message : e.t0.message
                      });
                    case 14:
                    case "end":
                      return e.stop();
                  }
                }, e, null, [[0, 11]]);
              }));
              return function (t, a) {
                return e.apply(this, arguments);
              };
            }())), a({
              type: st
            }), a(function (e) {
              return function () {
                var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                  var n, c, s, i, o;
                  return Object(Pt.a)().wrap(function (t) {
                    for (;;) switch (t.prev = t.next) {
                      case 0:
                        return t.prev = 0, a({
                          type: st
                        }), n = r(), c = n.userLogin.userInfo, s = {
                          headers: {
                            Authorization: "Bearer ".concat(c.token)
                          }
                        }, t.next = 6, Ft.a.get("/api/orders/yearordersbyproduct?year=".concat(e), s);
                      case 6:
                        i = t.sent, o = i.data, a({
                          type: it,
                          payload: o
                        }), t.next = 14;
                        break;
                      case 11:
                        t.prev = 11, t.t0 = t.catch(0), a({
                          type: ot,
                          payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                        });
                      case 14:
                      case "end":
                        return t.stop();
                    }
                  }, t, null, [[0, 11]]);
                }));
                return function (e, a) {
                  return t.apply(this, arguments);
                };
              }();
            }(t)), a({
              type: Ie
            }), a(function () {
              var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t, a) {
                var r, n, c, s, i;
                return Object(Pt.a)().wrap(function (e) {
                  for (;;) switch (e.prev = e.next) {
                    case 0:
                      return e.prev = 0, t({
                        type: Ie
                      }), r = a(), n = r.userLogin.userInfo, c = {
                        headers: {
                          Authorization: "Bearer ".concat(n.token)
                        }
                      }, e.next = 6, Ft.a.get("/api/users/top5", c);
                    case 6:
                      s = e.sent, i = s.data, t({
                        type: _e,
                        payload: i
                      }), e.next = 14;
                      break;
                    case 11:
                      e.prev = 11, e.t0 = e.catch(0), t({
                        type: Le,
                        payload: e.t0.response && e.t0.response.data.message ? e.t0.response.data.message : e.t0.message
                      });
                    case 14:
                    case "end":
                      return e.stop();
                  }
                }, e, null, [[0, 11]]);
              }));
              return function (t, a) {
                return e.apply(this, arguments);
              };
            }());
          } else console.log(O);
          var n, c;
        }, [t, c, l, n, o, u, b]);
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)("div", {
            className: "d-sm-flex align-items-center justify-content-between mb-4",
            children: Object(Dt.jsx)("h1", {
              className: "h3 mb-0 text-gray-800",
              children: "Dashboard"
            })
          }), Object(Dt.jsxs)("div", {
            className: "row",
            children: [Object(Dt.jsx)("div", {
              className: "col-xl-3 col-md-6 mb-4",
              children: Object(Dt.jsx)("div", {
                className: "card border-left-primary shadow h-100 py-2",
                children: Object(Dt.jsx)("div", {
                  className: "card-body",
                  children: Object(Dt.jsxs)("div", {
                    className: "row no-gutters align-items-center",
                    children: [Object(Dt.jsxs)("div", {
                      className: "col mr-2",
                      children: [Object(Dt.jsx)("div", {
                        className: "text-xs font-weight-bold text-primary text-uppercase mb-1",
                        children: "Prodaja ovog meseca"
                      }), Object(Dt.jsxs)("div", {
                        className: "h5 mb-0 font-weight-bold text-gray-800",
                        children: [i && i.sum || 0, " din"]
                      })]
                    }), Object(Dt.jsx)("div", {
                      className: "col-auto",
                      children: Object(Dt.jsx)("i", {
                        className: "fas fa-dollar-sign fa-2x text-gray-300"
                      })
                    })]
                  })
                })
              })
            }), Object(Dt.jsx)("div", {
              className: "col-xl-3 col-md-6 mb-4",
              children: Object(Dt.jsx)("div", {
                className: "card border-left-success shadow h-100 py-2",
                children: Object(Dt.jsx)("div", {
                  className: "card-body",
                  children: Object(Dt.jsxs)("div", {
                    className: "row no-gutters align-items-center",
                    children: [Object(Dt.jsxs)("div", {
                      className: "col mr-2",
                      children: [Object(Dt.jsx)("div", {
                        className: "text-xs font-weight-bold text-success text-uppercase mb-1",
                        children: "Prodaja ove godine"
                      }), Object(Dt.jsxs)("div", {
                        className: "h5 mb-0 font-weight-bold text-gray-800",
                        children: [d && d.sum || 0, " din"]
                      })]
                    }), Object(Dt.jsx)("div", {
                      className: "col-auto",
                      children: Object(Dt.jsx)("i", {
                        className: "fas fa-dollar-sign fa-2x text-gray-300"
                      })
                    })]
                  })
                })
              })
            }), Object(Dt.jsx)("div", {
              className: "col-xl-3 col-md-6 mb-4",
              children: Object(Dt.jsx)("div", {
                className: "card border-left-info shadow h-100 py-2",
                children: Object(Dt.jsx)("div", {
                  className: "card-body",
                  children: Object(Dt.jsxs)("div", {
                    className: "row no-gutters align-items-center",
                    children: [Object(Dt.jsxs)("div", {
                      className: "col mr-2",
                      children: [Object(Dt.jsx)("div", {
                        className: "text-xs font-weight-bold text-info text-uppercase mb-1",
                        children: "Ukupno porud\u017ebina ovog meseca"
                      }), Object(Dt.jsx)("div", {
                        className: "row no-gutters align-items-center",
                        children: Object(Dt.jsx)("div", {
                          className: "col-auto",
                          children: Object(Dt.jsx)("div", {
                            className: "h5 mb-0 mr-3 font-weight-bold text-gray-800",
                            children: i && i.total || 0
                          })
                        })
                      })]
                    }), Object(Dt.jsx)("div", {
                      className: "col-auto",
                      children: Object(Dt.jsx)("i", {
                        className: "fas fa-clipboard-list fa-2x text-gray-300"
                      })
                    })]
                  })
                })
              })
            }), Object(Dt.jsx)("div", {
              className: "col-xl-3 col-md-6 mb-4",
              children: Object(Dt.jsx)("div", {
                className: "card border-left-warning shadow h-100 py-2",
                children: Object(Dt.jsx)("div", {
                  className: "card-body",
                  children: Object(Dt.jsxs)("div", {
                    className: "row no-gutters align-items-center",
                    children: [Object(Dt.jsxs)("div", {
                      className: "col mr-2",
                      children: [Object(Dt.jsx)("div", {
                        className: "text-xs font-weight-bold text-warning text-uppercase mb-1",
                        children: "Ukupno porud\u017ebina ove godine"
                      }), Object(Dt.jsx)("div", {
                        className: "h5 mb-0 font-weight-bold text-gray-800",
                        children: d && d.total || 0
                      })]
                    }), Object(Dt.jsx)("div", {
                      className: "col-auto",
                      children: Object(Dt.jsx)("i", {
                        className: "fas fa-clipboard-list fa-2x text-gray-300"
                      })
                    })]
                  })
                })
              })
            })]
          }), Object(Dt.jsxs)("div", {
            className: "row",
            children: [Object(Dt.jsx)("div", {
              className: "col-lg-6 mb-4",
              children: Object(Dt.jsxs)("div", {
                className: "card shadow mb-4",
                children: [Object(Dt.jsx)("div", {
                  className: "card-header py-3",
                  children: Object(Dt.jsx)("h6", {
                    className: "m-0 font-weight-bold text-primary",
                    children: "TOP 5 kupaca sa najve\u0107om prodajom"
                  })
                }), h ? Object(Dt.jsx)(ta, {}) : p ? Object(Dt.jsx)(Zt, {
                  variant: "danger",
                  children: p
                }) : Object(Dt.jsx)("div", {
                  children: O.map(function (e) {
                    return Object(Dt.jsx)("span", {
                      children: e.name
                    });
                  })
                }), Object(Dt.jsxs)("div", {
                  className: "card-body",
                  children: [Object(Dt.jsxs)("h4", {
                    className: "small font-weight-bold",
                    children: ["Server Migration ", Object(Dt.jsx)("span", {
                      className: "float-right",
                      children: "20%"
                    })]
                  }), Object(Dt.jsx)("div", {
                    className: "progress mb-4",
                    children: Object(Dt.jsx)("div", {
                      className: "progress-bar bg-danger",
                      role: "progressbar",
                      style: {
                        width: "20%",
                        ariaValuenow: "20",
                        ariaValuemin: "0",
                        ariaValuemax: "100"
                      }
                    })
                  }), Object(Dt.jsxs)("h4", {
                    className: "small font-weight-bold",
                    children: ["Sales Tracking ", Object(Dt.jsx)("span", {
                      className: "float-right",
                      children: "40%"
                    })]
                  }), Object(Dt.jsx)("div", {
                    className: "progress mb-4",
                    children: Object(Dt.jsx)("div", {
                      className: "progress-bar bg-warning",
                      role: "progressbar",
                      style: {
                        width: "40%",
                        ariaValuenow: "40",
                        ariaValuemin: "0",
                        ariaValuemax: "100"
                      }
                    })
                  }), Object(Dt.jsxs)("h4", {
                    className: "small font-weight-bold",
                    children: ["Customer Database ", Object(Dt.jsx)("span", {
                      className: "float-right",
                      children: "60%"
                    })]
                  }), Object(Dt.jsx)("div", {
                    className: "progress mb-4",
                    children: Object(Dt.jsx)("div", {
                      className: "progress-bar",
                      role: "progressbar",
                      style: {
                        width: "60%",
                        ariaValuenow: "60",
                        ariaValuemin: "0",
                        ariaValuemax: "100"
                      }
                    })
                  }), Object(Dt.jsxs)("h4", {
                    className: "small font-weight-bold",
                    children: ["Payout Details ", Object(Dt.jsx)("span", {
                      className: "float-right",
                      children: "80%"
                    })]
                  }), Object(Dt.jsx)("div", {
                    className: "progress mb-4",
                    children: Object(Dt.jsx)("div", {
                      className: "progress-bar bg-info",
                      role: "progressbar",
                      style: {
                        width: "80%",
                        ariaValuenow: "80",
                        ariaValuemin: "0",
                        ariaValuemax: "100"
                      }
                    })
                  }), Object(Dt.jsxs)("h4", {
                    className: "small font-weight-bold",
                    children: ["Account Setup ", Object(Dt.jsx)("span", {
                      className: "float-right",
                      children: "Complete!"
                    })]
                  }), Object(Dt.jsx)("div", {
                    className: "progress",
                    children: Object(Dt.jsx)("div", {
                      className: "progress-bar bg-success",
                      role: "progressbar",
                      style: {
                        width: "100%",
                        ariaValuenow: "100",
                        ariaValuemin: "0",
                        ariaValuemax: "100"
                      }
                    })
                  })]
                })]
              })
            }), Object(Dt.jsxs)("div", {
              className: "row col-lg-6 mb-4",
              children: [Object(Dt.jsx)("div", {
                className: "col-lg-6 mb-4",
                children: Object(Dt.jsx)("div", {
                  className: "card bg-primary text-white shadow",
                  children: Object(Dt.jsxs)("div", {
                    className: "card-body",
                    children: ["Primary", Object(Dt.jsx)("div", {
                      className: "text-white-50 small",
                      children: "#4e73df"
                    })]
                  })
                })
              }), Object(Dt.jsx)("div", {
                className: "col-lg-6 mb-4",
                children: Object(Dt.jsx)("div", {
                  className: "card bg-success text-white shadow",
                  children: Object(Dt.jsxs)("div", {
                    className: "card-body",
                    children: ["Success", Object(Dt.jsx)("div", {
                      className: "text-white-50 small",
                      children: "#1cc88a"
                    })]
                  })
                })
              }), Object(Dt.jsx)("div", {
                className: "col-lg-6 mb-4",
                children: Object(Dt.jsx)("div", {
                  className: "card bg-info text-white shadow",
                  children: Object(Dt.jsxs)("div", {
                    className: "card-body",
                    children: ["Info", Object(Dt.jsx)("div", {
                      className: "text-white-50 small",
                      children: "#36b9cc"
                    })]
                  })
                })
              }), Object(Dt.jsx)("div", {
                className: "col-lg-6 mb-4",
                children: Object(Dt.jsx)("div", {
                  className: "card bg-warning text-white shadow",
                  children: Object(Dt.jsxs)("div", {
                    className: "card-body",
                    children: ["Warning", Object(Dt.jsx)("div", {
                      className: "text-white-50 small",
                      children: "#f6c23e"
                    })]
                  })
                })
              }), Object(Dt.jsx)("div", {
                className: "col-lg-6 mb-4",
                children: Object(Dt.jsx)("div", {
                  className: "card bg-danger text-white shadow",
                  children: Object(Dt.jsxs)("div", {
                    className: "card-body",
                    children: ["Danger", Object(Dt.jsx)("div", {
                      className: "text-white-50 small",
                      children: "#e74a3b"
                    })]
                  })
                })
              }), Object(Dt.jsx)("div", {
                className: "col-lg-6 mb-4",
                children: Object(Dt.jsx)("div", {
                  className: "card bg-secondary text-white shadow",
                  children: Object(Dt.jsxs)("div", {
                    className: "card-body",
                    children: ["Secondary", Object(Dt.jsx)("div", {
                      className: "text-white-50 small",
                      children: "#858796"
                    })]
                  })
                })
              }), Object(Dt.jsx)("div", {
                className: "col-lg-6 mb-4",
                children: Object(Dt.jsx)("div", {
                  className: "card bg-light text-black shadow",
                  children: Object(Dt.jsxs)("div", {
                    className: "card-body",
                    children: ["Light", Object(Dt.jsx)("div", {
                      className: "text-black-50 small",
                      children: "#f8f9fc"
                    })]
                  })
                })
              }), Object(Dt.jsx)("div", {
                className: "col-lg-6 mb-4",
                children: Object(Dt.jsx)("div", {
                  className: "card bg-dark text-white shadow",
                  children: Object(Dt.jsxs)("div", {
                    className: "card-body",
                    children: ["Dark", Object(Dt.jsx)("div", {
                      className: "text-white-50 small",
                      children: "#5a5c69"
                    })]
                  })
                })
              })]
            })]
          })]
        });
      },
      $a = function (e) {
        var t = e.history,
          a = (e.match, Object(s.b)()),
          n = Object(s.c)(function (e) {
            return e.categoryList;
          }),
          c = n.loading,
          i = n.error,
          o = n.categories,
          l = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          d = Object(s.c)(function (e) {
            return e.categoryDelete;
          }),
          j = d.loading,
          u = d.error,
          b = d.success,
          h = Object(s.c)(function (e) {
            return e.categoryCreate;
          }),
          p = h.loading,
          O = h.error,
          x = h.success,
          m = h.category;
        Object(r.useEffect)(function () {
          a({
            type: q
          }), l && l.isAdmin || t.push("/login"), x && null != m ? t.push("/admin/category/".concat(m._id, "/edit")) : x && null == m ? t.push("/admin/category/".concat(0, "/edit")) : a(ua(""));
        }, [a, l, t, b, x, m]);
        var g = function (e) {
          window.confirm("Are you sure?") && a(function (e) {
            return function () {
              var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                var n, c, s;
                return Object(Pt.a)().wrap(function (t) {
                  for (;;) switch (t.prev = t.next) {
                    case 0:
                      return t.prev = 0, a({
                        type: M
                      }), n = r(), c = n.userLogin.userInfo, s = {
                        headers: {
                          Authorization: "Bearer ".concat(c.token)
                        }
                      }, t.next = 6, Ft.a.delete("/api/categories/".concat(e), s);
                    case 6:
                      a({
                        type: B
                      }), t.next = 12;
                      break;
                    case 9:
                      t.prev = 9, t.t0 = t.catch(0), a({
                        type: V,
                        payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                      });
                    case 12:
                    case "end":
                      return t.stop();
                  }
                }, t, null, [[0, 9]]);
              }));
              return function (e, a) {
                return t.apply(this, arguments);
              };
            }();
          }(e));
        };
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(Qt.a, {
            className: "align-items-center",
            children: Object(Dt.jsx)(Wt.a, {
              className: "text-right",
              children: Object(Dt.jsxs)(At.a, {
                className: "my-3",
                onClick: function () {
                  a({
                    type: Q,
                    payload: null
                  });
                },
                children: [Object(Dt.jsx)("span", {
                  className: "plus-sign-margin",
                  children: Object(Dt.jsx)("i", {
                    className: "fas fa-plus"
                  })
                }), "Nova kategorija"]
              })
            })
          }), p && Object(Dt.jsx)(ta, {}), O && Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: O
          }), j && Object(Dt.jsx)(ta, {}), u && Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: u
          }), c ? Object(Dt.jsx)(ta, {}) : i ? Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: i
          }) : Object(Dt.jsx)(Dt.Fragment, {
            children: Object(Dt.jsxs)(Ea.a, {
              bordered: !0,
              hover: !0,
              responsive: !0,
              className: "table-sm",
              children: [Object(Dt.jsx)("thead", {
                children: Object(Dt.jsxs)("tr", {
                  children: [Object(Dt.jsx)("th", {
                    children: "Naziv"
                  }), Object(Dt.jsx)("th", {
                    children: "Izmena"
                  }), Object(Dt.jsx)("th", {
                    children: "Brisanje"
                  })]
                })
              }), Object(Dt.jsx)("tbody", {
                children: o.map(function (e) {
                  return Object(Dt.jsxs)("tr", {
                    children: [Object(Dt.jsx)("td", {
                      children: e.name
                    }), Object(Dt.jsx)("td", {
                      children: Object(Dt.jsx)(It.LinkContainer, {
                        to: "/admin/category/".concat(e._id, "/edit"),
                        children: Object(Dt.jsx)(At.a, {
                          variant: "light",
                          className: "btn-sm",
                          children: Object(Dt.jsx)("i", {
                            className: "fas fa-edit"
                          })
                        })
                      })
                    }), Object(Dt.jsx)("td", {
                      children: Object(Dt.jsx)(At.a, {
                        variant: "danger",
                        className: "btn-sm",
                        onClick: function () {
                          return g(e._id);
                        },
                        children: Object(Dt.jsx)("i", {
                          className: "fas fa-trash"
                        })
                      })
                    })]
                  }, e._id);
                })
              })]
            })
          })]
        });
      },
      Xa = function (e) {
        var t = e.match,
          a = e.history,
          n = t.params.id,
          c = Object(r.useState)(""),
          i = Object(Rt.a)(c, 2),
          o = i[0],
          l = i[1],
          d = Object(r.useState)(""),
          j = Object(Rt.a)(d, 2),
          u = j[0],
          b = j[1],
          h = Object(r.useState)(!1),
          p = Object(Rt.a)(h, 2),
          O = p[0],
          x = p[1],
          m = Object(s.b)(),
          g = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo,
          f = Object(s.c)(function (e) {
            return e.categoryDetails;
          }),
          v = f.loading,
          y = f.error,
          S = f.category,
          N = Object(s.c)(function (e) {
            return e.categoryUpdate;
          }),
          E = N.loading,
          k = N.error,
          C = N.success,
          I = Object(s.c)(function (e) {
            return e.categoryCreate;
          }),
          _ = (I.loading, I.error, I.success);
        Object(r.useEffect)(function () {
          C ? (m({
            type: $
          }), m(ba(n)), a.push("/admin/categorylist")) : _ ? (m({
            type: q
          }), a.push("/admin/categorylist")) : S.name && S._id === n || "0" == n ? (l(S.name), b(S.image)) : m(ba(n));
        }, [C, _, m, a, S, n]);
        var L = function () {
          var e = Object(Ut.a)(Object(Pt.a)().mark(function e(t) {
            var a, r, n, c, s;
            return Object(Pt.a)().wrap(function (e) {
              for (;;) switch (e.prev = e.next) {
                case 0:
                  return a = t.target.files[0], (r = new FormData()).append("image", a), x(!0), e.prev = 4, n = {
                    headers: {
                      "Content-Type": "multipart/form-data"
                    }
                  }, e.next = 8, Ft.a.post("/api/upload", r, n);
                case 8:
                  c = e.sent, s = c.data, b(s), x(!1), e.next = 18;
                  break;
                case 14:
                  e.prev = 14, e.t0 = e.catch(4), console.error(e.t0), x(!1);
                case 18:
                case "end":
                  return e.stop();
              }
            }, e, null, [[4, 14]]);
          }));
          return function (t) {
            return e.apply(this, arguments);
          };
        }();
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(Et.Link, {
            to: "/admin/categorylist",
            className: "btn btn-light my-3",
            children: "Nazad"
          }), Object(Dt.jsxs)(va, {
            children: [Object(Dt.jsx)("h1", {
              children: "Podaci kategorije"
            }), E && Object(Dt.jsx)(ta, {}), k && Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: k
            }), v ? Object(Dt.jsx)(ta, {}) : y ? Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: y
            }) : Object(Dt.jsxs)(Tt.a, {
              onSubmit: function (e) {
                e.preventDefault(), m("0" != n ? function (e) {
                  return function () {
                    var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                      var n, c, s, i, o;
                      return Object(Pt.a)().wrap(function (t) {
                        for (;;) switch (t.prev = t.next) {
                          case 0:
                            return t.prev = 0, a({
                              type: H
                            }), n = r(), c = n.userLogin.userInfo, s = {
                              headers: {
                                "Content-Type": "application/json",
                                Authorization: "Bearer ".concat(c.token)
                              }
                            }, t.next = 6, Ft.a.put("/api/categories/".concat(e._id), e, s);
                          case 6:
                            i = t.sent, o = i.data, a({
                              type: K,
                              payload: o
                            }), t.next = 14;
                            break;
                          case 11:
                            t.prev = 11, t.t0 = t.catch(0), a({
                              type: J,
                              payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                            });
                          case 14:
                          case "end":
                            return t.stop();
                        }
                      }, t, null, [[0, 11]]);
                    }));
                    return function (e, a) {
                      return t.apply(this, arguments);
                    };
                  }();
                }({
                  _id: n,
                  name: o,
                  image: u
                }) : function (e) {
                  return function () {
                    var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a, r) {
                      var n, c, s, i, o;
                      return Object(Pt.a)().wrap(function (t) {
                        for (;;) switch (t.prev = t.next) {
                          case 0:
                            return t.prev = 1, a({
                              type: Y
                            }), n = r(), c = n.userLogin.userInfo, s = {
                              headers: {
                                Authorization: "Bearer ".concat(c.token)
                              }
                            }, t.next = 7, Ft.a.post("/api/categories", e, s);
                          case 7:
                            i = t.sent, o = i.data, a({
                              type: Q,
                              payload: o
                            }), t.next = 15;
                            break;
                          case 12:
                            t.prev = 12, t.t0 = t.catch(1), a({
                              type: W,
                              payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                            });
                          case 15:
                          case "end":
                            return t.stop();
                        }
                      }, t, null, [[1, 12]]);
                    }));
                    return function (e, a) {
                      return t.apply(this, arguments);
                    };
                  }();
                }({
                  name: o,
                  userInfo: g,
                  image: u
                }));
              },
              children: [Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "name",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Naziv"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Unesite naziv kategorije",
                  value: o,
                  onChange: function (e) {
                    return l(e.target.value);
                  }
                })]
              }), Object(Dt.jsxs)(Tt.a.Group, {
                controlId: "image",
                children: [Object(Dt.jsx)(Tt.a.Label, {
                  children: "Fotografija"
                }), Object(Dt.jsx)(Tt.a.Control, {
                  type: "text",
                  placeholder: "Putanja do fotografije kategorije",
                  value: u,
                  onChange: function (e) {
                    return b(e.target.value);
                  }
                }), Object(Dt.jsx)(Tt.a.File, {
                  id: "image-file",
                  accept: "image/*",
                  label: "Odaberite fotografiju",
                  custom: !0,
                  onChange: L
                }), O && Object(Dt.jsx)(ta, {})]
              }), Object(Dt.jsx)(At.a, {
                type: "submit",
                variant: "primary",
                children: "Snimanje"
              })]
            })]
          })]
        });
      },
      Za = function (e) {
        var t = e.history,
          a = Object(s.b)(),
          n = window.location.href.toLowerCase().endsWith("admin");
        Object(r.useEffect)(function () {
          n && t.push("/admin/index");
        }, [n, t]);
        return Object(Dt.jsx)(Dt.Fragment, {
          children: Object(Dt.jsx)("div", {
            id: "page-top",
            children: Object(Dt.jsxs)("div", {
              id: "wrapper",
              children: [Object(Dt.jsxs)("ul", {
                className: "navbar-nav bg-gradient-primary sidebar sidebar-dark accordion",
                id: "accordionSidebar",
                children: [Object(Dt.jsxs)("a", {
                  className: "sidebar-brand d-flex align-items-center justify-content-center",
                  href: "/admin/index",
                  children: [Object(Dt.jsx)("div", {
                    className: "sidebar-brand-icon rotate-n-15",
                    children: Object(Dt.jsx)("i", {
                      className: "fas fa-laugh-wink"
                    })
                  }), Object(Dt.jsx)("div", {
                    className: "sidebar-brand-text mx-3",
                    children: "MAPO Admin"
                  })]
                }), Object(Dt.jsx)("hr", {
                  className: "sidebar-divider my-0"
                }), Object(Dt.jsx)("li", {
                  className: "nav-item",
                  children: Object(Dt.jsx)(It.LinkContainer, {
                    className: "nav-link",
                    to: "/admin/index",
                    children: Object(Dt.jsxs)(Lt.a.Link, {
                      children: [Object(Dt.jsx)("i", {
                        className: "fas fa-fw fa-tachometer-alt"
                      }), Object(Dt.jsx)("span", {
                        children: "Dashboard"
                      })]
                    })
                  })
                }), Object(Dt.jsx)("hr", {
                  className: "sidebar-divider"
                }), Object(Dt.jsx)("li", {
                  className: "nav-item",
                  children: Object(Dt.jsx)(It.LinkContainer, {
                    className: "nav-link",
                    to: "/admin/userlist",
                    children: Object(Dt.jsxs)(Lt.a.Link, {
                      children: [Object(Dt.jsx)("i", {
                        className: "fas fa-fw fa-users"
                      }), Object(Dt.jsx)("span", {
                        children: "Kupci"
                      })]
                    })
                  })
                }), Object(Dt.jsx)("li", {
                  className: "nav-item",
                  children: Object(Dt.jsx)(It.LinkContainer, {
                    className: "nav-link",
                    to: "/admin/orderlist",
                    children: Object(Dt.jsxs)(Lt.a.Link, {
                      children: [Object(Dt.jsx)("i", {
                        className: "fas fa-fw fa-table"
                      }), Object(Dt.jsx)("span", {
                        children: "Porud\u017ebine"
                      })]
                    })
                  })
                }), Object(Dt.jsx)("li", {
                  className: "nav-item",
                  children: Object(Dt.jsx)(It.LinkContainer, {
                    className: "nav-link",
                    to: "/admin/categorylist",
                    children: Object(Dt.jsxs)(Lt.a.Link, {
                      children: [Object(Dt.jsx)("i", {
                        className: "fas fa-fw fa-barcode"
                      }), Object(Dt.jsx)("span", {
                        children: "Kategorije"
                      })]
                    })
                  })
                }), Object(Dt.jsx)("li", {
                  className: "nav-item",
                  children: Object(Dt.jsx)(It.LinkContainer, {
                    className: "nav-link",
                    to: "/admin/productlist",
                    children: Object(Dt.jsxs)(Lt.a.Link, {
                      children: [Object(Dt.jsx)("i", {
                        className: "fas fa-fw fa-barcode"
                      }), Object(Dt.jsx)("span", {
                        children: "Proizvodi"
                      })]
                    })
                  })
                }), Object(Dt.jsx)("hr", {
                  className: "sidebar-divider"
                }), Object(Dt.jsx)("li", {
                  className: "nav-item",
                  children: Object(Dt.jsx)(It.LinkContainer, {
                    className: "nav-link",
                    to: "/admin",
                    onClick: function (e) {
                      !function (e) {
                        e.preventDefault(), a(Gt());
                      }(e);
                    },
                    children: Object(Dt.jsxs)(Lt.a.Link, {
                      children: [Object(Dt.jsx)("i", {
                        className: "fas fa-fw fa-barcode"
                      }), Object(Dt.jsx)("span", {
                        children: "Odjava"
                      })]
                    })
                  })
                })]
              }), Object(Dt.jsx)("div", {
                id: "content-wrapper",
                children: Object(Dt.jsx)("div", {
                  id: "content",
                  children: Object(Dt.jsx)("div", {
                    className: "container-fluid",
                    children: Object(Dt.jsxs)(kt.g, {
                      children: [Object(Dt.jsx)(kt.d, {
                        exact: !0,
                        path: "/admin/index",
                        component: Ja
                      }), Object(Dt.jsx)(kt.d, {
                        exact: !0,
                        path: "/admin/userlist",
                        component: Va
                      }), Object(Dt.jsx)(kt.d, {
                        path: "/admin/orderlist",
                        component: qa
                      }), Object(Dt.jsx)(kt.d, {
                        path: "/admin/user/:id/edit",
                        component: Ya
                      }), Object(Dt.jsx)(kt.d, {
                        path: "/admin/userorders/:id",
                        component: Ha
                      }), Object(Dt.jsx)(kt.d, {
                        exact: !0,
                        path: "/admin/productlist",
                        component: Qa
                      }), Object(Dt.jsx)(kt.d, {
                        path: "/admin/product/:id/edit",
                        component: Wa
                      }), Object(Dt.jsx)(kt.d, {
                        path: "/admin/productlist/:pageNumber",
                        component: Qa,
                        exact: !0
                      }), Object(Dt.jsx)(kt.d, {
                        exact: !0,
                        path: "/admin/categorylist",
                        component: $a
                      }), Object(Dt.jsx)(kt.d, {
                        path: "/admin/category/:id/edit",
                        component: Xa
                      }), Object(Dt.jsx)(kt.d, {
                        path: "/admin/order/:id",
                        component: Ka,
                        exact: !0
                      })]
                    })
                  })
                })
              })]
            })
          })
        });
      },
      er = function (e) {
        var t = e.history,
          a = e.match,
          n = Object(r.useState)(!1),
          c = Object(Rt.a)(n, 2),
          i = c[0],
          o = c[1],
          l = Object(r.useState)(""),
          d = Object(Rt.a)(l, 2),
          j = d[0],
          u = d[1],
          b = Object(r.useState)("all"),
          h = Object(Rt.a)(b, 2),
          p = (h[0], h[1], a.params.pageNumber || 1),
          O = Object(s.b)(),
          x = Object(s.c)(function (e) {
            return e.productList;
          }),
          m = x.loading,
          g = x.error,
          f = x.products,
          v = x.page,
          y = x.pages,
          S = Object(s.c)(function (e) {
            return e.userLogin;
          }).userInfo;
        Object(r.useEffect)(function () {
          O(sa(j, p));
        }, [O, S, t, p, i]);
        var N = ["Sve proizvode", "Elektronske ure\u0111aje", "Kompjutersku opremu", "Kuhinjsku opremu"],
          E = ["Apple", "Samsung", "Google", "HTC"],
          k = ["Sony", "Cannon", "Amazon", "Logitech"];
        function C() {
          return Object(Dt.jsxs)("ul", {
            className: "list-group list-group-flush rounded",
            children: [Object(Dt.jsxs)("li", {
              className: "list-group-item d-none d-lg-block",
              children: [Object(Dt.jsx)("h5", {
                className: "mt-1 mb-2",
                children: "Prika\u017ei"
              }), Object(Dt.jsx)("div", {
                className: "d-flex flex-wrap my-2",
                children: N.map(function (e, t) {
                  return Object(Dt.jsx)(It.LinkContainer, {
                    to: "/products",
                    className: "btn btn-sm btn-outline-dark rounded-pill me-2 mb-2",
                    replace: !0,
                    children: Object(Dt.jsx)(Lt.a.Link, {
                      children: e
                    })
                  }, t);
                })
              })]
            }), Object(Dt.jsxs)("li", {
              className: "list-group-item",
              children: [Object(Dt.jsx)("h5", {
                className: "mt-1 mb-1",
                children: "Brendovi"
              }), Object(Dt.jsx)("div", {
                className: "d-flex flex-column",
                children: E.map(function (e, t) {
                  return Object(Dt.jsxs)("div", {
                    className: "form-check",
                    children: [Object(Dt.jsx)("input", {
                      className: "form-check-input",
                      type: "checkbox"
                    }), Object(Dt.jsx)("label", {
                      className: "form-check-label",
                      htmlFor: "flexCheckDefault",
                      children: e
                    })]
                  }, t);
                })
              })]
            }), Object(Dt.jsxs)("li", {
              className: "list-group-item",
              children: [Object(Dt.jsx)("h5", {
                className: "mt-1 mb-1",
                children: "Proizvo\u0111a\u010di"
              }), Object(Dt.jsx)("div", {
                className: "d-flex flex-column",
                children: k.map(function (e, t) {
                  return Object(Dt.jsxs)("div", {
                    className: "form-check",
                    children: [Object(Dt.jsx)("input", {
                      className: "form-check-input",
                      type: "checkbox"
                    }), Object(Dt.jsx)("label", {
                      className: "form-check-label",
                      htmlFor: "flexCheckDefault",
                      children: e
                    })]
                  }, t);
                })
              })]
            }), Object(Dt.jsx)("li", {
              className: "list-group-item",
              children: Object(Dt.jsx)("div", {
                className: "d-grid d-block mb-3",
                children: Object(Dt.jsx)("button", {
                  className: "btn btn-dark",
                  children: "Primeni filter"
                })
              })
            })]
          });
        }
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(ja, {
            title: "Proizvodi"
          }), Object(Dt.jsx)("div", {
            className: "contact-us products",
            children: Object(Dt.jsx)("div", {
              className: "container center",
              children: Object(Dt.jsx)("h2", {
                className: "",
                children: "Proizvodi"
              })
            })
          }), m ? Object(Dt.jsx)(ta, {}) : g ? Object(Dt.jsx)(Zt, {
            variant: "danger",
            children: g
          }) : Object(Dt.jsxs)(Dt.Fragment, {
            children: [Object(Dt.jsxs)("div", {
              className: "container",
              children: [Object(Dt.jsx)("div", {
                className: "h-scroller d-block d-lg-none",
                children: Object(Dt.jsx)("nav", {
                  className: "nav h-underline",
                  children: N.map(function (e, t) {
                    return Object(Dt.jsx)("div", {
                      className: "h-link me-2",
                      children: Object(Dt.jsx)(It.LinkContainer, {
                        to: "/products",
                        className: "btn btn-sm btn-outline-dark rounded-pill",
                        replace: !0,
                        children: Object(Dt.jsx)(Lt.a.Link, {
                          children: e
                        })
                      })
                    }, t);
                  })
                })
              }), Object(Dt.jsx)("div", {
                className: "row mb-3 d-block d-lg-none",
                children: Object(Dt.jsx)("div", {
                  className: "col-12",
                  children: Object(Dt.jsxs)("div", {
                    id: "accordionFilter",
                    className: "accordion shadow-sm",
                    children: [Object(Dt.jsx)("div", {
                      className: "accordion-item",
                      children: Object(Dt.jsx)("h2", {
                        className: "accordion-header",
                        id: "headingOne",
                        children: Object(Dt.jsx)("button", {
                          className: "accordion-button fw-bold collapsed",
                          type: "button",
                          "data-bs-toggle": "collapse",
                          "data-bs-target": "#collapseFilter",
                          "aria-expanded": "false",
                          "aria-controls": "collapseFilter",
                          children: "Pretraga proizvoda"
                        })
                      })
                    }), Object(Dt.jsx)("div", {
                      id: "collapseFilter",
                      className: "accordion-collapse collapse",
                      "data-bs-parent": "#accordionFilter",
                      children: Object(Dt.jsx)("div", {
                        className: "accordion-body p-0",
                        children: Object(Dt.jsx)(C, {})
                      })
                    })]
                  })
                })
              }), Object(Dt.jsxs)("div", {
                className: "row mb-4 mt-lg-3",
                children: [Object(Dt.jsx)("div", {
                  className: "d-none d-lg-block col-lg-3",
                  children: Object(Dt.jsx)("div", {
                    className: "border rounded shadow-sm",
                    children: Object(Dt.jsx)(C, {})
                  })
                }), Object(Dt.jsx)("div", {
                  className: "col-lg-9",
                  children: Object(Dt.jsxs)("div", {
                    className: "d-flex flex-column h-100",
                    children: [Object(Dt.jsx)(Qt.a, {
                      children: Object(Dt.jsxs)("div", {
                        className: "input-group",
                        children: [Object(Dt.jsx)("input", {
                          className: "form-control",
                          type: "text",
                          placeholder: "Pretraga proizvoda...",
                          "aria-label": "search input",
                          onChange: function (e) {
                            return u(e.target.value);
                          },
                          value: j
                        }), Object(Dt.jsx)("button", {
                          className: "btn btn-primary small",
                          onClick: function () {
                            o(!i);
                          },
                          children: Object(Dt.jsx)("i", {
                            className: "fas fa-search"
                          })
                        })]
                      })
                    }), Object(Dt.jsx)(Qt.a, {
                      children: f.map(function (e) {
                        return Object(Dt.jsx)(Wt.a, {
                          sm: 12,
                          md: "6",
                          lg: 4,
                          xl: 4,
                          children: Object(Dt.jsx)(Kt, {
                            product: e
                          })
                        }, e._id);
                      })
                    })]
                  })
                })]
              })]
            }), Object(Dt.jsx)(ra, {
              pages: y,
              page: v,
              isAdmin: !0
            })]
          })]
        });
      },
      tr = function (e) {
        e.history;
        var t = e.match,
          a = t.params.id,
          n = t.params.keyword,
          c = t.params.pageNumber || 1;
        console.log(t.params);
        var i = Object(s.b)(),
          o = Object(s.c)(function (e) {
            return e.productList;
          }),
          l = o.loading,
          d = o.error,
          j = o.products,
          u = o.page,
          b = o.pages;
        j && j.length > 0 && (console.log(j[0].category.name), console.log("page " + u + " from " + b));
        Object(s.c)(function (e) {
          return e.userLogin;
        }).userInfo;
        return Object(r.useEffect)(function () {
          i(sa(n, c, a));
        }, [i, t, c]), Object(Dt.jsx)(Dt.Fragment, {
          children: Object(Dt.jsxs)("div", {
            className: "container",
            children: [Object(Dt.jsx)(Et.Link, {
              className: "btn btn-light my-3",
              to: "/",
              children: "Nazad"
            }), j && j.length > 0 && Object(Dt.jsx)("h1", {
              children: j[0].category.name
            }), l ? Object(Dt.jsx)(ta, {}) : d ? Object(Dt.jsx)(Zt, {
              variant: "danger",
              children: d
            }) : Object(Dt.jsxs)(Dt.Fragment, {
              children: [j && j.length > 0 && Object(Dt.jsx)(ja, {
                title: j[0].category.name
              }), Object(Dt.jsx)(Qt.a, {
                children: j.map(function (e) {
                  return Object(Dt.jsx)(Wt.a, {
                    sm: 12,
                    md: "6",
                    lg: 4,
                    xl: 3,
                    children: Object(Dt.jsx)(Kt, {
                      product: e
                    })
                  }, e._id);
                })
              }), Object(Dt.jsx)(ra, {
                pages: b,
                page: u,
                keyword: n || "",
                category: a
              })]
            })]
          })
        });
      },
      ar = function (e) {
        e.match;
        var t = Object(s.b)();
        return Object(r.useEffect)(function () {}, [t]), Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(ja, {
            title: "O nama"
          }), Object(Dt.jsx)("div", {
            className: "contact-us about",
            children: Object(Dt.jsx)("div", {
              className: "container center",
              children: Object(Dt.jsx)("h2", {
                className: "",
                children: "O nama"
              })
            })
          }), Object(Dt.jsxs)("div", {
            className: "container top20",
            children: [Object(Dt.jsx)(Et.Link, {
              className: "btn btn-light",
              to: "/",
              children: "Nazad"
            }), Object(Dt.jsxs)("div", {
              className: "container ",
              children: [Object(Dt.jsx)("p", {
                children: "Osnovana 1980. godine, Herbalife Nutrition je vode\u0107a kompanija za wellness \u010diji je cilj da vam pojednostavi put do optimalnog na\u010dina \u017eivota."
              }), Object(Dt.jsx)("h2", {
                children: "Herbalife visokokvalitetni proizvodi i programi nude:"
              }), Object(Dt.jsxs)("ul", {
                children: [Object(Dt.jsx)("li", {
                  children: "uspe\u0161nu kontrolu te\u017eine"
                }), Object(Dt.jsx)("li", {
                  children: "kvalitetniju ishranu"
                }), Object(Dt.jsx)("li", {
                  children: "bolju li\u010dnu negu"
                }), Object(Dt.jsx)("li", {
                  children: "priliku za dodatnu zaradu"
                })]
              }), Object(Dt.jsx)("h2", {
                children: "Proizvodi koji se proizvode na nau\u010dnoj osnovi, namenjeni va\u0161em uspehu"
              }), Object(Dt.jsx)("p", {
                children: "Herbalife inovativne proizvode razvili su nau\u010dnici, lekari i nutricionisti koji su se vodili va\u0161im li\u010dnim ciljevima u vezi se zdravljem. I dok su Herbalife formulacije inspirisane prirodom, osmi\u0161ljene da vam omogu\u0107e da do\u017eivotno u\u017eivate u njihovim blagotvornim svojstvima, prednosti koje nudi poslovna prilika vam otvaraju mogu\u0107nost da ostvarite i finansijski uspeh."
              }), Object(Dt.jsx)("h2", {
                children: "Stru\u010dnjaci kojima mo\u017eete verovati"
              }), "Herbalife-ov Nau\u010dni savetodavni odbor, Grupa za zdravstvena pitanja i Medicinski savetodavni odbor su pokreta\u010dke snage zaslu\u017ene za Herbalife-ov uspeh i zauzimaju vode\u0107e mesto u bran\u0161i. Ovaj tim uglednih nau\u010dnih stru\u010dnjaka usmerava razvoj Herbalife proizvoda kroz istra\u017eivanje i ispitivanje proizvoda, tako da kvalitet Herbalife-a zadovoljava najvi\u0161e standarde koji su danas zadati za ovu privrednu granu.", Object(Dt.jsx)("h2", {
                children: "Podr\u0161ka na koju mo\u017eete ra\u010dunati"
              }), "Uz tim od vi\u0161e od 1.800.000 nezavisnih \u010clanova koji Herbalife proizvode prodaju u vi\u0161e od 60 zemalja \u0161irom sveta (i uz godi\u0161nji maloprodajni promet od 1,8 milijarde US $) mo\u017eete biti sigurni da \u0107ete uvek imati podr\u0161ku koja vam je potrebna da biste uspeli. Kada budete spremni da promenite svoj \u017eivot, s ponosom \u0107emo vam pomo\u0107i da uspete na svakom koraku i omogu\u0107iti vam da:", Object(Dt.jsxs)("ul", {
                children: [Object(Dt.jsx)("li", {
                  children: "zadovoljite svoje potrebe u pogledu ishrane, gubitka te\u017eine i li\u010dne nege"
                }), Object(Dt.jsx)("li", {
                  children: "nau\u010dite vi\u0161e o na\u0161oj poslovnoj prilici"
                })]
              }), Object(Dt.jsx)("p", {
                children: "Uz Herbalife, kvalitetne proizvode i stru\u010dnu podr\u0161ku lako \u0107ete izgledati, ose\u0107ati se i izgledati \u0161to je najbolje mogu\u0107e. Otkrijte Herbalife \u2013 i otkri\u0107ete do\u017eivotno optimalno zdravlje."
              })]
            })]
          })]
        });
      },
      rr = function (e) {
        e.match;
        var t = Object(s.b)(),
          a = Object(r.useState)(""),
          n = Object(Rt.a)(a, 2),
          c = n[0],
          i = n[1],
          o = Object(r.useState)(""),
          l = Object(Rt.a)(o, 2),
          d = l[0],
          j = l[1],
          u = Object(r.useState)(""),
          b = Object(Rt.a)(u, 2),
          h = b[0],
          p = b[1],
          O = Object(r.useState)(null),
          x = Object(Rt.a)(O, 2),
          m = x[0],
          g = x[1],
          f = Object(r.useState)(""),
          v = Object(Rt.a)(f, 2),
          y = v[0],
          S = v[1],
          N = Object(r.useState)("danger"),
          E = Object(Rt.a)(N, 2),
          k = E[0],
          C = E[1],
          I = Object(r.useState)(!1),
          _ = Object(Rt.a)(I, 2),
          L = _[0],
          w = _[1],
          R = Object(s.c)(function (e) {
            return e.emailSend;
          }),
          T = R.loading,
          A = R.error,
          D = R.msg,
          P = Object(s.c)(function (e) {
            return e.createPdf;
          });
        P.loading, P.error, P.msg;
        Object(r.useEffect)(function () {
          "success" == D ? (g("Uspe\u0161no poslata poruka. Kontakrira\u0107emo Vas u najkra\u0107em mogu\u0107em roku."), C("success"), U(), z()) : "error" == D && (U(), g("Neuspe\u0161no slanje. Probajte ponovo ili nas kontaktirajte putem telefona."), C("danger"));
        }, [t, D]);
        var U = function () {
            w(!0), window.setTimeout(function () {
              w(!1);
            }, 4e3);
          },
          z = function () {
            i(""), j(""), p(""), S("");
          };
        return Object(Dt.jsxs)(Dt.Fragment, {
          children: [Object(Dt.jsx)(ja, {
            title: "Kontaktirajte nas"
          }), Object(Dt.jsx)("div", {
            className: "contact-us",
            children: Object(Dt.jsx)("div", {
              className: "container center",
              children: Object(Dt.jsx)("h2", {
                className: "",
                children: "Kontaktirajete nas"
              })
            })
          }), Object(Dt.jsxs)("div", {
            className: "container top20",
            children: [Object(Dt.jsx)(Et.Link, {
              className: "btn btn-light",
              to: "/",
              children: "Nazad"
            }), Object(Dt.jsxs)("div", {
              className: "container ",
              children: [m && L && Object(Dt.jsx)(Zt, {
                visible: L,
                variant: k,
                children: m
              }), A && Object(Dt.jsx)(Zt, {
                variant: "danger",
                children: A
              }), T && Object(Dt.jsx)(ta, {}), Object(Dt.jsxs)("div", {
                className: "row top20",
                children: [Object(Dt.jsxs)("div", {
                  className: "col-lg-6",
                  children: [Object(Dt.jsx)("p", {
                    children: "Imate pitanja ili \u017eelite neki vid saradnje? Pi\u0161ite nam."
                  }), Object(Dt.jsxs)(Tt.a, {
                    onSubmit: function (e) {
                      e.preventDefault(), t(function (e) {
                        return function () {
                          var t = Object(Ut.a)(Object(Pt.a)().mark(function t(a) {
                            var r, n;
                            return Object(Pt.a)().wrap(function (t) {
                              for (;;) switch (t.prev = t.next) {
                                case 0:
                                  return t.prev = 0, console.log(e), a({
                                    type: ne
                                  }), t.next = 5, Ft.a.post("/api/email", e);
                                case 5:
                                  r = t.sent, n = r.data, a({
                                    type: ce,
                                    payload: n
                                  }), t.next = 13;
                                  break;
                                case 10:
                                  t.prev = 10, t.t0 = t.catch(0), a({
                                    type: se,
                                    payload: t.t0.response && t.t0.response.data.message ? t.t0.response.data.message : t.t0.message
                                  });
                                case 13:
                                case "end":
                                  return t.stop();
                              }
                            }, t, null, [[0, 10]]);
                          }));
                          return function (e) {
                            return t.apply(this, arguments);
                          };
                        }();
                      }({
                        name: c,
                        phone: d,
                        email: h,
                        message: y
                      }));
                    },
                    children: [Object(Dt.jsxs)(Tt.a.Group, {
                      controlId: "name",
                      children: [Object(Dt.jsx)(Tt.a.Label, {
                        children: "Ime i prezime"
                      }), Object(Dt.jsx)(Tt.a.Control, {
                        type: "text",
                        required: !0,
                        placeholder: "Unesite ime i prezime",
                        value: c,
                        onChange: function (e) {
                          return i(e.target.value);
                        }
                      })]
                    }), Object(Dt.jsxs)(Tt.a.Group, {
                      controlId: "phone",
                      children: [Object(Dt.jsx)(Tt.a.Label, {
                        children: "Broj telefona"
                      }), Object(Dt.jsx)(Tt.a.Control, {
                        type: "text",
                        required: !0,
                        placeholder: "Unesite broj telefona",
                        value: d,
                        onChange: function (e) {
                          return j(e.target.value);
                        }
                      })]
                    }), Object(Dt.jsxs)(Tt.a.Group, {
                      controlId: "email",
                      children: [Object(Dt.jsx)(Tt.a.Label, {
                        children: "Email adresa"
                      }), Object(Dt.jsx)(Tt.a.Control, {
                        type: "email",
                        required: !0,
                        placeholder: "Unesite email adresu",
                        value: h,
                        onChange: function (e) {
                          return p(e.target.value);
                        }
                      })]
                    }), Object(Dt.jsxs)(Tt.a.Group, {
                      controlId: "message",
                      children: [Object(Dt.jsx)(Tt.a.Label, {
                        children: "Poruka"
                      }), Object(Dt.jsx)(Tt.a.Control, {
                        type: "text",
                        as: "textarea",
                        rows: 3,
                        required: !0,
                        placeholder: "Unesite Va\u0161u poruku",
                        value: y,
                        onChange: function (e) {
                          return S(e.target.value);
                        }
                      })]
                    }), Object(Dt.jsx)(At.a, {
                      type: "submit",
                      variant: "primary",
                      children: "Po\u0161alji"
                    })]
                  })]
                }), Object(Dt.jsxs)("div", {
                  className: "col-lg-6",
                  children: [Object(Dt.jsx)("p", {
                    children: "Osnovni podaci firme:"
                  }), Object(Dt.jsx)("p", {
                    children: Object(Dt.jsx)("strong", {
                      children: "Marija Popovi\u0107 PR Radnja za proizvodnju ostale ode\u0107e MAPO"
                    })
                  }), Object(Dt.jsxs)("p", {
                    children: [Object(Dt.jsx)("strong", {
                      children: "Mati\u010dni broj: "
                    }), "63940364"]
                  }), Object(Dt.jsxs)("p", {
                    children: [Object(Dt.jsx)("strong", {
                      children: "Adresa: "
                    }), "Radmile Savi\u0107evi\u0107 27, Donja Vre\u017eina"]
                  }), Object(Dt.jsxs)("p", {
                    children: [Object(Dt.jsx)("strong", {
                      children: "Kontakt telefon: "
                    }), Object(Dt.jsx)("a", {
                      href: "tel:+38162447558",
                      children: "062/447558"
                    })]
                  })]
                })]
              })]
            })]
          })]
        });
      },
      nr = function (e) {
        e.history;
        var t = function (e) {
            var t = e.children;
            return Object(Dt.jsxs)(Dt.Fragment, {
              children: [Object(Dt.jsx)(Yt, {}), Object(Dt.jsx)("main", {
                className: "py-3",
                children: t
              }), Object(Dt.jsx)(qt, {})]
            });
          },
          a = function (e) {
            var t = e.children;
            return Object(Dt.jsxs)(Dt.Fragment, {
              children: [Object(Dt.jsx)(Yt, {}), Object(Dt.jsx)("main", {
                className: "py-3 full",
                children: Object(Dt.jsx)(Ct.a, {
                  children: t
                })
              }), Object(Dt.jsx)(qt, {})]
            });
          };
        return Object(Dt.jsxs)(Et.BrowserRouter, {
          children: [Object(Dt.jsx)(kt.d, {
            exact: !0,
            path: ["/login", "/register", "/profile", "top5", "/shipping", "/payment", "/placeorder", "/order/:id", "/category/:id", "/category/:id/page/:pageNumber", "/products", "/product/:id", "/cart/:id?", "/search/:keyword", "/search/:keyword/page/:pageNumber", "/page/:pageNumber", "/", "/about-us", "/contact-us"],
            children: Object(Dt.jsxs)(t, {
              children: [Object(Dt.jsx)(kt.d, {
                path: "/login",
                component: ya
              }), Object(Dt.jsx)(kt.d, {
                path: "/register",
                component: Na
              }), Object(Dt.jsx)(kt.d, {
                path: "/profile",
                component: wa
              }), Object(Dt.jsx)(kt.d, {
                path: "/shipping",
                component: Ta
              }), Object(Dt.jsx)(kt.d, {
                path: "/payment",
                component: Aa
              }), Object(Dt.jsx)(kt.d, {
                path: "/placeorder",
                component: Da
              }), Object(Dt.jsx)(kt.d, {
                path: "/order/:id",
                component: Ba
              }), Object(Dt.jsx)(kt.d, {
                path: "/category/:id",
                component: tr,
                exact: !0
              }), Object(Dt.jsx)(kt.d, {
                path: "/category/:id/page/:pageNumber",
                component: tr
              }), Object(Dt.jsx)(kt.d, {
                path: "/products",
                component: er
              }), Object(Dt.jsx)(kt.d, {
                path: "/product/:id",
                component: ma
              }), Object(Dt.jsx)(kt.d, {
                path: "/cart/:id?",
                component: fa
              }), Object(Dt.jsx)(kt.d, {
                path: "/search/:keyword",
                component: ha,
                exact: !0
              }), Object(Dt.jsx)(kt.d, {
                path: "/search/:keyword/page/:pageNumber",
                component: ha,
                exact: !0
              }), Object(Dt.jsx)(kt.d, {
                path: "/page/:pageNumber",
                component: ha,
                exact: !0
              }), Object(Dt.jsx)(kt.d, {
                path: "/",
                component: ha,
                exact: !0
              }), Object(Dt.jsx)(kt.d, {
                path: "/about-us",
                component: ar,
                exact: !0
              }), Object(Dt.jsx)(kt.d, {
                path: "/contact-us",
                component: rr,
                exact: !0
              })]
            })
          }), Object(Dt.jsx)(kt.d, {
            exact: !0,
            path: ["/admin", "/admin/index", "/admin/userlist", "/admin/orderlist", "/admin/user/:id/edit", "/admin/userorders/:id", "/admin/productlist", "/admin/productlist/:pageNumber", "/admin/categorylist", "/admin/product/:id/edit", "/admin/category/:id/edit", "/admin/order/:id"],
            children: Object(Dt.jsx)(a, {
              children: Object(Dt.jsx)(kt.d, {
                path: "/admin",
                component: Za
              })
            })
          })]
        });
      },
      cr = function (e) {
        e && e instanceof Function && a.e(3).then(a.bind(null, 593)).then(function (t) {
          var a = t.getCLS,
            r = t.getFID,
            n = t.getFCP,
            c = t.getLCP,
            s = t.getTTFB;
          a(e), r(e), n(e), c(e), s(e);
        });
      };
    c.a.render(Object(Dt.jsx)(s.a, {
      store: Nt,
      children: Object(Dt.jsx)(nr, {})
    }), document.getElementById("root")), cr();
  }
}, [[572, 1, 2]]]);
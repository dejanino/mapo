import React from 'react'
import { Alert } from 'react-bootstrap'

const Message = ({ variant, children, visible }) => {
	return <Alert visible={visible} variant={variant}>{children}</Alert>
}

Message.defaultProps = {
	variant: 'info',
}

export default Message

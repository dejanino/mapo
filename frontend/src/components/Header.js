import React from 'react'
import { Route } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { LinkContainer } from 'react-router-bootstrap'
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap'
import SearchBox from './SearchBox'
import { logout } from '../actions/userActions'
import Logo from '../img/mapo.jpg'

const Header = () => {
	const dispatch = useDispatch()

	// useSelector is to grab what we want from the state
	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	const logoutHandler = () => {
		dispatch(logout())
	}

	
	return (
		<header>
			<Navbar
				className='text-uppercase'
				bg='primary'
				variant='dark'
				expand='lg'
				collapseOnSelect
			>
				<Container>
					{/* Home */}
					<LinkContainer to='/'>
						<Navbar.Brand>
							{/* <img src={Logo} alt='Mapo' /> */}MAPO
						</Navbar.Brand>
					</LinkContainer>
					<Navbar.Toggle aria-controls='basic-navbar-nav' />
					<Navbar.Collapse id='basic-navbar-nav'>
						{/* <Route render={({ history }) => <SearchBox history={history} />} /> */}
						<Nav className='ml-auto'>
							{/* Products */}
							<LinkContainer to='/products'>
								<Nav.Link>
									Proizvodi
								</Nav.Link>
							</LinkContainer>
							{/* Cart */}
							<LinkContainer to='/cart'>
								<Nav.Link>
									<i className='fas fa-shopping-cart'></i> Korpa
								</Nav.Link>
							</LinkContainer>
							{/* About Us */}
							<LinkContainer to='/about-us'>
								<Nav.Link>
									O nama
								</Nav.Link>
							</LinkContainer>
							{/* Contact Us */}
							<LinkContainer to='/contact-us'>
								<Nav.Link>
									Kontakt
								</Nav.Link>
							</LinkContainer>
							{userInfo ? (
								<NavDropdown title={userInfo.name} id='username'>
									<LinkContainer to='/profile'>
										<NavDropdown.Item>Profil</NavDropdown.Item>
									</LinkContainer>
									{/* Logout */}
									<NavDropdown.Item onClick={logoutHandler}>
										Odjava
									</NavDropdown.Item>
								</NavDropdown>
							) : (
								// Login
								<LinkContainer to='/login'>
									<Nav.Link>
										<i className='fas fa-user'></i> Prijava
									</Nav.Link>
								</LinkContainer>
							)}
							{userInfo && userInfo.isAdmin && (
								<LinkContainer to='/admin'>
								<Nav.Link>
									Admin
								</Nav.Link>
							</LinkContainer>
							)}
						</Nav>
					</Navbar.Collapse>
				</Container>
			</Navbar>
		</header>
	)
}

export default Header

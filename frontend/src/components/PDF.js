import { Page, Font, Text, View, Document, StyleSheet, Image } from '@react-pdf/renderer';
import Logo from '../img/m-logo.png'
import moment from 'moment';

Font.register({
    family: "Roboto",
    src:
        "https://cdnjs.cloudflare.com/ajax/libs/ink/3.1.10/fonts/Roboto/roboto-light-webfont.ttf"
});

const getWidth = () => {
    return
}

// Create styles
const styles = StyleSheet.create({
    page: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        backgroundColor: '#FFF',
        fontFamily: 'Roboto',
        margin: '10px',
        marginTop: 0,
        marginBottom: 0
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1,
        textAlign: 'left',
        fontSize: 12,
        width: '45%'
    },
    sectionHeader: {
        margin: 10,
        padding: 10,
        marginBottom: 0,
        paddingBottom: 0,
        flexGrow: 1,
        textAlign: 'right',
        fontSize: 14,
        width: '45%'
    },
    full: {
        margin: 10,
        padding: 10,
        textAlign: 'left',
        width: '100%',
        fontSize: 12
    },
    tright: {
        textAlign: 'right'
    },
    strong: {
        fontWeight: 'bold'
    },
    logo: {
        width: '120px'
    },
    table: {
        display: "table",
        width: "auto",
        borderStyle: "solid",
        borderWidth: 1,
        borderRightWidth: 0,
        borderBottomWidth: 0
    },
    borderlessTable: {
        display: "table",
        width: "auto",
        borderStyle: "solid",
        borderWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 0
    },
    tableRow: {
        margin: "auto",
        flexDirection: "row"
    },
    tableCol: {
        width: "25%",
        borderStyle: "solid",
        borderWidth: 1,
        borderLeftWidth: 0,
        borderTopWidth: 0
    },
    tableColFirst: {
        width: "40%",
        borderStyle: "solid",
        borderWidth: 1,
        borderLeftWidth: 0,
        borderTopWidth: 0
    },
    tableColSecond: {
        width: "10%",
        borderStyle: "solid",
        borderWidth: 1,
        borderLeftWidth: 0,
        borderTopWidth: 0
    },
    tableColTotalFirst: {
        width: "50%",
        borderStyle: "solid",
        borderWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 1,
        borderTopWidth: 0
    },
    tableCell: {
        margin: "auto",
        marginTop: 5,
        marginBottom: 5,
        fontSize: 10
    },
    tableCellLeft: {
        marginLeft: 5,
        marginTop: 5,
        marginBottom: 5,
        fontSize: 10
    }
});

// Create Document Component
const PDF = (order) => {
    const orderData = order.order
    const today = moment(orderData.createdAt).format('DD.MM.yyyy.')
    return (
        <Document>
            <Page size="A4" style={styles.page}>
                {/* <View style={styles.full}>
                    <Image style={styles.logo} src={Logo} />
                </View> */}
                <View style={styles.sectionHeader}>
                    <Image style={styles.logo} src={Logo} />
                </View>
                <View style={styles.sectionHeader}>
                    <Text style={styles.tright}>PEDRAČUN</Text>
                    <Text style={styles.tright}> </Text>
                    <Text style={styles.tright}>Broj predračuna: {orderData.orderNo}</Text>
                    <Text style={styles.tright}>Datum izdavanja: {today}</Text>
                </View>
                <View style={styles.full}>
                    <Text></Text>
                </View>
                <View style={styles.section}>
                    <Text>MAPO</Text>
                    <Text>Radmile Savićević 27, Donja Vrežina</Text>
                    <Text>Tel: 062447558</Text>
                    <Text>Email: maponis2015@gmail.com</Text>
                </View>
                <View style={styles.section}>
                    <Text> </Text>
                    <Text>Matični broj: 63940364</Text>
                    <Text>PIB: 109117318</Text>
                    <Text>Broj računa: 325-9500700195189-23</Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.strong}>Podaci kupca</Text>
                    <Text>{orderData.user.name}</Text>
                    <Text>{orderData.shippingAddress.address}{', '}{orderData.shippingAddress.postalCode}{' '}{orderData.shippingAddress.city}</Text>
                </View>
                <View style={styles.section}>
                    <Text> </Text>
                    <Text>Telefon: {orderData.shippingAddress.phone}</Text>
                    <Text>Email: {orderData.user.email}</Text>
                </View>
                <View style={styles.full}>
                    <View style={styles.table}>
                        <View style={styles.tableRow}>
                            <View style={styles.tableColFirst}>
                                <Text style={styles.tableCellLeft}>Naziv proizvoda</Text>
                            </View>
                            <View style={styles.tableColSecond}>
                                <Text style={styles.tableCell}>Količina</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>Cena (din)</Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>Ukupno (din)</Text>
                            </View>
                        </View>
                        {orderData.orderItems.map((item, index) => (
                            <View key={"order-item-" + index} style={styles.tableRow}>
                                <View style={styles.tableColFirst}>
                                    <Text style={styles.tableCellLeft}>{item.name}</Text>
                                </View>
                                <View style={styles.tableColSecond}>
                                    <Text style={styles.tableCell}>{item.qty}</Text>
                                </View>
                                <View style={styles.tableCol}>
                                    <Text style={styles.tableCell}>{item.price}</Text>
                                </View>
                                <View style={styles.tableCol}>
                                    <Text style={styles.tableCell}>{item.qty * item.price}</Text>
                                </View>
                            </View>
                        ))}
                    </View>
                    <View style={styles.borderlessTable}>
                        <View style={styles.tableRow}>
                            <View style={styles.tableColTotalFirst}>
                                <Text style={styles.tableCellLeft}> </Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>Ukupno za uplatu: </Text>
                            </View>
                            <View style={styles.tableCol}>
                                <Text style={styles.tableCell}>{orderData.totalPrice} dinara</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </Page>
        </Document>
    )
}

export default PDF;
import axios from 'axios'
import {
	PDF_REQUEST,
	PDF_SUCCESS,
	PDF_FAIL,
} from '../constants/pdfConstants'

// Actions to get crearte pdf
export const createPdf = (body) => async (
	dispatch
) => {
	try {
		console.log(body)
		dispatch({ type: PDF_REQUEST })
		// Make request to generate pdf
		const { data } = await axios.post(
			`/api/pdf`, body
		)
		dispatch({
			type: PDF_SUCCESS,
			payload: data,
		})
	} catch (error) {
		dispatch({
			type: PDF_FAIL,
			payload:
				// Send a custom error message
				// Else send a generic error message
				error.response && error.response.data.message
					? error.response.data.message
					: error.message,
		})
	}
}
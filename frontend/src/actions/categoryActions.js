import axios from 'axios'
import {
	CATEGORY_LIST_REQUEST,
	CATEGORY_LIST_SUCCESS,
	CATEGORY_LIST_FAIL,
	CATEGORY_DETAILS_REQUEST,
	CATEGORY_DETAILS_SUCCESS,
	CATEGORY_DETAILS_FAIL,
	CATEGORY_DELETE_REQUEST,
	CATEGORY_DELETE_SUCCESS,
	CATEGORY_DELETE_FAIL,
	CATEGORY_CREATE_REQUEST,
	CATEGORY_CREATE_SUCCESS,
	CATEGORY_CREATE_FAIL,
	CATEGORY_UPDATE_REQUEST,
	CATEGORY_UPDATE_SUCCESS,
	CATEGORY_UPDATE_FAIL,
} from '../constants/categoryConstants'

// Actions to get all categories
export const listCategories = (keyword = '') => async (
	dispatch
) => {
	try {
		dispatch({ type: CATEGORY_LIST_REQUEST })
		// Make request to get all categories
		const { data } = await axios.get(
			`/api/categories?keyword=${keyword}`
		)
		dispatch({
			type: CATEGORY_LIST_SUCCESS,
			payload: data,
		})
	} catch (error) {
		dispatch({
			type: CATEGORY_LIST_FAIL,
			payload:
				// Send a custom error message
				// Else send a generic error message
				error.response && error.response.data.message
					? error.response.data.message
					: error.message,
		})
	}
}

// Actions to get a single category
export const listCategoryDetails = (id) => async (dispatch) => {
	try {
		dispatch({ type: CATEGORY_DETAILS_REQUEST })
		// Make request to get a single category
		const { data } = await axios.get(`/api/categories/${id}`)

		dispatch({
			type: CATEGORY_DETAILS_SUCCESS,
			payload: data,
		})
	} catch (error) {
		dispatch({
			type: CATEGORY_DETAILS_FAIL,
			payload:
				// Send a custom error message
				// Else send a generic error message
				error.response && error.response.data.message
					? error.response.data.message
					: error.message,
		})
	}
}
// Actions to delete a single category
export const deleteCategory = (id) => async (dispatch, getState) => {
	try {
		dispatch({ type: CATEGORY_DELETE_REQUEST })

		// Get userInfo from userLogin by destructuring
		const {
			userLogin: { userInfo },
		} = getState()

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
			},
		}

		// Make delete request to delete a product
		await axios.delete(`/api/categories/${id}`, config)

		dispatch({ type: CATEGORY_DELETE_SUCCESS })
	} catch (error) {
		dispatch({
			type: CATEGORY_DELETE_FAIL,
			payload:
				// Send a custom error message
				// Else send a generic error message
				error.response && error.response.data.message
					? error.response.data.message
					: error.message,
		})
	}
}
// Actions to create a single category
export const createCategory = (category) => async (dispatch, getState) => {
	debugger
	try {
		dispatch({ type: CATEGORY_CREATE_REQUEST })

		// Get userInfo from userLogin by destructuring
		const {
			userLogin: { userInfo },
		} = getState()

		const config = {
			headers: {
				Authorization: `Bearer ${userInfo.token}`,
			},
		}

		// Make post request to create a category
		const { data } = await axios.post('/api/categories', category, config)

		dispatch({ type: CATEGORY_CREATE_SUCCESS, payload: data })
	} catch (error) {
		dispatch({
			type: CATEGORY_CREATE_FAIL,
			payload:
				// Send a custom error message
				// Else send a generic error message
				error.response && error.response.data.message
					? error.response.data.message
					: error.message,
		})
	}
}
// Actions to update a single category
export const updateCategory = (category) => async (dispatch, getState) => {
	try {
		dispatch({ type: CATEGORY_UPDATE_REQUEST })

		// Get userInfo from userLogin by destructuring
		const {
			userLogin: { userInfo },
		} = getState()

		const config = {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${userInfo.token}`,
			},
		}

		// Make put request to update a category
		const { data } = await axios.put(
			`/api/categories/${category._id}`,
			category,
			config
		)

		dispatch({ type: CATEGORY_UPDATE_SUCCESS, payload: data })
	} catch (error) {
		dispatch({
			type: CATEGORY_UPDATE_FAIL,
			payload:
				// Send a custom error message
				// Else send a generic error message
				error.response && error.response.data.message
					? error.response.data.message
					: error.message,
		})
	}
}

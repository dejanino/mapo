import axios from 'axios'
import {
	EMAIL_SEND_REQUEST,
	EMAIL_SEND_SUCCESS,
	EMAIL_SEND_FAIL,
} from '../constants/emailConstants'

// Actions to get all categories
export const sendEmail = (body) => async (
	dispatch
) => {
	try {
		console.log(body)
		dispatch({ type: EMAIL_SEND_REQUEST })
		// Make request to send an email
		const { data } = await axios.post(
			`/api/email`, body
		)
		dispatch({
			type: EMAIL_SEND_SUCCESS,
			payload: data,
		})
	} catch (error) {
		dispatch({
			type: EMAIL_SEND_FAIL,
			payload:
				// Send a custom error message
				// Else send a generic error message
				error.response && error.response.data.message
					? error.response.data.message
					: error.message,
		})
	}
}
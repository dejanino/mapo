export const ORDER_CREATE_REQUEST = 'ORDER_CREATE_REQUEST'
export const ORDER_CREATE_SUCCESS = 'ORDER_CREATE_SUCCESS'
export const ORDER_CREATE_FAIL = 'ORDER_CREATE_FAIL'
export const ORDER_CREATE_RESET = 'ORDER_CREATE_RESET'

export const ORDER_DETAILS_REQUEST = 'ORDER_DETAILS_REQUEST'
export const ORDER_DETAILS_SUCCESS = 'ORDER_DETAILS_SUCCESS'
export const ORDER_DETAILS_FAIL = 'ORDER_DETAILS_FAIL'

export const ORDER_PAY_REQUEST = 'ORDER_PAY_REQUEST'
export const ORDER_PAY_SUCCESS = 'ORDER_PAY_SUCCESS'
export const ORDER_PAY_FAIL = 'ORDER_PAY_FAIL'
export const ORDER_PAY_RESET = 'ORDER_PAY_RESET'

export const ORDER_LIST_MY_REQUEST = 'ORDER_LIST_MY_REQUEST'
export const ORDER_LIST_MY_SUCCESS = 'ORDER_LIST_MY_SUCCESS'
export const ORDER_LIST_MY_FAIL = 'ORDER_LIST_MY_FAIL'
export const ORDER_LIST_MY_RESET = 'ORDER_LIST_MY_RESET'

export const ORDER_LIST_MONTH_REQUEST = 'ORDER_LIST_MONTH_REQUEST'
export const ORDER_LIST_MONTH_SUCCESS = 'ORDER_LIST_MONTH_SUCCESS'
export const ORDER_LIST_MONTH_FAIL = 'ORDER_LIST_MONTH_FAIL'
export const ORDER_LIST_MONTH_RESET = 'ORDER_LIST_MONTH_RESET'

export const ORDER_LIST_YEAR_REQUEST = 'ORDER_LIST_YEAR_REQUEST'
export const ORDER_LIST_YEAR_SUCCESS = 'ORDER_LIST_YEAR_SUCCESS'
export const ORDER_LIST_YEAR_FAIL = 'ORDER_LIST_YEAR_FAIL'
export const ORDER_LIST_YEAR_RESET = 'ORDER_LIST_YEAR_RESET'

export const ORDER_LIST_YEAR_BY_PRODUCT_REQUEST = 'ORDER_LIST_YEAR_BY_PRODUCT_REQUEST'
export const ORDER_LIST_YEAR_BY_PRODUCT_SUCCESS = 'ORDER_LIST_YEAR_BY_PRODUCT_SUCCESS'
export const ORDER_LIST_YEAR_BY_PRODUCT_FAIL = 'ORDER_LIST_YEAR_BY_PRODUCT_FAIL'
export const ORDER_LIST_YEAR_BY_PRODUCT_RESET = 'ORDER_LIST_YEAR_BY_PRODUCT_RESET'

export const ORDER_LIST_REQUEST = 'ORDER_LIST_REQUEST'
export const ORDER_LIST_SUCCESS = 'ORDER_LIST_SUCCESS'
export const ORDER_LIST_FAIL = 'ORDER_LIST_FAIL'

export const ORDER_LIST_USER_REQUEST = 'ORDER_LIST_REQUEST'
export const ORDER_LIST_USER_SUCCESS = 'ORDER_LIST_SUCCESS'
export const ORDER_LIST_USER_FAIL = 'ORDER_LIST_FAIL'

export const ORDER_DELIVER_REQUEST = 'ORDER_DELIVER_REQUEST'
export const ORDER_DELIVER_SUCCESS = 'ORDER_DELIVER_SUCCESS'
export const ORDER_DELIVER_FAIL = 'ORDER_DELIVER_FAIL'
export const ORDER_DELIVER_RESET = 'ORDER_DELIVER_RESET'

export const ORDER_LAST_NO_REQUEST = 'ORDER_LAST_NO_REQUEST'
export const ORDER_LAST_NO_SUCCESS = 'ORDER_LAST_NO_SUCCESS'
export const ORDER_LAST_NO_FAIL = 'ORDER_LAST_NO_FAIL'
export const ORDER_LAST_NO_RESET = 'ORDER_LAST_NO_RESET'

import {
	CATEGORY_LIST_REQUEST,
	CATEGORY_LIST_SUCCESS,
	CATEGORY_LIST_FAIL,
	CATEGORY_DETAILS_REQUEST,
	CATEGORY_DETAILS_SUCCESS,
	CATEGORY_DETAILS_FAIL,
	CATEGORY_DELETE_REQUEST,
	CATEGORY_DELETE_SUCCESS,
	CATEGORY_DELETE_FAIL,
	CATEGORY_CREATE_REQUEST,
	CATEGORY_CREATE_SUCCESS,
	CATEGORY_CREATE_FAIL,
	CATEGORY_CREATE_RESET,
	CATEGORY_UPDATE_REQUEST,
	CATEGORY_UPDATE_SUCCESS,
	CATEGORY_UPDATE_FAIL,
	CATEGORY_UPDATE_RESET,
} from '../constants/categoryConstants'

// categoryListReducer for all categories
export const categoryListReducer = (state = { categories: [] }, action) => {
	switch (action.type) {
		// Requests category list
		// categories is an empty array because its still loading
		// loading will NOT be done yet, so true
		case CATEGORY_LIST_REQUEST:
			return { loading: true, categories: [] }

		// On success
		// categories will contain data as a payload
		// loading will be done, so false
		case CATEGORY_LIST_SUCCESS:
			return {
				loading: false,
				categories: action.payload.categories,
				pages: action.payload.pages,
				page: action.payload.page,
			}

		// On fail
		// categories will contain the error as a payload
		// loading will be done, so false
		case CATEGORY_LIST_FAIL:
			return { loading: false, error: action.payload }

		// In any other case, just return the state
		default:
			return state
	}
}

// categoryDetailsReducer for a single category
export const categoryDetailsReducer = (
	state = { category: { } },
	action
) => {
	switch (action.type) {
		// Requests a category
		// category is an empty object because its still loading
		// reviews is an empty array because its still loading
		// loading will NOT be done yet, so true
		// add contents of state using a spread operator (...)
		case CATEGORY_DETAILS_REQUEST:
			return { loading: true, ...state }

		// On success
		// category will contain data as a payload
		// loading will be done, so false
		case CATEGORY_DETAILS_SUCCESS:
			return { loading: false, category: action.payload }

		// On fail
		// category will contain the error as a payload
		// loading will be done, so false
		case CATEGORY_DETAILS_FAIL:
			return { loading: false, error: action.payload }

		// In any other case, just return the state
		default:
			return state
	}
}
// categoryDeleteReducer for admin users
export const categoryDeleteReducer = (state = {}, action) => {
	switch (action.type) {
		// loading will NOT be done yet, so true
		case CATEGORY_DELETE_REQUEST:
			return { loading: true }

		// On success
		// loading will be done, so false
		case CATEGORY_DELETE_SUCCESS:
			return { loading: false, success: true }

		// On fail
		// the error will contain the payload
		// loading will be done, so false
		case CATEGORY_DELETE_FAIL:
			return { loading: false, error: action.payload }

		// In any other case, just return the state
		default:
			return state
	}
}
// categoryCreateReducer for admin users
export const categoryCreateReducer = (state = {}, action) => {
	switch (action.type) {
		// loading will NOT be done yet, so true
		case CATEGORY_CREATE_REQUEST:
			return { loading: true }

		// On success
		// loading will be done, so false
		case CATEGORY_CREATE_SUCCESS:
			return { loading: false, success: true, category: action.payload }

		// On fail
		// the error will contain the payload
		// loading will be done, so false
		case CATEGORY_CREATE_FAIL:
			return { loading: false, error: action.payload }

		case CATEGORY_CREATE_RESET:
			return {}

		// In any other case, just return the state
		default:
			return state
	}
}
// categoryUpdateReducer for admin users
export const categoryUpdateReducer = (state = { category: {} }, action) => {
	switch (action.type) {
		// loading will NOT be done yet, so true
		case CATEGORY_UPDATE_REQUEST:
			return { loading: true }

		// On success
		// loading will be done, so false
		case CATEGORY_UPDATE_SUCCESS:
			return { loading: false, success: true, category: action.payload }

		// On fail
		// the error will contain the payload
		// loading will be done, so false
		case CATEGORY_UPDATE_FAIL:
			return { loading: false, error: action.payload }

		case CATEGORY_UPDATE_RESET:
			return { category: {} }

		// In any other case, just return the state
		default:
			return state
	}
}
import {
	ORDER_CREATE_REQUEST,
	ORDER_CREATE_SUCCESS,
	ORDER_CREATE_FAIL,
	ORDER_CREATE_RESET,
	ORDER_DETAILS_REQUEST,
	ORDER_DETAILS_SUCCESS,
	ORDER_DETAILS_FAIL,
	ORDER_PAY_REQUEST,
	ORDER_PAY_SUCCESS,
	ORDER_PAY_FAIL,
	ORDER_PAY_RESET,
	ORDER_LIST_MY_REQUEST,
	ORDER_LIST_MY_SUCCESS,
	ORDER_LIST_MY_FAIL,
	ORDER_LIST_MY_RESET,
	ORDER_LIST_MONTH_REQUEST,
	ORDER_LIST_MONTH_SUCCESS,
	ORDER_LIST_MONTH_FAIL,
	ORDER_LIST_MONTH_RESET,
	ORDER_LIST_YEAR_REQUEST,
	ORDER_LIST_YEAR_SUCCESS,
	ORDER_LIST_YEAR_FAIL,
	ORDER_LIST_YEAR_RESET,
	ORDER_LIST_YEAR_BY_PRODUCT_REQUEST,
	ORDER_LIST_YEAR_BY_PRODUCT_SUCCESS,
	ORDER_LIST_YEAR_BY_PRODUCT_FAIL,
	ORDER_LIST_YEAR_BY_PRODUCT_RESET,
	ORDER_LIST_REQUEST,
	ORDER_LIST_SUCCESS,
	ORDER_LIST_FAIL,
	ORDER_LIST_USER_REQUEST,
	ORDER_LIST_USER_SUCCESS,
	ORDER_LIST_USER_FAIL,
	ORDER_DELIVER_REQUEST,
	ORDER_DELIVER_SUCCESS,
	ORDER_DELIVER_FAIL,
	ORDER_DELIVER_RESET,
	ORDER_LAST_NO_REQUEST,
	ORDER_LAST_NO_SUCCESS,
	ORDER_LAST_NO_RESET,
	ORDER_LAST_NO_FAIL
} from '../constants/orderConstants'

export const orderCreateReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_CREATE_REQUEST:
			return {
				loading: true,
			}
		case ORDER_CREATE_SUCCESS:
			return {
				loading: false,
				success: true,
				order: action.payload,
			}
		case ORDER_CREATE_FAIL:
			return { loading: false, error: action.payload }
		case ORDER_CREATE_RESET:
			return {}
		// In any other case, just return the state
		default:
			return state
	}
}
export const orderDetailsReducer = (
	state = { loading: true, orderItems: [], shippingAddress: {} },
	action
) => {
	switch (action.type) {
		case ORDER_DETAILS_REQUEST:
			return {
				...state,
				loading: true,
			}
		case ORDER_DETAILS_SUCCESS:
			return {
				loading: false,
				order: action.payload,
			}
		case ORDER_DETAILS_FAIL:
			return { loading: false, error: action.payload }
		// In any other case, just return the state
		default:
			return state
	}
}
export const orderPayReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_PAY_REQUEST:
			return { loading: true }
		case ORDER_PAY_SUCCESS:
			return {
				loading: false,
				success: true,
			}
		case ORDER_PAY_FAIL:
			return { loading: false, error: action.payload }
		case ORDER_PAY_RESET:
			return {}
		// In any other case, just return the state
		default:
			return state
	}
}
export const orderListMyReducer = (state = { orders: [] }, action) => {
	switch (action.type) {
		case ORDER_LIST_MY_REQUEST:
			return { loading: true }
		case ORDER_LIST_MY_SUCCESS:
			return {
				loading: false,
				orders: action.payload,
			}
		case ORDER_LIST_MY_FAIL:
			return { loading: false, error: action.payload }
		case ORDER_LIST_MY_RESET:
			return { orders: [] }
		// In any other case, just return the state
		default:
			return state
	}
}
export const orderThisMonthReducer = (state = { total: {sum: -1, total: 0} }, action) => {
	switch (action.type) {
		case ORDER_LIST_MONTH_REQUEST:
			return { loading: true }
		case ORDER_LIST_MONTH_SUCCESS:
			return {
				loading: false,
				total: action.payload,
			}
		case ORDER_LIST_MONTH_FAIL:
			return { loading: false, error: action.payload }
		case ORDER_LIST_MONTH_RESET:
			return { total: {sum: 0, total: 0} }
		// In any other case, just return the state
		default:
			return state
	}
}
export const orderThisYearReducer = (state = { items: [{sum: -1, total: 0}] }, action) => {
	switch (action.type) {
		case ORDER_LIST_YEAR_REQUEST:
			return { loading: true }
		case ORDER_LIST_YEAR_SUCCESS:
			return {
				loading: false,
				total: action.payload,
			}
		case ORDER_LIST_YEAR_FAIL:
			return { loading: false, error: action.payload }
		case ORDER_LIST_YEAR_RESET:
			return { total: {sum: 0, total: 0} }
		// In any other case, just return the state
		default:
			return state
	}
}
export const orderThisYearByProductReducer = (state = { items: [] }, action) => {
	switch (action.type) {
		case ORDER_LIST_YEAR_BY_PRODUCT_REQUEST:
			return { loading: true }
		case ORDER_LIST_YEAR_BY_PRODUCT_SUCCESS:
			return {
				loading: false,
				items: action.payload,
			}
		case ORDER_LIST_YEAR_BY_PRODUCT_FAIL:
			return { loading: false, error: action.payload }
		case ORDER_LIST_YEAR_BY_PRODUCT_RESET:
			return { total: [] }
		// In any other case, just return the state
		default:
			return state
	}
}
export const orderListReducer = (state = { orders: [] }, action) => {
	switch (action.type) {
		case ORDER_LIST_REQUEST:
			return { loading: true }
		case ORDER_LIST_SUCCESS:
			return {
				loading: false,
				orders: action.payload,
			}
		case ORDER_LIST_FAIL:
			return { loading: false, error: action.payload }
		// In any other case, just return the state
		default:
			return state
	}
}
export const orderListUserReducer = (state = { orders: [] }, action) => {
	switch (action.type) {
		case ORDER_LIST_USER_REQUEST:
			return { loading: true }
		case ORDER_LIST_USER_SUCCESS:
			return {
				loading: false,
				orders: action.payload,
			}
		case ORDER_LIST_USER_FAIL:
			return { loading: false, error: action.payload }
		// In any other case, just return the state
		default:
			return state
	}
}
export const orderDeliverReducer = (state = {}, action) => {
	switch (action.type) {
		case ORDER_DELIVER_REQUEST:
			return { loading: true }
		case ORDER_DELIVER_SUCCESS:
			return {
				loading: false,
				success: true,
			}
		case ORDER_DELIVER_FAIL:
			return { loading: false, error: action.payload }
		case ORDER_DELIVER_RESET:
			return {}
		// In any other case, just return the state
		default:
			return state
	}
}

export const orderLastNoReducer = (state = { orderNo: '' }, action) => {
	switch (action.type) {
		case ORDER_LAST_NO_REQUEST:
			return { loading: true }
		case ORDER_LAST_NO_SUCCESS:
			return {
				loading: false,
				orderNo: action.payload,
			}
		case ORDER_LAST_NO_FAIL:
			return { loading: false, error: action.payload }
		case ORDER_LAST_NO_RESET:
			return { orderNo: '' }
		// In any other case, just return the state
		default:
			return state
	}
}

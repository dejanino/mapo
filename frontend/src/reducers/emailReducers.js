import {
	EMAIL_SEND_REQUEST,
	EMAIL_SEND_SUCCESS,
	EMAIL_SEND_FAIL
} from '../constants/emailConstants'

// emailSendReducer for email sending
export const emailSendReducer = (state = { msg: '' }, action) => {
	switch (action.type) {
		// Requests category list
		// categories is an empty array because its still loading
		// loading will NOT be done yet, so true
		case EMAIL_SEND_REQUEST:
			return { loading: true, msg: '' }

		// On success
		// categories will contain data as a payload
		// loading will be done, so false
		case EMAIL_SEND_SUCCESS:
			return {
				loading: false,
				msg: action.payload.msg
			}

		// On fail
		// categories will contain the error as a payload
		// loading will be done, so false
		case EMAIL_SEND_FAIL:
			return { loading: false, error: action.payload }

		// In any other case, just return the state
		default:
			return state
	}
}
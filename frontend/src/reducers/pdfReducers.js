import {
	PDF_REQUEST,
	PDF_SUCCESS,
	PDF_FAIL
} from '../constants/pdfConstants'

// createPdfReducer for creating pdf
export const createPdfReducer = (state = { msg: '' }, action) => {
	switch (action.type) {
		// Requests category list
		// categories is an empty array because its still loading
		// loading will NOT be done yet, so true
		case PDF_REQUEST:
			return { loading: true, msg: '' }

		// On success
		// categories will contain data as a payload
		// loading will be done, so false
		case PDF_SUCCESS:
			return {
				loading: false,
				msg: action.payload.msg
			}

		// On fail
		// categories will contain the error as a payload
		// loading will be done, so false
		case PDF_FAIL:
			return { loading: false, error: action.payload }

		// In any other case, just return the state
		default:
			return state
	}
}
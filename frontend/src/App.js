import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { BrowserRouter as Router, Route, useLocation, withRouter } from 'react-router-dom'
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

import Header from './components/Header'
import Footer from './components/Footer'
import HomeScreen from './screens/HomeScreen'
import ProductScreen from './screens/ProductScreen'
import CartScreen from './screens/CartScreen'
import LoginScreen from './screens/LoginScreen'
import RegisterScreen from './screens/RegisterScreen'
import ProfileScreen from './screens/ProfileScreen'
import ShippingScreen from './screens/ShippingScreen'
import PaymentScreen from './screens/PaymentScreen'
import PlaceOrderScreen from './screens/PlaceOrderScreen'
import OrderScreen from './screens/OrderScreen'
import UserListScreen from './screens/UserListScreen'
import UserEditScreen from './screens/UserEditScreen'
import ProductListScreen from './screens/ProductListScreen'
import ProductEditScreen from './screens/ProductEditScreen'
import OrderListScreen from './screens/OrderListScreen'
import Dashboard from './screens/Dashboard'
import UserOrdersScreen from './screens/UserOrdersScreen'
import AllProductsScreen from './screens/AllProductsScreen'
import { Switch } from 'react-router-dom/cjs/react-router-dom.min'
import Index from './screens/Index'
import OrderAdminScreen from './screens/OrderAdminScreen'
import ProductByCategoryScreen from './screens/ProductByCategoryScreen'
import AboutUsScreen from './screens/AboutUsScreen'
import ContactUsScreen from './screens/ContactUsScreen'

const App = ({ history }) => {
	// const [state, setState] = useState();
	// const [isAdminPanel, setIsAdminPanel] = useState(false)
	// const [locationChanged, setLocationChanged] = useState(false)
	// const [oldLocation, setOldLocation] = useState('')

	// //const isAdminPanel = location.toLowerCase().indexOf('admin') > -1

	// const userLogin = useSelector((state) => state.userLogin)
	// const { userInfo } = userLogin

	// const isAdmin = userInfo.isAdmin

	// useEffect(() => {
	// 	let loc = window.location.href
	// 	console.log(loc)
	// 	setIsAdminPanel(loc.toLowerCase().indexOf('admin') > -1)
	// 	if (oldLocation != loc) {
	// 		setLocationChanged(!locationChanged);
	// 		setOldLocation(loc);
	// 	}
	// }, [state, locationChanged, isAdmin, isAdminPanel])

	const MainLayout = ({ children }) => (
		<>
			<Header />
			<main className='py-3'>
				{/* <Container> */}
					{children}
				{/* </Container> */}
			</main>
			<Footer />
		</>
	);
	const AdminLayout = ({ children }) => (
		<>
			<Header />
			<main className='py-3 full'>
				<Container>
					{children}
				</Container>
			</main>
			<Footer />
		</>
	);

	return (
		<Router>

			<Route exact path={['/login', '/register', '/profile', 'top5', '/shipping', '/payment', '/placeorder', '/order/:id', '/category/:id', '/category/:id/page/:pageNumber',
				 '/products', '/product/:id',
				'/cart/:id?', '/search/:keyword', '/search/:keyword/page/:pageNumber', '/page/:pageNumber', '/', '/about-us', '/contact-us']}>
				<MainLayout>
					{/* LoginScreen */}
					<Route path='/login' component={LoginScreen} />
					{/* RegisterScreen */}
					<Route path='/register' component={RegisterScreen} />
					{/* ProfileScreen */}
					<Route path='/profile' component={ProfileScreen} />
					{/* ShippingScreen */}
					<Route path='/shipping' component={ShippingScreen} />
					{/* PaymentScreen */}
					<Route path='/payment' component={PaymentScreen} />
					{/* PlaceOrderScreen */}
					<Route path='/placeorder' component={PlaceOrderScreen} />
					{/* OrderScreen */}
					<Route path='/order/:id' component={OrderScreen} />
					{/* ProductByCategoryScreen */}
					<Route path='/category/:id' component={ProductByCategoryScreen} exact />
					<Route path='/category/:id/page/:pageNumber' component={ProductByCategoryScreen} />
					{/* AllProductsScreen */}
					<Route path='/products' component={AllProductsScreen} />
					{/* ProductScreen */}
					<Route path='/product/:id' component={ProductScreen} />
					{/* CartScreen */}
					<Route path='/cart/:id?' component={CartScreen} />
					{/* HomeScreen Search */}
					<Route path='/search/:keyword' component={HomeScreen} exact />
					{/* HomeScreen Search Page number */}
					<Route path='/search/:keyword/page/:pageNumber' component={HomeScreen} exact />
					{/* HomeScreen Page number */}
					<Route path='/page/:pageNumber' component={HomeScreen} exact />
					{/* HomeScreen */}
					<Route path='/' component={HomeScreen} exact />
					<Route path='/about-us' component={AboutUsScreen} exact />
					<Route path='/contact-us' component={ContactUsScreen} exact />
				</MainLayout>
			</Route>
			<Route exact path={['/admin', '/admin/index', '/admin/userlist', '/admin/orderlist', '/admin/user/:id/edit', '/admin/userorders/:id', '/admin/productlist',
				'/admin/productlist/:pageNumber', '/admin/categorylist', '/admin/product/:id/edit', '/admin/category/:id/edit', '/admin/order/:id']}>
				<AdminLayout>
					<Route path='/admin' component={Dashboard} />
				</AdminLayout>
			</Route>
		</Router >
	)
}

export default App

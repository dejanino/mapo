import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import {
	productListReducer,
	productDetailsReducer,
	productDeleteReducer,
	productCreateReducer,
	productUpdateReducer,
	productReviewCreateReducer,
	productTopRatedReducer,
} from './reducers/productReducers'
import {
	categoryListReducer,
	categoryDetailsReducer,
	categoryDeleteReducer,
	categoryCreateReducer,
	categoryUpdateReducer
} from './reducers/categoryReducers'
import { cartReducer } from './reducers/cartReducers'
import { emailSendReducer } from './reducers/emailReducers'
import { createPdfReducer } from './reducers/pdfReducers'
import {
	userLoginReducer,
	userRegisterReducer,
	userDetailsReducer,
	referralDetailsReducer,
	userUpdateProfileReducer,
	userListReducer,
	userDeleteReducer,
	userUpdateReducer,
	userTop5Reducer
} from './reducers/userReducers'
import {
	orderCreateReducer,
	orderDetailsReducer,
	orderPayReducer,
	orderListMyReducer,
	orderListUserReducer,
	orderListReducer,
	orderDeliverReducer,
	orderThisMonthReducer,
	orderThisYearReducer,
	orderThisYearByProductReducer,
	orderLastNoReducer,
} from './reducers/orderReducers'

const reducer = combineReducers({
	productList: productListReducer,
	productDetails: productDetailsReducer,
	categoryList: categoryListReducer,
	categoryDetails: categoryDetailsReducer,
	cart: cartReducer,
	userLogin: userLoginReducer,
	userRegister: userRegisterReducer,
	userDetails: userDetailsReducer,
	referralDetails: referralDetailsReducer,
	userUpdateProfile: userUpdateProfileReducer,
	userList: userListReducer,
	top5Users: userTop5Reducer,
	userUpdate: userUpdateReducer,
	userDelete: userDeleteReducer,
	orderCreate: orderCreateReducer,
	orderDetails: orderDetailsReducer,
	orderPay: orderPayReducer,
	orderListMy: orderListMyReducer,
	orderListUser: orderListUserReducer,
	orderThisMonth: orderThisMonthReducer,
	orderThisYear: orderThisYearReducer,
	orderThisYearByProduct: orderThisYearByProductReducer,
	orderDeliver: orderDeliverReducer,
	orderLastNo: orderLastNoReducer,
	productDelete: productDeleteReducer,
	productCreate: productCreateReducer,
	productUpdate: productUpdateReducer,
	categoryDelete: categoryDeleteReducer,
	categoryCreate: categoryCreateReducer,
	categoryUpdate: categoryUpdateReducer,
	orderList: orderListReducer,
	productReviewCreate: productReviewCreateReducer,
	productTopRated: productTopRatedReducer,
	emailSend: emailSendReducer,
	createPdf: createPdfReducer,
})

// Get cartItems from local storage
const cartItemsFromStorage = localStorage.getItem('cartItems')
	? JSON.parse(localStorage.getItem('cartItems'))
	: []
// Get userInfo from local storage
const userInfoFromStorage = localStorage.getItem('userInfo')
	? JSON.parse(localStorage.getItem('userInfo'))
	: null
// Get shippingAddress from local storage
const shipppingAddressFromStorage = localStorage.getItem('shippingAddress')
	? JSON.parse(localStorage.getItem('shippingAddress'))
	: {}
// Get paymentMethod from local storage
const paymentMethodFromStorage = localStorage.getItem('paymentMethod')
	? JSON.parse(localStorage.getItem('paymentMethod'))
	: {}

// Create initial state
const initialState = {
	cart: {
		cartItems: cartItemsFromStorage,
		shippingAddress: shipppingAddressFromStorage,
		paymentMethod: paymentMethodFromStorage,
	},
	userLogin: { userInfo: userInfoFromStorage },
}

// Set middleware to thunk middleware
const middleware = [thunk]

// Create store
// Pass initial state to load things at that point
// Setup redux-devtools-extension
const store = createStore(
	reducer,
	initialState,
	composeWithDevTools(applyMiddleware(...middleware))
)

export default store

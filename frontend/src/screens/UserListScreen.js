import React, { useEffect, useState } from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { Table, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { listUsers, deleteUser } from '../actions/userActions'

const UserListScreen = ({ history }) => {
	const [doSearch, setDoSearch] = useState(false)
	const [query, setQuery] = useState('')

	const dispatch = useDispatch()

	const userList = useSelector((state) => state.userList)
	const { loading, error, users } = userList

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	const userDelete = useSelector((state) => state.userDelete)
	const { success: successDelete } = userDelete

	useEffect(() => {
		if (userInfo && userInfo.isAdmin) {
			dispatch(listUsers(query))
		} else {
			history.push('/login')
		}
	}, [dispatch, userInfo, history, successDelete, doSearch])

	const deleteHandler = (id) => {
		if (window.confirm('Are you sure?')) {
			dispatch(deleteUser(id))
		}
	}

	function trigerSearch() {
		setDoSearch(!doSearch)
		//setQuery('');
	}

	return (
		<>
			<p></p>
			<div className="input-group">
				<input
					className="form-control"
					type="text"
					placeholder="Pretraga kupaca..."
					aria-label="search input"
					onChange={(e) => setQuery(e.target.value)}
					value={query}
				/>
				<button className="btn btn-primary" type="button" onClick={trigerSearch}>
					<i className="fas fa-search fa-sm"></i>
				</button>
			</div>
			<p></p>
			{loading ? (
				<Loader />
			) : error ? (
				<Message variant='danger'>{error}</Message>
			) : (
				<Table bordered hover responsive className='table-sm'>
					<thead>
						<tr>
							<th>Lični kod</th>
							<th>Ime i prezime</th>
							<th>Email</th>
							<th>Admin</th>
							<th>Izmena</th>
							<th>Brisanje</th>
							<th>Porudžbine</th>
						</tr>
					</thead>
					<tbody>
						{users.map((user) => (
							<tr key={user._id}>
								<td>{user.personalCode}</td>
								<td>{user.name}</td>
								<td>
									<a href={`mailto:${user.email}`}>{user.email}</a>
								</td>
								<td>
									{user.isAdmin ? (
										<i className='fas fa-check' style={{ color: 'green' }}></i>
									) : (
										<i className='fas fa-times' style={{ color: 'red' }}></i>
									)}
								</td>
								{/* Edit button */}
								<td>
									<LinkContainer to={`/admin/user/${user._id}/edit`}>
										<Button variant='light' className='btn-sm'>
											<i className='fas fa-edit'></i>
										</Button>
									</LinkContainer>
								</td>
								{/* Delete button */}
								<td>
									<Button
										variant='danger'
										className='btn-sm'
										onClick={() => deleteHandler(user._id)}
									>
										<i className='fas fa-trash'></i>
									</Button>
								</td>
								{/* Details button */}
								<td>
									<LinkContainer to={`/admin/userorders/${user._id}`}>
										<Button variant='light' className='btn-sm'>
											Detalji
										</Button>
									</LinkContainer>
								</td>
							</tr>
						))}
					</tbody>
				</Table>
			)}
		</>
	)
}

export default UserListScreen

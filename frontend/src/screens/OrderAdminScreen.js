import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Button, Row, Col, ListGroup, Image, Card } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { applyMiddleware, combineReducers } from "redux"
import { configureStore } from "@reduxjs/toolkit";
import Message from '../components/Message'
import Loader from '../components/Loader'
import moment from 'moment'

import ReactPDF, { renderToFile, PDFDownloadLink } from '@react-pdf/renderer';
import PDF from '../components/PDF'

import { updateUser, getUserDetailsByPersonalCode, updateMultipleUsers } from '../actions/userActions'
import {
	getOrderDetails,
	payOrderManually,
	deliverOrder,
} from '../actions/orderActions'
import {
	ORDER_PAY_RESET,
	ORDER_DELIVER_RESET,
	ORDER_CREATE_SUCCESS,
} from '../constants/orderConstants'
import { REFERRAL_DETAILS_SUCCESS, USER_UPDATE_SUCCESS } from '../constants/userConstants'

const OrderAdminScreen = ({ match, history }) => {
	const orderId = match.params.id

	const dispatch = useDispatch()

	const orderDetails = useSelector((state) => state.orderDetails)
	const { order, loading, error } = orderDetails

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	// Get success value from get user by referral code
	const referralDetails = useSelector((state) => state.referralDetails)
	const { loading: referralLoading, success: referralSuccess, referral } = referralDetails

	// const orderPay = useSelector((state) => state.orderPay)
	// const { loading: loadingPay, success: successPay } = orderPay

	const orderPay = useSelector((state) => state.orderPay)
	const { loading: loadingPay, success: successPay } = orderPay

	const orderDeliver = useSelector((state) => state.orderDeliver)
	const { loading: loadingDeliver, success: successDeliver } = orderDeliver

	const [referralCode, setReferralCode] = useState('')
	const [clicked, setClicked] = useState(false)
	const [promises, setPromises] = useState({})
	const [counter, setCounter] = useState(0)

	if (!loading) {
		// Calculate prices
		// Add two decimals to price if needed
		const addDecimals = (num) => {
			return (Math.round(num * 100) / 100).toFixed(2)
		}
		// Items price
		order.itemsPrice = addDecimals(
			order.orderItems.reduce((acc, item) => acc + item.price * item.qty, 0)
		)
	}

	useEffect(() => {
		// To get PAYPAL_CLIENT_ID
		// const addPayPalScript = async () => {
		// 	const { data: clientId } = await axios.get('/api/config/paypal')
		// 	// Create the script
		// 	const script = document.createElement('script')
		// 	script.type = 'text/javascript'
		// 	script.src = `https://www.paypal.com/sdk/js?client-id=${clientId}`
		// 	script.async = true
		// 	script.onload = () => {
		// 		setSdkReady(true)
		// 	}
		// 	document.body.appendChild(script)
		// }

		if (!order || successPay || successDeliver || order._id !== orderId) {
			dispatch({ type: ORDER_PAY_RESET })
			dispatch({ type: ORDER_DELIVER_RESET })
			dispatch(getOrderDetails(orderId))
			// if not paid add paypal script
		}
		// else if (!order.isPaid) {
		// 	// if (!window.paypal) {
		// 	// 	addPayPalScript()
		// 	// } else {
		// 	// 	setSdkReady(true)
		// 	// }
		// }

		else {
			if (referral && referral._id && clicked) {
				setReferralCode(referral.referralCode)
				_updateUser()
			}

		}
		if (promises.length > 0 && referralCode == "" && counter == 0) {
			setCounter(counter + 1)
			dispatch(updateMultipleUsers(promises))
		}
		if (order && order._id != undefined) {
			//setReferralCode(order.user.referralCode)
		}
	}, [/*dispatch, */orderId, successPay, successDeliver, order, referral, referralCode == ""]) // Dependencies, on change they fire off useEffect

	const successPaymentHandler = async (paymentResult) => {
		dispatch(payOrderManually(orderId, paymentResult))

		handleReferrals()
	}

	let previous = ""
	const _updateUser = async () => {
		const points = (order.totalPrice * 2 / 100).toFixed(2)
		const referralPoints = (referral.referralPoints + parseFloat(points)).toFixed(2)

		if (referral._id) {
			let prom = {
				_id: referral._id,
				referralPoints
			}

			let list = []
			if (promises.length > 0) {
				promises.forEach(pr => {
					list.push(pr)
				});
			}
			if (list.find(x => x._id == prom._id) == undefined) {
				list.push(prom)
			}
			setPromises(list)
			if (previous != referral.referralCode) {
				dispatch(getUserDetailsByPersonalCode(referral.referralCode))
				previous = referral.referralCode
			}
		}
		else if (referralCode != referral.referralCode && referralCode != "") {
		}
	}

	const handleReferrals = () => {
		setClicked(true)
		dispatch(getUserDetailsByPersonalCode(order.user.referralCode))
			.then((response) => {
				_updateUser()
			})
			.then((response) => {

			})
	}

	const delay = (ms) => new Promise(resolve =>
		setTimeout(resolve, ms)
	);

	const fetchObjects = async () => {
		debugger
		return dispatch => {
			dispatch(getUserDetailsByPersonalCode(order.user.referralCode));

			return delay(4000).then(() => {
				updateUser()
			});
		}
	}

	const deliverHandler = () => {
		dispatch(deliverOrder(order))
	}

	const goBack = (e) => {
		e.preventDefault();
		if (userInfo.isAdmin) {
			history.goBack()
		}
		else {
			history.push('/profile')
		}
	}

	return loading ? (
		<Loader />
	) : error ? (
		<Message variant='danger'>{error}</Message>
	) : (
		<>

			<Link
				// TODO if admin has an order go back to /profile
				to={userInfo.isAdmin ? '/admin/orderlist' : '/profile'}
				className='btn btn-light my-3'
				onClick={(e) => goBack(e)}
			>
				Nazad
			</Link>
			<h1>Porudžbina {order.orderNo}</h1>
			<Row>
				{/* Left Steps Summary */}
				<Col md={8}>
					<ListGroup variant='flush'>
						<ListGroup.Item>
							<h2>Podaci za dostavu</h2>

							<p>
								<span className='push-to-right'>
									<strong>Broj profakture: </strong> {order.orderNo}
								</span>
							</p>

							<p>
								<span className='push-to-right'>
									<strong>Ime i prezime: </strong> {order.user.name}
								</span>
							</p>

							<p>
								<span className='push-to-right'>
									<strong>Email: </strong>
									<a href={`mailto:${order.user.email}`}>{order.user.email}</a>
								</span>
							</p>

							<p>
								<span className='push-to-right'>
									<strong>Adresa: </strong>
									{order.shippingAddress.address}, {order.shippingAddress.postalCode}{' '}{order.shippingAddress.city}
									,{' '}
									{order.shippingAddress.phone}
								</span>
							</p>
							{order.isDelivered ? (
								<Message variant='success'>
									Dostavljeno dana {moment(order.deliveredAt).format('DD.MM.yyyy.')}
								</Message>
							) : (
								<Message variant='danger'>Nije dostavljeno</Message>
							)}
						</ListGroup.Item>
						<ListGroup.Item>
							<h2>Način plaćanja</h2>
							<p>
								<span className='push-to-right'>
									<strong>Metod: </strong>
									{order.paymentMethod}
								</span>
							</p>
							{order.isPaid ? (
								<Message variant='success'>Plaćeno dana {moment(order.paidAt).format('DD.MM.yyyy.')}</Message>
							) : (
								<Message variant='danger'>Nije plaćeno</Message>
							)}
						</ListGroup.Item>
						<ListGroup.Item>
							<h2>Poručeni proizvodi</h2>
							{order.orderItems.length === 0 ? (
								<Message>Vaša porudžbina je prazna</Message>
							) : (
								<ListGroup variant='flush'>
									{order.orderItems.map((item, index) => (
										<ListGroup.Item key={index}>
											<Row>
												<Col md={1}>
													<Image
														src={item.image}
														alt={item.name}
														fluid
														rounded
													/>
												</Col>
												<Col>
													<Link to={`/product/${item.product}`}>
														{item.name}
													</Link>
												</Col>
												<Col md={4}>
													{item.qty} x R{item.price} = R{item.qty * item.price}
												</Col>
											</Row>
										</ListGroup.Item>
									))}
								</ListGroup>
							)}
						</ListGroup.Item>
						<ListGroup.Item>
							{order.orderItems.length > 0 && <Row>
								<h2>
									<PDFDownloadLink document={<PDF order={order} />} fileName={orderId + ".pdf"}>
										{({ blob, url, loading, error }) =>
											loading ? 'Učitavanje fakture...' : 'Preuzmite fakturu'
										}
									</PDFDownloadLink>
								</h2>
							</Row>}
						</ListGroup.Item>
					</ListGroup>
				</Col>
				{/* Right Order Summary */}
				<Col md={4}>
					<Card>
						<ListGroup variant='flush'>
							<ListGroup.Item>
								<h2>Pregled porudžbine</h2>
							</ListGroup.Item>
							<ListGroup.Item className='push-to-right'>
								<Row>
									<Col>Proizvodi</Col>
									<Col>{order.itemsPrice} din</Col>
								</Row>
							</ListGroup.Item>
							<ListGroup.Item className='push-to-right'>
								<Row>
									<Col>Poštarina</Col>
									<Col>{order.shippingPrice} din</Col>
								</Row>
							</ListGroup.Item>
							{/* <ListGroup.Item className='push-to-right'>
								<Row>
									<Col>Tax</Col>
									<Col>R{order.taxPrice}</Col>
								</Row>
							</ListGroup.Item> */}
							<ListGroup.Item className='push-to-right'>
								<Row>
									<Col>
										<strong>Ukupno</strong>
									</Col>
									<Col>
										<strong>{order.totalPrice} din</strong>
									</Col>
								</Row>
							</ListGroup.Item>
							{!order.isPaid && (
								// <ListGroup.Item>
								// 	{loadingPay && <Loader />}
								// 	{!sdkReady ? (
								// 		<Loader />
								// 	) : (
								// 		// <PayPalButton
								// 		// 	amount={order.totalPrice}
								// 		// 	onSuccess={successPaymentHandler}
								// 		// />
								// 		<></>
								// 	)}
								// </ListGroup.Item>
								<ListGroup.Item>
									{loadingPay && <Loader />}
									<Button
										type='button'
										className='btn btn-block'
										onClick={successPaymentHandler}
									>
										Označi kao plaćeno
									</Button>
								</ListGroup.Item>
							)}
							{loadingDeliver && <Loader />}
							{userInfo.isAdmin && order.isPaid && !order.isDelivered && (
								<ListGroup.Item>
									<Button
										type='button'
										className='btn btn-block'
										onClick={deliverHandler}
									>
										Označi kao dostavljeno
									</Button>
								</ListGroup.Item>
							)}
						</ListGroup>
					</Card>
				</Col>
			</Row>
		</>
	)
}

export default OrderAdminScreen

import React, { useState, useEffect } from 'react'
import { Form, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import FormContainer from '../components/FormContainer'
import CheckoutSteps from '../components/CheckoutSteps'
import { saveShippingAddress } from '../actions/cartActions'
import { USER_LIST_SUCCESS } from '../constants/userConstants'
import { getUserDetails } from '../actions/userActions'

const ShippingScreen = ({ history }) => {
	// useSelector is to grab what we want from the state
	const userDetails = useSelector((state) => state.userDetails)
	const { loading, error, user } = userDetails

	console.log(user)

	// useSelector is to grab the cart from the state
	const cart = useSelector((state) => state.cart)
	const { shippingAddress } = cart

	const [address, setAddress] = useState(shippingAddress.address)
	const [city, setCity] = useState(shippingAddress.city)
	const [postalCode, setPostalCode] = useState(shippingAddress.postalCode)
	const [phone, setPhone] = useState(shippingAddress.phone)
	const [country, setCountry] = useState(shippingAddress.country)
	const [email, setEmail] = useState(shippingAddress.email)

	const dispatch = useDispatch()

	// make request here upon component load
	useEffect(
		() => {
			if (!userDetails) {
				history.push('/login')
			} else {
				if (!user || !user.name) {
					//dispatch({ type: USER_LIST_SUCCESS })
					//dispatch(getUserDetails('profile'))
				} else {
					setEmail(user.email)
					setAddress(user.address)
					setCity(user.city)
					setPostalCode(user.postcode)
					setPhone(user.phone)
				}
			}
		},
		[dispatch, history, userDetails, user] // Dependencies, on change they fire off useEffect
	)

	const submitHandler = (e) => {
		e.preventDefault()
		dispatch(saveShippingAddress({ address, city, postalCode, phone, email, country }))
		history.push('/payment')
	}

	return (
		<FormContainer>
			<CheckoutSteps step1 step2 />
			<h1>Podaci za dostavu</h1>
			<Form onSubmit={submitHandler}>
				{/* Address */}
				<Form.Group controlId='address'>
					<Form.Label>Adresa</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite adresu za dostavu'
						value={address}
						required
						onChange={(e) => setAddress(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* City */}
				<Form.Group controlId='city'>
					<Form.Label>Grad</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite naziv grada'
						value={city}
						required
						onChange={(e) => setCity(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Postal Code */}
				<Form.Group controlId='postalCode'>
					<Form.Label>Poštanski broj</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite poštanski broj'
						value={postalCode}
						required
						onChange={(e) => setPostalCode(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Phone */}
				<Form.Group controlId='phone'>
					<Form.Label>Broj telefona</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite broj telefona'
						value={phone}
						onChange={(e) => setPhone(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Email */}
				<Form.Group controlId='email'>
					<Form.Label>Email adresa</Form.Label>
					<Form.Control
						type='email'
						placeholder='Unesite email adresu'
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Country */}
				{/* <Form.Group controlId='country'>
					<Form.Label>Država</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite naziv države'
						value={country}
						required
						onChange={(e) => setCountry(e.target.value)}
					></Form.Control>
				</Form.Group> */}
				<Button type='submit' variant='primary'>
					Dalje
				</Button>
			</Form>
		</FormContainer>
	)
}

export default ShippingScreen

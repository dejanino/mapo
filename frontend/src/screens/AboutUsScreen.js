import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col } from 'react-bootstrap'
import Product from '../components/Product'
import Category from '../components/Category'
import Message from '../components/Message'
import Loader from '../components/Loader'
import Paginate from '../components/Paginate'
import ProductCarousel from '../components/ProductCarousel'
import Meta from '../components/Meta'
import { listProducts } from '../actions/productActions'
import { listCategories } from '../actions/categoryActions'

const AboutUsScreen = ({ match }) => {
    const dispatch = useDispatch()

    // make request here upon component load
    useEffect(
        () => {

        },
        [dispatch] // Dependencies, on change they fire off useEffect
    )
    return (
        <>
            <Meta title={'O nama'} />
            <div className='contact-us about'>
                <div className='container center'>
                    <h2 className=''>O nama</h2>
                </div>
            </div>
            <div className='container top20'>
                <Link className='btn btn-light' to='/'>
                    Nazad
                </Link>
                <div className='container '>
                    <p>Osnovana 1980. godine, Herbalife Nutrition je vodeća kompanija za wellness čiji je cilj da vam pojednostavi put do optimalnog načina života.</p>
                    <h2>Herbalife visokokvalitetni proizvodi i programi nude:</h2>
                    <ul>
                        <li>uspešnu kontrolu težine</li>
                        <li>kvalitetniju ishranu</li>
                        <li>bolju ličnu negu</li>
                        <li>priliku za dodatnu zaradu</li>
                    </ul>
                    <h2>Proizvodi koji se proizvode na naučnoj osnovi, namenjeni vašem uspehu</h2>
                    <p>Herbalife inovativne proizvode razvili su naučnici, lekari i nutricionisti koji su se vodili vašim ličnim ciljevima u vezi se zdravljem. I dok su Herbalife formulacije inspirisane prirodom, osmišljene da vam omoguće da doživotno uživate u njihovim blagotvornim svojstvima, prednosti koje nudi poslovna prilika vam otvaraju mogućnost da ostvarite i finansijski uspeh.</p>

                    <h2>Stručnjaci kojima možete verovati</h2>
                    Herbalife-ov Naučni savetodavni odbor, Grupa za zdravstvena pitanja i Medicinski savetodavni odbor su pokretačke snage zaslužne za Herbalife-ov uspeh i zauzimaju vodeće mesto u branši. Ovaj tim uglednih naučnih stručnjaka usmerava razvoj Herbalife proizvoda kroz istraživanje i ispitivanje proizvoda, tako da kvalitet Herbalife-a zadovoljava najviše standarde koji su danas zadati za ovu privrednu granu.

                    <h2>Podrška na koju možete računati</h2>
                    Uz tim od više od 1.800.000 nezavisnih Članova koji Herbalife proizvode prodaju u više od 60 zemalja širom sveta (i uz godišnji maloprodajni promet od 1,8 milijarde US $) možete biti sigurni da ćete uvek imati podršku koja vam je potrebna da biste uspeli. Kada budete spremni da promenite svoj život, s ponosom ćemo vam pomoći da uspete na svakom koraku i omogućiti vam da:
                    <ul>
                        <li>zadovoljite svoje potrebe u pogledu ishrane, gubitka težine i lične nege</li>
                        <li>naučite više o našoj poslovnoj prilici</li>
                    </ul>
                    <p>Uz Herbalife, kvalitetne proizvode i stručnu podršku lako ćete izgledati, osećati se i izgledati što je najbolje moguće. Otkrijte Herbalife – i otkrićete doživotno optimalno zdravlje.</p>
                </div>
            </div>
        </>
    )
}

export default AboutUsScreen

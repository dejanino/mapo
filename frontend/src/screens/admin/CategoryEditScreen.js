import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../../components/Message'
import Loader from '../../components/Loader'
import FormContainer from '../../components/FormContainer'
import { createCategory, listCategoryDetails, updateCategory } from '../../actions/categoryActions'
import { CATEGORY_CREATE_RESET, CATEGORY_CREATE_SUCCESS, CATEGORY_UPDATE_RESET } from '../../constants/categoryConstants'

const CategoryEditScreen = ({ match, history }) => {
	const categoryId = match.params.id

	const [name, setName] = useState('')
	const [image, setImage] = useState('')
	const [uploading, setUploading] = useState(false)

	const dispatch = useDispatch()

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	// useSelector is to grab what we want from the state
	const categoryDetails = useSelector((state) => state.categoryDetails)
	const { loading, error, category } = categoryDetails

	const categoryUpdate = useSelector((state) => state.categoryUpdate)
	const {
		loading: loadingUpdate,
		error: errorUpdate,
		success: successUpdate,
	} = categoryUpdate

	const categoryCreate = useSelector((state) => state.categoryCreate)
	const {
		loading: loadingCreate,
		error: errorCreate,
		success: successCreate,
	} = categoryCreate

	

	// make request here upon component load
	useEffect(() => {
		if (successUpdate) {
			dispatch({ type: CATEGORY_UPDATE_RESET })
			dispatch(listCategoryDetails(categoryId))
			history.push('/admin/categorylist')
		}
		else if (successCreate) {
			dispatch({ type: CATEGORY_CREATE_RESET })
			history.push('/admin/categorylist')
		} else {
			if ((!category.name || category._id !== categoryId) && categoryId != "0") {
				dispatch(listCategoryDetails(categoryId))
			} else {
				setName(category.name)
				setImage(category.image)
			}
		}
	}, [successUpdate, successCreate, dispatch, history, category, categoryId]) // Dependencies, on change they fire off useEffect)

	const uploadFileHandler = async (e) => {
		const file = e.target.files[0]

		const formData = new FormData()
		formData.append('image', file)
		setUploading(true)
		try {
			const config = {
				headers: {
					'Content-Type': 'multipart/form-data',
				},
			}

			// Making post request to upload the image
			const { data } = await axios.post('/api/upload', formData, config)

			setImage(data)
			setUploading(false)
		} catch (error) {
			console.error(error)
			setUploading(false)
		}
	}

	const submitHandler = (e) => {
		e.preventDefault()
		if (categoryId != "0") {
			dispatch(
				updateCategory({
					_id: categoryId,
					name,
					image
				})
			)
		}
		else {
			dispatch(
				createCategory({
					name,
					userInfo,
					image
				})
			)
		}
	}

	return (
		<>
			<Link to='/admin/categorylist' className='btn btn-light my-3'>
				Nazad
			</Link>
			<FormContainer>
				<h1>Podaci kategorije</h1>
				{loadingUpdate && <Loader />}
				{errorUpdate && <Message variant='danger'>{errorUpdate}</Message>}
				{/* On error, display message/error
            When loading, display Loading... */}
				{loading ? (
					<Loader />
				) : error ? (
					<Message variant='danger'>{error}</Message>
				) : (
					<Form onSubmit={submitHandler}>
						{/* Name */}
						<Form.Group controlId='name'>
							<Form.Label>Naziv</Form.Label>
							<Form.Control
								type='text'
								placeholder='Unesite naziv kategorije'
								value={name}
								onChange={(e) => setName(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Image */}
						<Form.Group controlId='image'>
							<Form.Label>Fotografija</Form.Label>
							<Form.Control
								type='text'
								placeholder='Putanja do fotografije kategorije'
								value={image}
								onChange={(e) => setImage(e.target.value)}
							></Form.Control>
							{/* Image file */}
							<Form.File
								id='image-file'
								accept="image/*"
								label='Odaberite fotografiju'
								custom
								onChange={uploadFileHandler}
							></Form.File>
							{uploading && <Loader />}
						</Form.Group>
						{/* Button */}
						<Button type='submit' variant='primary'>
							Snimanje
						</Button>
					</Form>
				)}
			</FormContainer>
		</>
	)
}

export default CategoryEditScreen

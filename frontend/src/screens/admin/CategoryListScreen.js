import React, { useEffect } from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { Table, Button, Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../../components/Message'
import Loader from '../../components/Loader'
import {
	listCategories,
	deleteCategory,
	createCategory,
} from '../../actions/categoryActions'
import { CATEGORY_CREATE_REQUEST, CATEGORY_CREATE_RESET, CATEGORY_CREATE_SUCCESS } from '../../constants/categoryConstants'
const CategoryListScreen = ({ history, match }) => {
	const dispatch = useDispatch()

	const categoryList = useSelector((state) => state.categoryList)
	const { loading, error, categories } = categoryList

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	const categoryDelete = useSelector((state) => state.categoryDelete)
	const {
		loading: loadingDelete,
		error: errorDelete,
		success: successDelete,
	} = categoryDelete

	const categoryCreate = useSelector((state) => state.categoryCreate)
	const {
		loading: loadingCreate,
		error: errorCreate,
		success: successCreate,
		category: createdCategory,
	} = categoryCreate

	useEffect(() => {
		dispatch({ type: CATEGORY_CREATE_RESET })

		if (!userInfo || !userInfo.isAdmin) {
			history.push('/login')
		}
		if (successCreate && createdCategory != null) {
			history.push(`/admin/category/${createdCategory._id}/edit`)
		}
		else if (successCreate && createdCategory == null) {
			history.push(`/admin/category/${0}/edit`)
		} else {
			dispatch(listCategories(''))
		}
	}, [
		dispatch,
		userInfo,
		history,
		successDelete,
		successCreate,
		createdCategory
	])

	const deleteHandler = (id) => {
		if (window.confirm('Are you sure?')) {
			dispatch(deleteCategory(id))
		}
	}

	const createCategoryHandler = () => {
		const data = null;
		dispatch({ type: CATEGORY_CREATE_SUCCESS, payload: data })
	}

	return (
		<>
			<Row className='align-items-center'>
				<Col className='text-right'>
					<Button className='my-3' onClick={createCategoryHandler}>
						<span className='plus-sign-margin'>
							<i className='fas fa-plus'></i>
						</span>
						Nova kategorija
					</Button>
				</Col>
			</Row>
			{loadingCreate && <Loader />}
			{errorCreate && <Message variant='danger'>{errorCreate}</Message>}
			{loadingDelete && <Loader />}
			{errorDelete && <Message variant='danger'>{errorDelete}</Message>}
			{loading ? (
				<Loader />
			) : error ? (
				<Message variant='danger'>{error}</Message>
			) : (
				<>
					<Table bordered hover responsive className='table-sm'>
						<thead>
							<tr>
								{/* <th>ID</th> */}
								<th>Naziv</th>
								<th>Izmena</th>
								<th>Brisanje</th>
							</tr>
						</thead>
						<tbody>
							{categories.map((category) => (
								<tr key={category._id}>
									{/* <td>{category._id}</td> */}
									<td>{category.name}</td>
									{/* Edit button */}
									<td>
										<LinkContainer to={`/admin/category/${category._id}/edit`}>
											<Button variant='light' className='btn-sm'>
												<i className='fas fa-edit'></i>
											</Button>
										</LinkContainer>
									</td>
									{/* Delete button */}
									<td>
										<Button
											variant='danger'
											className='btn-sm'
											onClick={() => deleteHandler(category._id)}
										>
											<i className='fas fa-trash'></i>
										</Button>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
				</>
			)}
		</>
	)
}

export default CategoryListScreen

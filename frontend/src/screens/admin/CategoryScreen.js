import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col, Image, ListGroup, Card, Button, Form } from 'react-bootstrap'
import Rating from '../components/Rating'
import Message from '../components/Message'
import Loader from '../components/Loader'
import Meta from '../components/Meta'
import {
	listCategoryDetails,
	createCategoryReview,
} from '../actions/categoryActions'
import { CATEGORY_CREATE_REVIEW_RESET } from '../constants/categoryConstants'

const CategoryScreen = ({ history, match }) => {
	const dispatch = useDispatch()

	// useSelector is to grab what we want from the state
	const categoryDetails = useSelector((state) => state.categoryDetails)
	const { loading, error, category } = categoryDetails

	// Make sure user is logged in
	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	// make request here upon component load
	useEffect(
		() => {
			// Fire off action to get a single category
			if (!category._id || category._id !== match.params.id) {
				dispatch(listCategoryDetails(match.params.id))
				dispatch({ type: CATEGORY_CREATE_REVIEW_RESET })
			}
		},
		[dispatch, match, category._id] // Dependencies, on change they fire off useEffect
	)

	return (
		<>
			<div className='container'>
				{/* Back button */}
				<Link className='btn btn-light my-3' to='/'>
					Nazad
				</Link>
				{/* When loading, display Loading...
            On error, display error
            Else display the category details */}
				{loading ? (
					<Loader />
				) : error ? (
					<Message variant='danger'>{error}</Message>
				) : (
					<>
						<Meta title={category.name} />
						<Row>
							{/* Product image */}
							<Col md='6'>
								<Image src={category.image} alt={category.name} fluid />
							</Col>
							<Col md='3'>
								{/* Product name */}
								<ListGroup variant='flush'>
									<ListGroup.Item>
										<h3>{category.name}</h3>
									</ListGroup.Item>
								</ListGroup>
							</Col>
						</Row>
					</>
				)}
			</div>
		</>
	)
}

export default CategoryScreen

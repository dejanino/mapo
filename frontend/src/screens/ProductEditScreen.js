import axios from 'axios'
import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import FormContainer from '../components/FormContainer'
import { createProduct, listProductDetails, updateProduct } from '../actions/productActions'
import { PRODUCT_CREATE_RESET, PRODUCT_CREATE_SUCCESS, PRODUCT_UPDATE_RESET } from '../constants/productConstants'
import { listCategories } from '../actions/categoryActions'

const ProductScreen = ({ match, history }) => {
	const productId = match.params.id

	const [name, setName] = useState('')
	const [price, setPrice] = useState(0)
	const [image, setImage] = useState('')
	const [brand, setBrand] = useState('')
	const [countInStock, setCountInStock] = useState(0)
	const [category, setCategory] = useState()
	const [options, setOptions] = useState()
	const [selectedOption, setSelectedOption] = useState()
	const [description, setDescription] = useState('')
	const [uploading, setUploading] = useState(false)

	const dispatch = useDispatch()

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	const categoryList = useSelector((state) => state.categoryList)
	const { loading: loadingCategories, error: errorCategories, categories } = categoryList

	// useSelector is to grab what we want from the state
	const productDetails = useSelector((state) => state.productDetails)
	const { loading, error, product } = productDetails

	const productUpdate = useSelector((state) => state.productUpdate)
	const {
		loading: loadingUpdate,
		error: errorUpdate,
		success: successUpdate,
	} = productUpdate

	const productCreate = useSelector((state) => state.productCreate)
	const {
		loading: loadingCreate,
		error: errorCreate,
		success: successCreate,
	} = productCreate

	const getCategoryName = (id) => {
		const one = categories.find(x => x._id == id)
		console.log(one)
		return one.name
	}

	// make request here upon component load
	useEffect(() => {
		if (successUpdate) {
			dispatch({ type: PRODUCT_UPDATE_RESET })
			dispatch(listCategories())
			dispatch(listProductDetails(productId))
			history.push('/admin/productlist')
		}
		else if (successCreate) {
			dispatch({ type: PRODUCT_CREATE_RESET })
			history.push('/admin/productlist')
		}
		else {
			if (!categories || categories.length == 0) {
				dispatch(listCategories(''))
			}
			let elems = [];
			if (categories) {
				categories.map(item =>
					elems.push({ label: item.name, value: item._id }),
				);
				setOptions(elems)
			}
			if ((!product.name || product._id !== productId) && productId != "0") {
				dispatch(listProductDetails(productId))
			}
			else if (productId == "0") {
				setName('')
				setPrice(0)
				setImage('')
				setBrand('')
				setCountInStock(0)
				setCategory()
				setDescription('')
			}
			else {
				setName(product.name)
				setPrice(product.price)
				setImage(product.image)
				setBrand(product.brand)
				setCountInStock(product.countInStock)
				setSelectedCategory(product.category)
				setDescription(product.description)
			}
		}
	}, [dispatch, successUpdate, successCreate, history, categories, product, productId]) // Dependencies, on change they fire off useEffect)

	const uploadFileHandler = async (e) => {
		const file = e.target.files[0]

		const formData = new FormData()
		formData.append('image', file)
		setUploading(true)
		try {
			const config = {
				headers: {
					'Content-Type': 'multipart/form-data',
				},
			}

			// Making post request to upload the image
			const { data } = await axios.post('/api/upload', formData, config)

			setImage(data)
			setUploading(false)
		} catch (error) {
			console.error(error)
			setUploading(false)
		}
	}

	const setSelectedCategory = async (value) => {
		let cat = categories.filter(x => x._id == value)[0]
		if (cat) {
			const option = {
				label: cat.name,
				value: cat._id
			}
			setCategory(value)
			setSelectedOption(option)
		}
	}

	const submitHandler = (e) => {
		e.preventDefault()
		debugger
		if (productId != "0") {
			dispatch(
				updateProduct({
					_id: productId,
					name,
					price,
					image,
					brand,
					countInStock,
					category,
					description,
				})
			)
		}
		else {
			console.log(product)
			dispatch(
				createProduct({
					name,
					price,
					userInfo,
					image,
					brand,
					category,
					countInStock,
					description,
				})
			)
		}
	}

	return (
		<>
			<Link to='/admin/productlist' className='btn btn-light my-3'>
				Nazad
			</Link>
			<FormContainer>
				<h1>Podaci proizvoda</h1>
				{loadingUpdate && <Loader />}
				{errorUpdate && <Message variant='danger'>{errorUpdate}</Message>}
				{/* On error, display message/error
            When loading, display Loading... */}
				{loading && loadingCategories ? (
					<Loader />
				) : error ? (
					<Message variant='danger'>{error}</Message>
				) : (
					<Form onSubmit={submitHandler}>
						{/* Name */}
						<Form.Group controlId='email'>
							<Form.Label>Naziv</Form.Label>
							<Form.Control
								type='text'
								placeholder='Unesite naziv proizvoda'
								value={name}
								onChange={(e) => setName(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Price */}
						<Form.Group controlId='price'>
							<Form.Label>Cena</Form.Label>
							<Form.Control
								type='number'
								min='0'
								step='0.01'
								placeholder='Unesite cenu'
								value={price}
								onChange={(e) => setPrice(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Image */}
						<Form.Group controlId='image'>
							<Form.Label>Fotografija</Form.Label>
							<Form.Control
								type='text'
								placeholder='Putanja do fotografije proizvoda'
								value={image}
								onChange={(e) => setImage(e.target.value)}
							></Form.Control>
							{/* Image file */}
							<Form.File
								id='image-file'
								accept="image/*"
								label='Odaberite fotografiju'
								custom
								onChange={uploadFileHandler}
							></Form.File>
							{uploading && <Loader />}
						</Form.Group>
						{/* Brand */}
						<Form.Group controlId='brand'>
							<Form.Label>Brend</Form.Label>
							<Form.Control
								type='text'
								placeholder='Naziv brenda'
								value={brand}
								onChange={(e) => setBrand(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Count in Stock */}
						<Form.Group controlId='countInStock'>
							<Form.Label>Količina na zalihama</Form.Label>
							<Form.Control
								type='number'
								min='0'
								step='1'
								placeholder='Unesite količinu zaliha'
								value={countInStock}
								onChange={(e) => setCountInStock(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Category */}
						{/* <Form.Group controlId='category'>
							<Form.Label>Kategorija</Form.Label>
							<Form.Control
								type='text'
								placeholder='Unesite kategoriju'
								value={category}
								onChange={(e) => setCategory(e.target.value)}
							></Form.Control>
						</Form.Group> */}
						{options && <Form.Group controlId='category'>
							<Form.Label>Kategorija</Form.Label>
							<select
								placeholder='Odaberite kategoriju'
								options={options}
								value={category}
								onChange={(e) => setSelectedCategory(e.target.value)}
							>
								{options.map(opt => (
									<option key={opt.value} value={opt.value}>{opt.label}</option>
								))}
							</select>
						</Form.Group>}
						{/* Description */}
						<Form.Group controlId='description'>
							<Form.Label>Opis</Form.Label>
							<Form.Control
								type='text'
								placeholder='Opis proizvoda'
								value={description}
								onChange={(e) => setDescription(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Button */}
						<Button type='submit' variant='primary'>
							Snimanje
						</Button>
					</Form>
				)}
			</FormContainer>
		</>
	)
}

export default ProductScreen

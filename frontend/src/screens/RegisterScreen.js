import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button, Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import FormContainer from '../components/FormContainer'
import { register } from '../actions/userActions'
const referralCodes = require('referral-codes')

const RegisterScreen = ({ location, history }) => {
	const [name, setName] = useState('')
	const [address, setAddress] = useState('')
	const [city, setCity] = useState('')
	const [postcode, setPostcode] = useState('')
	const [phone, setPhone] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [referralCode, setReferralCode] = useState('')
	const [personalCode, setPersonalCode] = useState('')
	const [confirmPassword, setConfirmPassword] = useState('')
	const [message, setMessage] = useState(null)

	const dispatch = useDispatch()

	// useSelector is to grab what we want from the state
	const userRegister = useSelector((state) => state.userRegister)
	const { loading, error, userInfo } = userRegister

	const redirect = location.search ? location.search.split('=')[1] : '/'

	// make request here upon component load
	useEffect(
		() => {
			if (userInfo) {
				history.push(redirect)
			}
			else {
				const referral = referralCodes.generate({
					length: 8,
					count: 1,
					charset: '0123456789',
				})
				setPersonalCode(referral)
			}
		},
		[history, userInfo, redirect] // Dependencies, on change they fire off useEffect
	)

	const submitHandler = (e) => {
		e.preventDefault()
		// Check if passwords match
		if (password !== confirmPassword) {
			setMessage('Lozinke se ne poklapaju!')
		} else {
			// Dispatch register
			console.log(postcode)
			dispatch(register(name, address, city, postcode, phone, email, password, referralCode, personalCode))
		}
	}

	return (
		<FormContainer>
			<h1>Registracija</h1>
			{/* 
            On error, display message/error
            When loading, display Loading... */}
			{message && <Message variant='danger'>{message}</Message>}
			{error && <Message variant='danger'>{error}</Message>}
			{loading && <Loader />}
			<Form onSubmit={submitHandler}>
				{/* Personal code */}
				<Form.Group controlId='personalCode'>
					<Form.Label>Lični kod</Form.Label>
					<Form.Control
						type='text'
						placeholder='Vaš lični kod'
						disabled
						value={personalCode}
					></Form.Control>
				</Form.Group>
				{/* Name */}
				<Form.Group controlId='name'>
					<Form.Label>Ime i prezime</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite ime i prezime'
						value={name}
						onChange={(e) => setName(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Address */}
				<Form.Group controlId='address'>
					<Form.Label>Adresa</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite adresu'
						value={address}
						onChange={(e) => setAddress(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* City */}
				<Form.Group controlId='city'>
					<Form.Label>Grad</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite grad'
						value={city}
						onChange={(e) => setCity(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Postal code */}
				<Form.Group controlId='postcode'>
					<Form.Label>Poštanski broj</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite poštanski broj'
						value={postcode}
						onChange={(e) => setPostcode(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Phone */}
				<Form.Group controlId='phone'>
					<Form.Label>Broj telefona</Form.Label>
					<Form.Control
						type='text'
						placeholder='Unesite broj telefona'
						value={phone}
						onChange={(e) => setPhone(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Email */}
				<Form.Group controlId='email'>
					<Form.Label>Email adresa</Form.Label>
					<Form.Control
						type='email'
						placeholder='Unesite email adresu'
						value={email}
						onChange={(e) => setEmail(e.target.value)}
					></Form.Control>
				</Form.Group>

				{/* Referral */}
				<Form.Group controlId='referralCode'>
					<Form.Label>Referal kod</Form.Label>
					<Form.Control
						type='text'
						placeholder='Vaš referal'
						value={referralCode}
						onChange={(e) => setReferralCode(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Password */}
				<Form.Group controlId='password'>
					<Form.Label>Lozinka</Form.Label>
					<Form.Control
						type='password'
						placeholder='Unesite lozinku'
						value={password}
						onChange={(e) => setPassword(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Confirm Password */}
				<Form.Group controlId='confirmPassword'>
					<Form.Label>Potvrda lozinke</Form.Label>
					<Form.Control
						type='password'
						placeholder='Unesite potvrdu lozinke'
						value={confirmPassword}
						onChange={(e) => setConfirmPassword(e.target.value)}
					></Form.Control>
				</Form.Group>
				{/* Button */}
				<Button type='submit' variant='primary'>
					Registracija
				</Button>
			</Form>
			{/* Register */}
			<Row className='py-3'>
				<Col>
					Već ste registrovani?{' '}
					<Link to={redirect ? `/login?redirect=${redirect}` : '/login'}>
						Prijava
					</Link>
				</Col>
			</Row>
		</FormContainer>
	)
}

export default RegisterScreen

import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Form, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import FormContainer from '../components/FormContainer'
import { getUserDetails, updateUser } from '../actions/userActions'
import { USER_UPDATE_RESET } from '../constants/userConstants'

const UserScreen = ({ match, history }) => {
	const userId = match.params.id

	const [name, setName] = useState('')
	const [address, setAddress] = useState('')
	const [city, setCity] = useState('')
	const [postcode, setPostcode] = useState('')
	const [phone, setPhone] = useState('')
	const [email, setEmail] = useState('')
	const [isAdmin, setIsAdmin] = useState(false)

	const dispatch = useDispatch()

	// useSelector is to grab what we want from the state
	const userDetails = useSelector((state) => state.userDetails)
	const { loading, error, user } = userDetails

	const userUpdate = useSelector((state) => state.userUpdate)
	const {
		loading: loadingUpdate,
		error: errorUpdate,
		success: successUpdate,
	} = userUpdate

	// make request here upon component load
	useEffect(() => {
		if (successUpdate) {
			dispatch({ type: USER_UPDATE_RESET })
			history.push('/admin/userlist')
		} else {
			if (!user.name || user._id !== userId) {
				dispatch(getUserDetails(userId))
			} else {
				setName(user.name)
				setAddress(user.address)
				setCity(user.city)
				setPostcode(user.postcode)
				setPhone(user.phone)
				setEmail(user.email)
				setIsAdmin(user.isAdmin)
			}
		}
	}, [successUpdate, dispatch, history, user, userId]) // Dependencies, on change they fire off useEffect)

	const submitHandler = (e) => {
		e.preventDefault()
		dispatch(updateUser({ _id: userId, name, address, city, postcode, phone, email, isAdmin }))
	}

	return (
		<>
			<Link to='/admin/userlist' className='btn btn-light my-3'>
				Nazad
			</Link>
			<FormContainer>
				<h1>Promena podataka kupca</h1>
				{loadingUpdate && <Loader />}
				{errorUpdate && <Message variant='danger'>{errorUpdate}</Message>}
				{/* On error, display message/error
            When loading, display Loading... */}
				{loading ? (
					<Loader />
				) : error ? (
					<Message variant='danger'>{error}</Message>
				) : (
					<Form onSubmit={submitHandler}>
						{/* Name */}
						<Form.Group controlId='email'>
							<Form.Label>Ime i prezime</Form.Label>
							<Form.Control
								type='text'
								placeholder='Ime i prezime'
								value={name}
								onChange={(e) => setName(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Address */}
						<Form.Group controlId='address'>
							<Form.Label>Adresa</Form.Label>
							<Form.Control
								type='text'
								placeholder='Unesite adresu'
								value={address}
								onChange={(e) => setAddress(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* City */}
						<Form.Group controlId='city'>
							<Form.Label>Grad</Form.Label>
							<Form.Control
								type='text'
								placeholder='Unesite grad'
								value={city}
								onChange={(e) => setCity(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Postal code */}
						<Form.Group controlId='postcode'>
							<Form.Label>Poštanski broj</Form.Label>
							<Form.Control
								type='text'
								placeholder='Unesite poštanski broj'
								value={postcode}
								onChange={(e) => setPostcode(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Phone */}
						<Form.Group controlId='phone'>
							<Form.Label>Broj telefona</Form.Label>
							<Form.Control
								type='text'
								placeholder='Unesite broj telefona'
								value={phone}
								onChange={(e) => setPhone(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Email */}
						<Form.Group controlId='email'>
							<Form.Label>Email adresa</Form.Label>
							<Form.Control
								type='email'
								placeholder='Email'
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* isAdmin */}
						<Form.Group controlId='isadmin'>
							<Form.Check
								type='checkbox'
								label='Administrator'
								checked={isAdmin}
								onChange={(e) => setIsAdmin(e.target.checked)}
							></Form.Check>
						</Form.Group>
						{/* Button */}
						<Button type='submit' variant='primary'>
							Sačuvaj promene
						</Button>
					</Form>
				)}
			</FormContainer>
		</>
	)
}

export default UserScreen

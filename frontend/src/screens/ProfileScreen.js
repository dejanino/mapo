import React, { useState, useEffect } from 'react'
import { Table, Form, Button, Row, Col } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { getUserDetails, updateUserProfile } from '../actions/userActions'
import { listMyOrders } from '../actions/orderActions'
import { USER_UPDATE_PROFILE_RESET } from '../constants/userConstants'
import moment from 'moment'

const ProfileScreen = ({ history }) => {
	const [name, setName] = useState('')
	const [address, setAddress] = useState('')
	const [city, setCity] = useState('')
	const [postcode, setPostcode] = useState('')
	const [phone, setPhone] = useState('')
	const [email, setEmail] = useState('')
	const [personalCode, setPersonalCode] = useState('')
	const [referralPoints, setReferralPoints] = useState(0)
	const [password, setPassword] = useState('')
	const [confirmPassword, setConfirmPassword] = useState('')
	const [message, setMessage] = useState(null)

	const dispatch = useDispatch()

	// useSelector is to grab what we want from the state
	const userDetails = useSelector((state) => state.userDetails)
	const { loading, error, user } = userDetails

	// Make sure user is logged in to access this page
	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	// Get success value from userUpdateProfileReducer
	const userUpdateProfile = useSelector((state) => state.userUpdateProfile)
	const { success } = userUpdateProfile

	// To get my list of orders
	const orderListMy = useSelector((state) => state.orderListMy)
	const { loading: loadingOrders, error: errorOrders, orders } = orderListMy

	// make request here upon component load
	useEffect(
		() => {
			if (!userInfo) {
				history.push('/login')
			} else {
				if (!user || !user.name || success) {
					dispatch({ type: USER_UPDATE_PROFILE_RESET })
					dispatch(getUserDetails('profile'))
					dispatch(listMyOrders())
				} else {
					setName(user.name)
					setEmail(user.email)
					setAddress(user.address)
					setCity(user.city)
					setPostcode(user.postcode)
					setPhone(user.phone)
					setPersonalCode(user.personalCode)
					setReferralPoints(user.referralPoints || 0)
				}
			}
		},
		[dispatch, history, userInfo, user, success, orders] // Dependencies, on change they fire off useEffect
	)

	const submitHandler = (e) => {
		e.preventDefault()
		// Check if passwords match
		if (password !== confirmPassword) {
			setMessage('Lozinke se ne poklapaju')
		} else {
			dispatch(updateUserProfile({ id: user._id, name, address, city, postcode, phone, email, password }))
		}
	}

	return (
		<>
			<div className='contact-us profile'>
				<div className='container center'>
					<h2 className=''>Moj Profil</h2>
					<h3>Lični kod: {personalCode}</h3>
					<h3>Vaša trenutna zarada: {referralPoints} dinara</h3>
				</div>
			</div>
			<div className='container'>
				<Row>
					<Col md={3}>
						<p></p><p></p>
						{/* On error, display message/error
            When loading, display Loading... */}
						{message && <Message variant='danger'>{message}</Message>}
						{error && <Message variant='danger'>{error}</Message>}
						{success && <Message variant='success'>Profil je ažuriran</Message>}
						{loading && <Loader />}
						<Form onSubmit={submitHandler} className='push-to-right'>
							{/* Name */}
							<Form.Group controlId='email'>
								<Form.Label>Ime i prezime</Form.Label>
								<Form.Control
									type='name'
									placeholder='Unesite ime i prezime'
									value={name}
									onChange={(e) => setName(e.target.value)}
								></Form.Control>
							</Form.Group>
							{/* Address */}
							<Form.Group controlId='address'>
								<Form.Label>Adresa</Form.Label>
								<Form.Control
									type='text'
									placeholder='Unesite adresu'
									value={address}
									onChange={(e) => setAddress(e.target.value)}
								></Form.Control>
							</Form.Group>
							{/* City */}
							<Form.Group controlId='city'>
								<Form.Label>Grad</Form.Label>
								<Form.Control
									type='text'
									placeholder='Unesite grad'
									value={city}
									onChange={(e) => setCity(e.target.value)}
								></Form.Control>
							</Form.Group>
							{/* Postal code */}
							<Form.Group controlId='postcode'>
								<Form.Label>Poštanski broj</Form.Label>
								<Form.Control
									type='text'
									placeholder='Unesite poštanski broj'
									value={postcode}
									onChange={(e) => setPostcode(e.target.value)}
								></Form.Control>
							</Form.Group>
							{/* Phone */}
							<Form.Group controlId='phone'>
								<Form.Label>Broj telefona</Form.Label>
								<Form.Control
									type='text'
									placeholder='Unesite broj telefona'
									value={phone}
									onChange={(e) => setPhone(e.target.value)}
								></Form.Control>
							</Form.Group>
							{/* Email */}
							<Form.Group controlId='email'>
								<Form.Label>Email adresa</Form.Label>
								<Form.Control
									type='email'
									placeholder='Unesite email adresu'
									value={email}
									onChange={(e) => setEmail(e.target.value)}
								></Form.Control>
							</Form.Group>
							{/* Password */}
							<Form.Group controlId='password'>
								<Form.Label>Lozinka</Form.Label>
								<Form.Control
									type='password'
									placeholder='Unesite lozinku'
									value={password}
									onChange={(e) => setPassword(e.target.value)}
								></Form.Control>
							</Form.Group>
							{/* Confirm Password */}
							<Form.Group controlId='confirmPassword'>
								<Form.Label>Potvrda lozinke</Form.Label>
								<Form.Control
									type='password'
									placeholder='Unesite potvrdu lozinke'
									value={confirmPassword}
									onChange={(e) => setConfirmPassword(e.target.value)}
								></Form.Control>
							</Form.Group>
							{/* Button */}
							<Button type='submit' variant='primary'>
								Ažuriraj
							</Button>
						</Form>
					</Col>
					<Col md={9}>
						<p></p>
						<h2>Moje porudžbine</h2>
						{loadingOrders ? (
							<Loader />
						) : errorOrders ? (
							<Message variant='danger'>{errorOrders}</Message>
						) : (
							<Table bordered hover responsive className='table-sm'>
								<thead>
									<tr>
										<th>ID</th>
										<th>Datum</th>
										<th>Ukupno</th>
										<th>Plaćeno</th>
										<th>Isporučeno</th>
										<th>Informacije</th>
									</tr>
								</thead>
								<tbody>
									{orders.map((order) => (
										<tr key={order._id}>
											<td>{order.orderNo}</td>
											<td>{moment(order.createdAt).format('DD.MM.yyyy.')}</td>
											<td>{order.totalPrice} din</td>
											<td>
												{order.isPaid ? (
													moment(order.paidAt).format('DD.MM.yyyy.')
												) : (
													<i className='fas fa-times' style={{ color: 'red' }}></i>
												)}
											</td>
											<td>
												{order.isDelivered ? (
													moment(order.deliveredAt).format('DD.MM.yyyy.')
												) : (
													<i className='fas fa-times' style={{ color: 'red' }}></i>
												)}
											</td>
											<td>
												<LinkContainer to={`/order/${order._id}`}>
													<Button className='btn-sm' variant='light'>
														Detalji
													</Button>
												</LinkContainer>
											</td>
										</tr>
									))}
								</tbody>
							</Table>
						)}
					</Col>
				</Row>
			</div>
		</>
	)
}

export default ProfileScreen

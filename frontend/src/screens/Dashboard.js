import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { NavLink, Switch, Route } from "react-router-dom";
import ProductListScreen from './ProductListScreen';
import OrderListScreen from './OrderListScreen';
import UserListScreen from './UserListScreen';
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import Sidebar from 'react-bootstrap-sidebar-menu'
import '../sb-admin-2.css'
import UserEditScreen from './UserEditScreen'
import UserOrdersScreen from './UserOrdersScreen';
import ProductEditScreen from './ProductEditScreen'
import OrderScreen from './OrderScreen';
import OrderAdminScreen from './OrderAdminScreen';
import { logout } from '../actions/userActions'
import Index from './Index';
import CategoryListScreen from './admin/CategoryListScreen';
import CategoryEditScreen from './admin/CategoryEditScreen';

import { listThisMonthOrders } from '../actions/orderActions'
import { ORDER_LIST_MONTH_REQUEST } from '../constants/orderConstants';

const Dashboard = ({ history }) => {
	const dispatch = useDispatch();
	const isStarting = window.location.href.toLowerCase().endsWith('admin')
	
	useEffect(() => {
		if (isStarting) {
			history.push('/admin/index')
		}
		/* else if (total == 0) {
			dispatch({ type: ORDER_LIST_MONTH_REQUEST })
			dispatch(listThisMonthOrders('5'))
		} else {
			console.log("total je: " + total)
		}*/
	}, [isStarting, history, /*successThisMonthOrders, orderThisMonth*/])
	
	const logoutHandler = (e) => {
		e.preventDefault();
		dispatch(logout())
	}

	return (
		<>
			<div id="page-top">
				<div id="wrapper">
					<ul className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
						<a className="sidebar-brand d-flex align-items-center justify-content-center" href="/admin/index">
							<div className="sidebar-brand-icon rotate-n-15">
								<i className="fas fa-laugh-wink"></i>
							</div>
							<div className="sidebar-brand-text mx-3">MAPO Admin</div>
						</a>

						<hr className="sidebar-divider my-0" />

						<li className="nav-item">
							{/* <a className="nav-link" href="/admin/index">
								<i className="fas fa-fw fa-tachometer-alt"></i>
								<span>Dashboard</span></a> */}
							<LinkContainer className='nav-link' to='/admin/index'>
								<Nav.Link><i className="fas fa-fw fa-tachometer-alt"></i><span>Dashboard</span></Nav.Link>
							</LinkContainer>
						</li>

						<hr className="sidebar-divider" />

						<li className="nav-item">
							<LinkContainer className='nav-link' to='/admin/userlist'>
								<Nav.Link><i className="fas fa-fw fa-users"></i><span>Kupci</span></Nav.Link>
							</LinkContainer>
						</li>

						<li className="nav-item">
							<LinkContainer className='nav-link' to='/admin/orderlist'>
								<Nav.Link><i className="fas fa-fw fa-table"></i><span>Porudžbine</span></Nav.Link>
							</LinkContainer>
						</li>

						<li className="nav-item">
							<LinkContainer className='nav-link' to='/admin/categorylist'>
								<Nav.Link><i className="fas fa-fw fa-barcode"></i><span>Kategorije</span></Nav.Link>
							</LinkContainer>
						</li>

						<li className="nav-item">
							<LinkContainer className='nav-link' to='/admin/productlist'>
								<Nav.Link><i className="fas fa-fw fa-barcode"></i><span>Proizvodi</span></Nav.Link>
							</LinkContainer>
						</li>

						<hr className="sidebar-divider" />

						<li className="nav-item">
							<LinkContainer className='nav-link' to='/admin' onClick={(e) => { logoutHandler(e) }}>
								<Nav.Link><i className="fas fa-fw fa-barcode"></i><span>Odjava</span></Nav.Link>
							</LinkContainer>
						</li>
					</ul>

					<div id="content-wrapper" >
						<div id="content">
							<div className="container-fluid">
								<Switch>
									<Route exact path='/admin/index' component={Index} />
									<Route exact path='/admin/userlist' component={UserListScreen} />
									<Route path='/admin/orderlist' component={OrderListScreen} />
									<Route path='/admin/user/:id/edit' component={UserEditScreen} />
									<Route path='/admin/userorders/:id' component={UserOrdersScreen} />
									<Route exact path='/admin/productlist' component={ProductListScreen} />
									<Route path='/admin/product/:id/edit' component={ProductEditScreen} />									
									<Route
										path='/admin/productlist/:pageNumber'
										component={ProductListScreen}
										exact
									/>
									<Route exact path='/admin/categorylist' component={CategoryListScreen} />
									<Route path='/admin/category/:id/edit' component={CategoryEditScreen} />
									<Route
										path='/admin/order/:id'
										component={OrderAdminScreen}
										exact
									/>

								</Switch>

							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
}

export default Dashboard;
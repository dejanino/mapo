import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { Table, Form, Button, Row, Col } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import { getUserDetails, getUserDetailsByPersonalCode, updateUserProfile } from '../actions/userActions'
import { listMyOrders, filterOrders } from '../actions/orderActions'
import { USER_DETAILS_REQUEST, USER_UPDATE_PROFILE_RESET, USER_UPDATE_REQUEST } from '../constants/userConstants'
import { ORDER_LIST_REQUEST, ORDER_LIST_USER_REQUEST } from '../constants/orderConstants'
import moment from 'moment'

const UserOrdersScreen = ({ match, history }) => {
	const userId = match.params.id
	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [uId, setUId] = useState('')
	const [personalCode, setPersonalCode] = useState('')
	const [referralPoints, setReferralPoints] = useState(0)
	const [referralName, setReferralName] = useState('')

	const dispatch = useDispatch()

	// useSelector is to grab what we want from the state
	const userDetails = useSelector((state) => state.userDetails)
	const { loading, error, user } = userDetails

	// Make sure user is logged in to access this page
	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	// Get success value from userUpdateProfileReducer
	const userUpdateProfile = useSelector((state) => state.userUpdateProfile)
	const { success } = userUpdateProfile

	// Get success value from get user by referral code
	const referralDetails = useSelector((state) => state.referralDetails)
	const { loading: referralLoading, success: referralSuccess, referral } = referralDetails

	// To get my list of orders
	const orderListUser = useSelector((state) => state.orderListUser)
	const { loading: loadingOrders, error: errorOrders, orders } = orderListUser

	// make request here upon component load
	useEffect(
		() => {
			if (!userInfo) {
				history.push('/login')
			} else {
				if (!orderListUser || uId != userId) {
					//dispatch({ type: USER_DETAILS_REQUEST })
					dispatch(getUserDetails(userId))

					//dispatch({ type: ORDER_LIST_USER_REQUEST })
					dispatch(filterOrders(userId))
					setUId(userId)
				} else {
					//
					setName(user.name)
					setEmail(user.email)
					setPersonalCode(user.referralCode)
					setPersonalCode(user.personalCode)
					setReferralPoints(user.referralPoints || 0)
					setReferralName('')

					// debugger
					if (referral && referral._id && user.referralCode != "") {
						//dispatch(getUserDetailsByPersonalCode(user.referralCode))
					}
				}
			}
			if (user) {

			}
		},
		[dispatch, uId, userDetails, referralDetails, orderListUser] // dispatch, history, match,  Dependencies, on change they fire off useEffect
	)

	const getReferralName = () => {
		if (user.referralCode) {
			//dispatch(getUserDetailsByPersonalCode(user.referralCode))
		}
	}

	return (
		<>
			<Link to='/admin/userlist' className='btn btn-light my-3'>
				Nazad
			</Link>
			<Row>
				<Col md={3}>
					<h2>Detalji kupca</h2>
					{/* When loading, display Loading... */}
					{loading && <Loader />}
					<Form className='push-to-right'>
						{/* Name */}
						<Form.Group controlId='email'>
							<Form.Label>Ime i prezime</Form.Label>
							<Form.Control
								type='name'
								placeholder='Unesite ime i prezime'
								value={name}
								readOnly
								onChange={(e) => setName(e.target.value)}
							></Form.Control>
						</Form.Group>
						{/* Email */}
						<Form.Group controlId='email'>
							<Form.Label>Email adresa</Form.Label>
							<Form.Control
								type='email'
								placeholder='Unesite email adresu'
								value={email}
								readOnly
								onChange={(e) => setEmail(e.target.value)}
							></Form.Control>
						</Form.Group>
					</Form>
					<h3>Lični kod: {personalCode}</h3>
					{/* <h3>Referal: {referral.name}</h3> */}
					<h3>Trenutna zarada: {referralPoints} dinara</h3>
				</Col>
				<Col md={9}>
					<h2>Sve porudžbine</h2>
					{loadingOrders ? (
						<Loader />
					) : errorOrders ? (
						<Message variant='danger'>{errorOrders}</Message>
					) : (
						<Table bordered hover responsive className='table-sm'>
							<thead>
								<tr>
									<th>Broj</th>
									<th>Datum</th>
									<th>Ukupno</th>
									<th>Plaćeno</th>
									<th>Isporučeno</th>
									<th>Informacije</th>
								</tr>
							</thead>
							<tbody>
								{orders.map((order) => (
									<tr key={order._id}>
										<td>{order.orderNr}</td>
										<td>{moment(order.createdAt).format('DD.MM.yyyy.')}</td>
										<td>{order.totalPrice} din</td>
										<td>
											{order.isPaid ? (
												moment(order.paidAt).format('DD.MM.yyyy.')
											) : (
												<i className='fas fa-times' style={{ color: 'red' }}></i>
											)}
										</td>
										<td>
											{order.isDelivered ? (
												moment(order.deliveredAt).format('DD.MM.yyyy.')
											) : (
												<i className='fas fa-times' style={{ color: 'red' }}></i>
											)}
										</td>
										<td>
											<LinkContainer to={`/order/${order._id}`}>
												<Button className='btn-sm' variant='light'>
													Detalji
												</Button>
											</LinkContainer>
										</td>
									</tr>
								))}
							</tbody>
						</Table>
					)}
				</Col>
			</Row>
		</>
	)
}

export default UserOrdersScreen

import React, { useEffect } from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { Table, Button, Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import Paginate from '../components/Paginate'
import {
	listProducts,
	deleteProduct,
	createProduct,
} from '../actions/productActions'
import { PRODUCT_CREATE_REQUEST, PRODUCT_CREATE_RESET, PRODUCT_CREATE_SUCCESS } from '../constants/productConstants'
import { listCategories } from '../actions/categoryActions'
const ProductListScreen = ({ history, match }) => {
	const pageNumber = match.params.pageNumber || 1

	const dispatch = useDispatch()

	const categoryList = useSelector((state) => state.categoryList)
	const { categories } = categoryList

	const productList = useSelector((state) => state.productList)
	const { loading, error, products, page, pages } = productList

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	const productDelete = useSelector((state) => state.productDelete)
	const {
		loading: loadingDelete,
		error: errorDelete,
		success: successDelete,
	} = productDelete

	const productCreate = useSelector((state) => state.productCreate)
	const {
		loading: loadingCreate,
		error: errorCreate,
		success: successCreate,
		product: createdProduct,
	} = productCreate

	useEffect(() => {
		dispatch({ type: PRODUCT_CREATE_RESET })

		if (!userInfo || !userInfo.isAdmin) {
			history.push('/login')
		}
		if (successCreate && createdProduct != null) {
			history.push(`/admin/product/${createdProduct._id}/edit`)
		}
		else if (successCreate && createdProduct == null) {
			history.push(`/admin/product/${0}/edit`)
		} else {
			dispatch(listProducts('', pageNumber))
			dispatch(listCategories(''))
		}
	}, [
		dispatch,
		userInfo,
		history,
		successDelete,
		successCreate,
		createdProduct,
		pageNumber,
	])

	const deleteHandler = (id) => {
		if (window.confirm('Are you sure?')) {
			dispatch(deleteProduct(id))
		}
	}

	const createProductHandler = () => {
		//dispatch(createProduct())
		const data = null;
		dispatch({ type: PRODUCT_CREATE_SUCCESS, payload: data })
	}

	const getCategoryName = (id) => {
		if (categories && categories.length > 0) {
			const one = categories.find(x => x._id == id)
			console.log(one)
			return one.name
		}
		else {
			return ''
		}
	}

	return (
		<>
			<Row className='align-items-center'>
				<Col className='text-right'>
					<Button className='my-3' onClick={createProductHandler}>
						<span className='plus-sign-margin'>
							<i className='fas fa-plus'></i>
						</span>
						Novi proizvod
					</Button>
				</Col>
			</Row>
			{loadingCreate && <Loader />}
			{errorCreate && <Message variant='danger'>{errorCreate}</Message>}
			{loadingDelete && <Loader />}
			{errorDelete && <Message variant='danger'>{errorDelete}</Message>}
			{loading ? (
				<Loader />
			) : error ? (
				<Message variant='danger'>{error}</Message>
			) : (
				<>
					<Table bordered hover responsive className='table-sm'>
						<thead>
							<tr>
								{/* <th>ID</th> */}
								<th>Naziv</th>
								<th>Cena</th>
								<th>Kategorija</th>
								<th>Brend</th>
								<th>Izmena</th>
								<th>Brisanje</th>
							</tr>
						</thead>
						<tbody>
							{products.map((product) => (
								<tr key={product._id}>
									{/* <td>{product._id}</td> */}
									<td>{product.name}</td>
									<td>{product.price} din</td>
									<td>{getCategoryName(product.category)}</td>
									<td>{product.brand}</td>
									{/* Edit button */}
									<td>
										<LinkContainer to={`/admin/product/${product._id}/edit`}>
											<Button variant='light' className='btn-sm'>
												<i className='fas fa-edit'></i>
											</Button>
										</LinkContainer>
									</td>
									{/* Delete button */}
									<td>
										<Button
											variant='danger'
											className='btn-sm'
											onClick={() => deleteHandler(product._id)}
										>
											<i className='fas fa-trash'></i>
										</Button>
									</td>
								</tr>
							))}
						</tbody>
					</Table>
					<Paginate pages={pages} page={page} isAdmin={true} />
				</>
			)}
		</>
	)
}

export default ProductListScreen

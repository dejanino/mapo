import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Button, Row, Col, ListGroup, Image, Card } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import CheckoutSteps from '../components/CheckoutSteps'
import { createOrder, getLastOrderNo } from '../actions/orderActions'
import { USER_DETAILS_RESET } from '../constants/userConstants'
import { ORDER_CREATE_RESET } from '../constants/orderConstants'

const PlaceOrderScreen = ({ history }) => {
	const dispatch = useDispatch()
	const [orderNo, setOrderNo] = useState('')

	const cart = useSelector((state) => state.cart)

	// useSelector is to grab what we want from the state
	const userDetails = useSelector((state) => state.userDetails)
	const { loading: userLoading, error: userError, user } = userDetails

	// useSelector is to grab what we want from the state
	const orderLastNo = useSelector((state) => state.orderLastNo)
	const { loading, success: orderNoSuccess, msg } = orderLastNo

	// Calculate prices
	// Add two decimals to price if needed
	const addDecimals = (num) => {
		return (Math.round(num * 100) / 100).toFixed(2)
	}
	// Items price
	cart.itemsPrice = addDecimals(
		cart.cartItems.reduce((acc, item) => acc + item.price * item.qty, 0)
	)
	// Shipping price - over R1000 = R0
	// Else R150
	cart.shippingPrice = 0 // addDecimals(cart.itemsPrice > 1000 ? 0 : 150)
	// Tax price
	cart.taxPrice = 0 //addDecimals(Number((0.15 * cart.itemsPrice).toFixed(2)))
	// Total price
	cart.totalPrice = addDecimals(
		(
			Number(cart.itemsPrice) +
			Number(cart.shippingPrice) +
			Number(cart.taxPrice)
		).toFixed(2)
	)

	const orderCreate = useSelector((state) => state.orderCreate)
	const { order, success, error } = orderCreate

	useEffect(() => {
		dispatch(getLastOrderNo())
		if (success) {
			history.push(`/order/${order._id}`)
			dispatch({ type: USER_DETAILS_RESET })
			dispatch({ type: ORDER_CREATE_RESET })
		}
		// eslint-disable-next-line
	}, [history, success, orderNoSuccess]) // Dependencies, on change they fire off useEffect

	const generateOrderNo = () => {
		debugger
		const date = new Date()
		const year = date.getFullYear()
		const lastOrderNo = String(orderLastNo.orderNo)
		if (lastOrderNo && lastOrderNo != '') {
			const noParts = lastOrderNo.split('/')
			if (year == noParts[1]) {
				const nr = +noParts[0] + 1;
				return nr + '/' + year
			}
			else {
				return '1/' + year
			}
		}
		else {
			const year = date.getFullYear()
			return '1/' + year
		}
	}

	const placeOrderHandler = () => {
		dispatch(
			createOrder({
				orderItems: cart.cartItems,
				shippingAddress: cart.shippingAddress,
				paymentMethod: cart.paymentMethod,
				itemsPrice: cart.itemsPrice,
				shippingPrice: cart.shippingPrice,
				taxPrice: cart.taxPrice,
				totalPrice: cart.totalPrice,
				orderNo: generateOrderNo()
			})
		)
	}

	return (
		<>
			<div className='container'>
				<CheckoutSteps step1 step2 step3 step4 />
				<Row>
					{/* Left Steps Summary */}
					<Col md={8}>
						<ListGroup variant='flush'>
							<ListGroup.Item>
								<h2>Podaci za dostavu</h2>
								<p>
									<span className='push-to-right'>
										<strong>Adresa: </strong>
										{cart.shippingAddress.address}, {cart.shippingAddress.city}{' '}
										{cart.shippingAddress.postalCode},{' '}
										{cart.shippingAddress.phone}
									</span>
								</p>
							</ListGroup.Item>
							<ListGroup.Item>
								<h2>Način plaćanja</h2>
								<span className='push-to-right'>
									<strong>Metod: </strong>
									{cart.paymentMethod}
								</span>
							</ListGroup.Item>
							<ListGroup.Item>
								<h2>Proizvodi u korpi</h2>
								{cart.cartItems.length === 0 ? (
									<Message>Vaša korpa je prazna</Message>
								) : (
									<ListGroup variant='flush'>
										{cart.cartItems.map((item, index) => (
											<ListGroup.Item key={index}>
												<Row>
													<Col md={1}>
														<Image
															src={item.image}
															alt={item.name}
															fluid
															rounded
														/>
													</Col>
													<Col>
														<Link to={`/product/${item.product}`}>
															{item.name}
														</Link>
													</Col>
													<Col md={4}>
														{item.qty} x {item.price} din = {item.qty * item.price} din
													</Col>
												</Row>
											</ListGroup.Item>
										))}
									</ListGroup>
								)}
							</ListGroup.Item>
						</ListGroup>
					</Col>
					{/* Right Order Summary */}
					<Col md={4}>
						<Card>
							<ListGroup variant='flush'>
								<ListGroup.Item>
									<h2>Pregled porudžbine</h2>
								</ListGroup.Item>
								<ListGroup.Item className='push-to-right'>
									<Row>
										<Col>Proizvodi</Col>
										<Col>{cart.itemsPrice} din</Col>
									</Row>
								</ListGroup.Item>
								<ListGroup.Item className='push-to-right'>
									<Row>
										<Col>Poštarina</Col>
										<Col>{cart.shippingPrice} din</Col>
									</Row>
								</ListGroup.Item>
								{/* <ListGroup.Item className='push-to-right'>
								<Row>
									<Col>Tax</Col>
									<Col>R{cart.taxPrice}</Col>
								</Row>
							</ListGroup.Item> */}
								<ListGroup.Item className='push-to-right'>
									<Row>
										<Col>
											<strong>Ukupno</strong>
										</Col>
										<Col>
											<strong>{cart.totalPrice} din</strong>
										</Col>
									</Row>
								</ListGroup.Item>
								<ListGroup.Item>
									{error && <Message variant='danger'>{error}</Message>}
								</ListGroup.Item>
								<ListGroup.Item>
									<Button
										type='button'
										className='btn-block'
										disabled={cart.cartItems === 0}
										onClick={placeOrderHandler}
									>
										Pošaljite porudžbinu
									</Button>
								</ListGroup.Item>
							</ListGroup>
						</Card>
					</Col>
				</Row>
			</div>
		</>
	)
}

export default PlaceOrderScreen

import React, { useEffect, useState } from 'react'
import { LinkContainer } from 'react-router-bootstrap'
import { Table, Button, Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import Paginate from '../components/Paginate'
import { Navbar, Nav, Container, NavDropdown } from 'react-bootstrap'
import {
	listProducts,
	deleteProduct,
	createProduct,
} from '../actions/productActions'
import Product from '../components/Product'
import SearchBox from '../components/SearchBox'
import Meta from '../components/Meta'

const AllProductsScreen = ({ history, match }) => {
	const [doSearch, setDoSearch] = useState(false)
	const [query, setQuery] = useState('')
	const [category, setCategory] = useState('all')
	const pageNumber = match.params.pageNumber || 1

	const dispatch = useDispatch()

	const productList = useSelector((state) => state.productList)
	const { loading, error, products, page, pages } = productList

	const userLogin = useSelector((state) => state.userLogin)
	const { userInfo } = userLogin

	useEffect(() => {
		dispatch(listProducts(query, pageNumber))
	}, [
		dispatch,
		userInfo,
		history,
		pageNumber,
		doSearch
	])

	const categories = [
		"Sve proizvode",
		"Elektronske uređaje",
		"Kompjutersku opremu",
		"Kuhinjsku opremu"
	];

	const brands = ["Apple", "Samsung", "Google", "HTC"];

	const manufacturers = ["Sony", "Cannon", "Amazon", "Logitech"];

	function trigerSearch() {
		setDoSearch(!doSearch)
		//setQuery('');
	}

	function FilterMenuLeft() {
		return (
			<ul className="list-group list-group-flush rounded">
				<li className="list-group-item d-none d-lg-block">
					<h5 className="mt-1 mb-2">Prikaži</h5>
					<div className="d-flex flex-wrap my-2">
						{categories.map((v, i) => {
							return (
								<LinkContainer
									key={i}
									to="/products"
									className="btn btn-sm btn-outline-dark rounded-pill me-2 mb-2"
									replace
								>
									<Nav.Link>
										{v}
									</Nav.Link>
								</LinkContainer>
							);
						})}
					</div>
				</li>
				<li className="list-group-item">
					<h5 className="mt-1 mb-1">Brendovi</h5>
					<div className="d-flex flex-column">
						{brands.map((v, i) => {
							return (
								<div key={i} className="form-check">
									<input className="form-check-input" type="checkbox" />
									<label className="form-check-label" htmlFor="flexCheckDefault">
										{v}
									</label>
								</div>
							);
						})}
					</div>
				</li>
				<li className="list-group-item">
					<h5 className="mt-1 mb-1">Proizvođači</h5>
					<div className="d-flex flex-column">
						{manufacturers.map((v, i) => {
							return (
								<div key={i} className="form-check">
									<input className="form-check-input" type="checkbox" />
									<label className="form-check-label" htmlFor="flexCheckDefault">
										{v}
									</label>
								</div>
							);
						})}
					</div>
				</li>
				<li className="list-group-item">
					<div className="d-grid d-block mb-3">
						<button className="btn btn-dark">Primeni filter</button>
					</div>
				</li>
			</ul>
		);
	}

	return (
		<>
			<Meta title={'Proizvodi'} />
			<div className='contact-us products'>
				<div className='container center'>
					<h2 className=''>Proizvodi</h2>
				</div>
			</div>
			{loading ? (
				<Loader />
			) : error ? (
				<Message variant='danger'>{error}</Message>
			) : (
				<>
					<div className="container">
						<div className="h-scroller d-block d-lg-none">
							<nav className="nav h-underline">
								{categories.map((v, i) => {
									return (
										<div key={i} className="h-link me-2">
											<LinkContainer
												to="/products"
												className="btn btn-sm btn-outline-dark rounded-pill"
												replace
											>
												<Nav.Link>
													{v}
												</Nav.Link>
											</LinkContainer>
										</div>
									);
								})}
							</nav>
						</div>

						<div className="row mb-3 d-block d-lg-none">
							<div className="col-12">
								<div id="accordionFilter" className="accordion shadow-sm">
									<div className="accordion-item">
										<h2 className="accordion-header" id="headingOne">
											<button
												className="accordion-button fw-bold collapsed"
												type="button"
												data-bs-toggle="collapse"
												data-bs-target="#collapseFilter"
												aria-expanded="false"
												aria-controls="collapseFilter"
											>
												Pretraga proizvoda
											</button>
										</h2>
									</div>
									<div
										id="collapseFilter"
										className="accordion-collapse collapse"
										data-bs-parent="#accordionFilter"
									>
										<div className="accordion-body p-0">
											<FilterMenuLeft />
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className="row mb-4 mt-lg-3">
							<div className="d-none d-lg-block col-lg-3">
								<div className="border rounded shadow-sm">
									<FilterMenuLeft />
								</div>
							</div>
							<div className="col-lg-9">
								<div className="d-flex flex-column h-100">
									{/* <div className="row mb-3">
										<div className="col-lg-9 col-xl-5 offset-xl-4 d-flex flex-row">
											<div className="input-group">
												<input
													className="form-control"
													type="text"
													placeholder="Pretraga proizvoda..."
													aria-label="search input"
												/>
												<button className="btn btn-outline-dark">
													<i className='fas fa-search'></i>
												</button>
											</div>
										</div>
									</div> */}
									<Row>
										<div className="input-group">
											<input
												className="form-control"
												type="text"
												placeholder="Pretraga proizvoda..."
												aria-label="search input"
												onChange={(e) => setQuery(e.target.value)}
												value={query}
											/>
											<button className="btn btn-primary small" onClick={trigerSearch}>
												<i className='fas fa-search'></i>
											</button>
										</div>
									</Row>
									<Row>
										{products.map((product) => (
											<Col key={product._id} sm={12} md='6' lg={4} xl={4}>
												<Product product={product} />
											</Col>
										))}
									</Row>
								</div>
							</div>
						</div>
					</div>
					<Paginate pages={pages} page={page} isAdmin={true} />
				</>
			)}
		</>
	)
}

export default AllProductsScreen

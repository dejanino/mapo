import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col } from 'react-bootstrap'
import Product from '../components/Product'
import Message from '../components/Message'
import Loader from '../components/Loader'
import Paginate from '../components/Paginate'
import Meta from '../components/Meta'
import { listProducts } from '../actions/productActions'

const ProductByCategoryScreen = ({ history, match }) => {
    const category = match.params.id
    const keyword = match.params.keyword
	const pageNumber = match.params.pageNumber || 1

    console.log(match.params)

    const dispatch = useDispatch()

    // useSelector is to grab what we want from the state
	const productList = useSelector((state) => state.productList)
	const { loading, error, products, page, pages } = productList

    if (products && products.length > 0) {
        console.log(products[0].category.name)
        console.log("page " + page + " from " + pages)
    }

    // Make sure user is logged in
    const userLogin = useSelector((state) => state.userLogin)
    const { userInfo } = userLogin

    // make request here upon component load
    useEffect(
        () => {
            // Fire off action to get a list of products
            dispatch(listProducts(keyword, pageNumber, category))
        },
        [dispatch, match, pageNumber] // Dependencies, on change they fire off useEffect
    )

    return (
        <>
            <div className='container'>
                {/* Back button */}
                <Link className='btn btn-light my-3' to='/'>
                    Nazad
                </Link>
                {products && products.length > 0 && <h1>{products[0].category.name}</h1>}
                {/* When loading, display Loading...
            On error, display error
            Else display the product details */}
                {loading ? (
                    <Loader />
                ) : error ? (
                    <Message variant='danger'>{error}</Message>
                ) : (
                    <>
                        {products && products.length > 0 && <Meta title={products[0].category.name} />}
                        <Row>
                            {products.map((product) => (
                                <Col key={product._id} sm={12} md='6' lg={4} xl={3}>
                                    <Product product={product} />
                                </Col>
                            ))}
                        </Row>
                        <Paginate
                            pages={pages}
                            page={page}
                            keyword={keyword ? keyword : ''}
                            category={category}
                        />
                    </>
                )}
            </div>
        </>
    )
}

export default ProductByCategoryScreen

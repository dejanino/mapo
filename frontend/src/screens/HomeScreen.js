import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Row, Col } from 'react-bootstrap'
import Product from '../components/Product'
import Category from '../components/Category'
import Message from '../components/Message'
import Loader from '../components/Loader'
import Paginate from '../components/Paginate'
import ProductCarousel from '../components/ProductCarousel'
import Meta from '../components/Meta'
import { listProducts } from '../actions/productActions'
import { listCategories } from '../actions/categoryActions'

const HomeScreen = ({ match }) => {
	const keyword = match.params.keyword

	const pageNumber = match.params.pageNumber || 1

	const dispatch = useDispatch()

	// useSelector is to grab what we want from the state
	const productList = useSelector((state) => state.productList)
	const { loading, error, products, page, pages } = productList

	const categoryList = useSelector((state) => state.categoryList)
	const { loading: categoryListLoading, error: categoryListError, categories } = categoryList

	// make request here upon component load
	useEffect(
		() => {
			// Fire off action to get the products
			dispatch(listProducts(keyword, pageNumber))
			dispatch(listCategories(keyword))
		},
		[dispatch, keyword, pageNumber] // Dependencies, on change they fire off useEffect
	)
	return (
		<>
			<Meta />
			{!keyword ? (
				<ProductCarousel />
			) : (
				<Link className='btn btn-light' to='/'>
					Nazad
				</Link>
			)}
			<div className='container center'>
				<h2 className='title'>Najnoviji proizvodi</h2>
				{/* When loading, display Loading...
            On error, display error
            Else display the products */}
				{loading ? (
					<Loader />
				) : error ? (
					<Message variant='danger'>{error}</Message>
				) : (
					<>
						<Row>
							{products.map((product) => (
								<Col key={product._id} sm={12} md='6' lg={4} xl={3}>
									<Product product={product} />
								</Col>
							))}
						</Row>
						<Paginate
							pages={pages}
							page={page}
							keyword={keyword ? keyword : ''}
						/>
					</>
				)}
			</div>
			<div className='container center'>
				<h2 className='title'>Pregled po kategorijama</h2>
				{/* When loading, display Loading...
            On error, display error
            Else display the categories */}
				{categoryListLoading ? (
					<Loader />
				) : categoryListError ? (
					<Message variant='danger'>{categoryListError}</Message>
				) : (
					<>
						<Row>
							{categories.map((category) => (
								<Col key={category._id} sm={12} md='6' lg={4} xl={3}>
									<Category category={category} />
								</Col>
							))}
						</Row>
					</>
				)}
			</div>
		</>
	)
}

export default HomeScreen

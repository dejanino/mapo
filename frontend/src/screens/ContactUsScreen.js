import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Form, Button, Row, Col } from 'react-bootstrap'
import Message from '../components/Message'
import Loader from '../components/Loader'
import Meta from '../components/Meta'
import FormContainer from '../components/FormContainer'
import { sendEmail } from '../actions/emailActions'

const ContactUsScreen = ({ match }) => {
    const dispatch = useDispatch()

    const [name, setName] = useState('')
    const [phone, setPhone] = useState('')
    const [email, setEmail] = useState('')
    const [message, setMessage] = useState(null)
    const [userMessage, setUserMessage] = useState('')
    const [messageVariant, setMessageVariant] = useState('danger')
    const [visible, setVisible] = useState(false)

    // useSelector is to grab what we want from the state
    const emailSend = useSelector((state) => state.emailSend)
    const { loading, error, msg } = emailSend

    const createPdf = useSelector((state) => state.createPdf)
    const { loading: pdfLoading, error: pdfError, msg: pdfMsg } = createPdf

    // make request here upon component load
    useEffect(
        () => {
            if (msg == 'success') {
                setMessage('Uspešno poslata poruka. Kontakriraćemo Vas u najkraćem mogućem roku.')
                setMessageVariant('success')
                setVisibility()
                clearFields()
            }
            else if (msg == 'error') {
                setVisibility()
                setMessage('Neuspešno slanje. Probajte ponovo ili nas kontaktirajte putem telefona.')
                setMessageVariant('danger')
            }
        },
        [dispatch, msg] // Dependencies, on change they fire off useEffect
    )

    const setVisibility = () => {
        setVisible(true)
        window.setTimeout(() => {
            setVisible(false)
        }, 4000)
    }

    const clearFields = () => {
        setName('')
        setPhone('')
        setEmail('')
        setUserMessage('')
    }

    const submitHandler = (e) => {
        e.preventDefault()
        const body = {
            name: name,
            phone: phone,
            email: email,
            message: userMessage
        }
        dispatch(sendEmail(body))
    }

    return (
        <>
            <Meta title={'Kontaktirajte nas'} />
            <div className='contact-us'>
                <div className='container center'>
                    <h2 className=''>Kontaktirajete nas</h2>
                </div>
            </div>
            <div className='container top20'>
                <Link className='btn btn-light' to='/'>
                    Nazad
                </Link>

                <div className='container '>
                    {/* 
            On error, display message/error
        When loading, display Loading... */}
                    {message && visible && <Message visible={visible} variant={messageVariant}>{message}</Message>}
                    {error && <Message variant='danger'>{error}</Message>}
                    {loading && <Loader />}
                    <div className='row top20'>
                        <div className='col-lg-6'>
                            <p>Imate pitanja ili želite neki vid saradnje? Pišite nam.</p>
                            <Form onSubmit={submitHandler}>
                                {/* Name */}
                                <Form.Group controlId='name'>
                                    <Form.Label>Ime i prezime</Form.Label>
                                    <Form.Control
                                        type='text'
                                        required
                                        placeholder='Unesite ime i prezime'
                                        value={name}
                                        onChange={(e) => setName(e.target.value)}
                                    ></Form.Control>
                                </Form.Group>
                                {/* Phone */}
                                <Form.Group controlId='phone'>
                                    <Form.Label>Broj telefona</Form.Label>
                                    <Form.Control
                                        type='text'
                                        required
                                        placeholder='Unesite broj telefona'
                                        value={phone}
                                        onChange={(e) => setPhone(e.target.value)}
                                    ></Form.Control>
                                </Form.Group>
                                {/* Email */}
                                <Form.Group controlId='email'>
                                    <Form.Label>Email adresa</Form.Label>
                                    <Form.Control
                                        type='email'
                                        required
                                        placeholder='Unesite email adresu'
                                        value={email}
                                        onChange={(e) => setEmail(e.target.value)}
                                    ></Form.Control>
                                </Form.Group>
                                {/* Message */}
                                <Form.Group controlId='message'>
                                    <Form.Label>Poruka</Form.Label>
                                    <Form.Control
                                        type='text'
                                        as={'textarea'}
                                        rows={3}
                                        required
                                        placeholder='Unesite Vašu poruku'
                                        value={userMessage}
                                        onChange={(e) => setUserMessage(e.target.value)}
                                    ></Form.Control>
                                </Form.Group>
                                {/* Button */}
                                <Button type='submit' variant='primary'>
                                    Pošalji
                                </Button>
                            </Form>
                        </div>
                        <div className='col-lg-6'>
                            <p>Osnovni podaci firme:</p>
                            <p><strong>Marija Popović PR Radnja za proizvodnju ostale odeće MAPO</strong></p>
                            <p><strong>Matični broj: </strong>63940364</p>
                            <p><strong>Adresa: </strong>Radmile Savićević 27, Donja Vrežina</p>
                            <p><strong>Kontakt telefon: </strong><a href='tel:+38162447558'>062/447558</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ContactUsScreen

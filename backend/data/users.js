import bcrypt from 'bcryptjs'

const users = [
	// Admin user
	{
		name: 'Dejan Grozdanovic',
		email: 'dejanino@gmail.com',
		password: bcrypt.hashSync('123456', 10), //  10 = num rounds
		address: 'Maksima Gorkog 3',
		city: 'Nis',
		postcode: '18000',
		phone: '0692366138',
		isAdmin: true,
		personalCode: '123456',
		referralCode: '',
		referralPoints: 0
	},
	// Standard users
	{
		name: 'Melvin Doe',
		email: 'melvin@eg.com',
		password: bcrypt.hashSync('12345', 10), //  10 = num rounds
		address: 'Maksima Gorkog 3',
		city: 'Nis',
		postcode: '18000',
		phone: '0692366138',
		isAdmin: false,
		personalCode: '123457',
		referralCode: '123456',
		referralPoints: 0
	},
	
	{
		name: 'Belle Doe',
		email: 'belle@eg.com',
		password: bcrypt.hashSync('12345', 10), //  10 = num rounds
		address: 'Maksima Gorkog 3',
		city: 'Nis',
		postcode: '18000',
		phone: '0692366138',
		personalCode: '123458',
		referralCode: '123457',
		referralPoints: 0
	},
	{
		name: 'Mike Doe',
		email: 'mike@eg.com',
		password: bcrypt.hashSync('12345', 10), //  10 = num rounds
		address: 'Maksima Gorkog 3',
		city: 'Nis',
		postcode: '18000',
		phone: '0692366138',
		personalCode: '123459',
		referralCode: '123458',
		referralPoints: 0
	},
]

export default users

const categories = [
	{
		name: 'Dečija garderoba',
		image: '/images/kids-wear.jpg'
	},
	{
		name: 'Ženska garderoba',
		image: '/images/woman-wear.jpg'
	},
	{
		name: 'Muška garderoba',
		image: '/images/man-wear.jpg'
	},
	{
		name: 'Kućni tekstil',
		image: '/images/bath-and-kitchen-towels.jpg'
	}
]

export default categories

import path from 'path'
import express, { json } from 'express'
import dotenv from 'dotenv'
import colors from 'colors'
import morgan from 'morgan'
import { notFound, errorHandler } from './middleware/errorMiddleware.js'
import connectDB from './config/db.js'
import cors from 'cors'
import bodyParser from 'body-parser'
import fs from 'fs'

var router = express.Router();

import productRoutes from './routes/productRoutes.js'
import categoryRoutes from './routes/categoryRoutes.js'
import userRoutes from './routes/userRoutes.js'
import orderRoutes from './routes/orderRoutes.js'
import uploadRoutes from './routes/uploadRoutes.js'
import emailRoutes from './routes/emailRoutes.js'
import pdfRoutes from './routes/pdfRoutes.js'

import Product from './models/productModel.js'

//dotenv.config()

const mongoUrl = "mongodb+srv://dejanino:cKgxqkxWFKxCmZ8A@mapo.8qqsfqa.mongodb.net/webshop"

// Invoke connectDB


// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(bodyParser.json());
// Run morgan ONLY if in development mode
// morgan logs all activities
// if (process.env.NODE_ENV === 'development') {
	// 	app.use(morgan('combined'))
	// }
	
	
	
	// create a write stream (in append mode)
	const __dirname = path.resolve()
	dotenv.config({ path: path.resolve(__dirname, '../.env') })
	
	connectDB(mongoUrl)
	
	const app = express()
	app.use(express.json())
	app.use(cors({ origin: true }));

	router.get("/api/products/top", async function (req, res, next) {
		console.log(req)
    	const products = await Product.find({}).sort({ rating: -1 }).limit(3)

		res.json(products)
	});
	app.use("/", router);

app.use('/api/products', productRoutes)
app.use('/api/category', categoryRoutes)
app.use('/api/categories', categoryRoutes)
app.use('/api/users', userRoutes)
app.use('/api/orders', orderRoutes)
app.use('/api/upload', uploadRoutes)
app.use('/api/email', emailRoutes)
app.use('/api/pdf', pdfRoutes)

app.get('/api/config/paypal', (req, res) =>
	res.send(process.env.PAYPAL_CLIENT_ID)
)

// Make uploads folder static
app.use('/uploads', express.static(path.join(__dirname, '/uploads')))

	app.get('/api', (req, res) => {
		res.send('API is running...')
	})

	app.get('/', (req, res) => {
		res.send('API is running...')
	})

// app.get('*', (req, res) =>
// 	res.sendFile('index.html', { root })
// )


// Error middleware for 404
//app.use(notFound)

// Error handler middleware
app.use(errorHandler)

// Set port number
const PORT = process.env.PORT || 8000

console.log(PORT)
app.listen(
	PORT,
	console.log(
		`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
	)
)

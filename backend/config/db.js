import mongoose from 'mongoose'

const connectDB = async (mongoUrl) => {
	try {
		// Connect to Mongo
		console.log(process.env.MONGO_URI)
		console.log(mongoUrl)
		const conn = await mongoose.connect(process.env.MONGO_URI || mongoUrl, {
			useUnifiedTopology: true,
			useNewUrlParser: true,
			useCreateIndex: true,
		}) // New mongo url parser
		console.log(`MongoDB Connected: ${conn.connection.host}`.cyan.underline)
	} catch (error) {
		console.error(`Error: ${error.message}`.red.underline.bold)
		// exit with failure
		process.exit(1)
	}
}

export default connectDB

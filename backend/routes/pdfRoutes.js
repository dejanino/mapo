import express from 'express'
const router = express.Router()
import {
	createPdf
} from '../controllers/pdfController.js'

router.route('/').post(createPdf)

export default router

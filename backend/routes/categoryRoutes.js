import express from 'express'
const router = express.Router()
import {
	getCategories,
	getCategoryById,
	deleteCategory,
	createCategory,
	updateCategory
} from '../controllers/categoryController.js'
import {
	getProducts,
	getProductById,
	deleteProduct,
	createProduct,
	updateProduct,
	createProductReview,
	getTopProducts,
} from '../controllers/productController.js'
import { protect, admin } from '../middleware/authMiddleware.js'

router.route('/').get(getCategories).post(protect, admin, createCategory)
router.route('/:id').get(getProducts)
router.route('/:id/page/:pageNumber').get(getProducts)
// router
// 	.route('/:id')
// 	.get(getCategoryById)
// 	.delete(protect, admin, deleteCategory)
// 	.put(protect, admin, updateCategory)

export default router

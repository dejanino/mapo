import express from 'express'
const router = express.Router()
import {
	addOrderItems,
	getOrderById,
	updateOrderToPaid,
	updateOrderToManuallyPaid,
	getMyOrders,
	getUserOrders,
	getOrders,
	getMonthOrders,
	getYearOrders,
	getYearOrdersByProduct,
	updateOrderToDelivered,
	getLastOrderNo
} from '../controllers/orderController.js'
import { protect, admin } from '../middleware/authMiddleware.js'

router.route('/').post(protect, addOrderItems).get(protect, admin, getOrders)
router.route('/admin').post(protect, addOrderItems).get(protect, admin, getOrders)
router.route('/myorders').get(protect, getMyOrders)
router.route('/monthorders').get(getMonthOrders)
router.route('/lastOrderNo').get(getLastOrderNo)
router.route('/yearordersbyproduct').get(getYearOrdersByProduct)
router.route('/yearorders').get(getYearOrders)
router.route('/:id').get(protect, getOrderById)
router.route('/:id/pay').put(protect, updateOrderToManuallyPaid) //updateOrderToPaid
router.route('/:id/deliver').put(protect, admin, updateOrderToDelivered)
router.route('/:userid/userorders').get(protect, getUserOrders)

export default router

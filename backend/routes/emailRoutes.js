import express from 'express'
const router = express.Router()
import {
	sendEmail
} from '../controllers/emailController.js'
//import { protect, admin } from '../middleware/authMiddleware.js'

router.route('/').post(sendEmail)
router.route('/:body').post(sendEmail)

export default router

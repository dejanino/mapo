import mongoose from 'mongoose'

// Create Email Schema
const emailSchema = mongoose.Schema(
	{
		name: {
			type: String,
			required: true,
		},
		phone: {
			type: String,
			required: false,
		},
		mailto: {
			type: String,
			required: true,
		},
		message: {
			type: String,
			required: false,
		},
		attachment: {
			type: String,
			required: false,
		},
	},
	{
		timestamps: true,
	}
)

const Email = mongoose.model('Email', emailSchema)

export default Email

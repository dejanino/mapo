import mongoose from 'mongoose'

// Create Category Schema
const categorySchema = mongoose.Schema(
	{
		// product: {
		// 	type: mongoose.Schema.Types.ObjectId, // Gets id of User
		// 	required: true,
		// 	ref: 'Product', // Adds relationship between Product and Category
		// },
		name: {
			type: String,
			required: true,
		},
		image: {
			type: String,
			required: true,
		},
	},
	{
		timestamps: true,
	}
)

const Category = mongoose.model('Category', categorySchema)

export default Category

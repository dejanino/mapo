import asyncHandler from 'express-async-handler'
import Email from '../models/emailModel.js'

import nodemailer from 'nodemailer'
//const nodemailer = require('nodemailer');
import { google } from 'googleapis'
//const { google } = require('googleapis')
import creds from './config.json' assert { type: "json" }
//const creds = require('./config');

const oAuth2Client = new google.auth.OAuth2(creds.client_id, creds.client_secret, creds.redirect_uri)
oAuth2Client.setCredentials({ refresh_token: creds.refresh_token })

async function send(mail) {
	try {
		const accessToken = await oAuth2Client.getAccessToken()
		const transport = {
			service: 'gmail',
			auth: {
				type: 'OAuth2',
				user: creds.user,
				accessToken,
				clientId: creds.client_id,
				clientSecret: creds.client_secret,
				refreshToken: creds.refresh_token,
			}
		}
		const transporter = nodemailer.createTransport(transport)
		const result = transporter.sendMail(mail)
		return result
	} catch (error) {
		return error
	}
}

// @desc    Fetch email send
// @route   POST /api/email
// @access  Public
const sendEmail = asyncHandler(async (req, res) => {
	const { name, phone, email, message, attachment } = req.body

	const sender = "dejanino@gmail.com"

	let body = `<!DOCTYPE html>
		<html><head><title>MAPO prodavnica</title>
		</head><body><p>Ime: ${name}` +
		`</p><p>Telefon: ${phone}` + 
		`</p><p>Email: ${email}` + 
		`</p><p>Poruka: ${message}` + 
		`</p></body></html>`

	let mail = {
		from: sender,
		to: sender,
		subject: 'MAPO prodavnica',
		html: body
	}
	send(mail)
		.then(response => {
			res.json({ msg: 'success' })
		})
		.catch(error => {
			res.json({ msg: 'error' })
		})

	// let sent = true;

	// res.json({ sent })
})

export {
	sendEmail
}

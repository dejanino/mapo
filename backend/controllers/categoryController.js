import asyncHandler from 'express-async-handler'
import Category from '../models/categoryModel.js'

// @desc    Fetch all categories
// @route   GET /api/categories
// @access  Public
const getCategories = asyncHandler(async (req, res) => {
	const keyword = req.query.keyword
		? // TODO Fuzzy Search
		  {
				name: {
					$regex: req.query.keyword,
					$options: 'i', // case insensitive
				},
		  }
		: {}

	const categories = await Category.find({ ...keyword })

	res.json({ categories })
})
// @desc    Fetch single category
// @route   GET /api/categories/:id
// @access  Public
const getCategoryById = asyncHandler(async (req, res) => {
	const category = await Category.findById(req.params.id)
	// Check if category exists
	if (category) {
		res.json(category)
	} else {
		res.status(404)
		throw new Error('Category not found')
	}
})
// @desc    Delete single category
// @route   DELETE /api/categories/:id
// @access  Private/Admin
const deleteCategory = asyncHandler(async (req, res) => {
	const category = await Category.findById(req.params.id)
	// Check if category exists
	if (category) {
		await category.remove()
		res.json({ message: 'Category removed' })
	} else {
		res.status(404)
		throw new Error('Category not found')
	}
})
// @desc    Create a category
// @route   POST /api/categories
// @access  Private/Admin
const createCategory = asyncHandler(async (req, res) => {
	const {
		name,
		image
	} = req.body

	const category = new Category({
		name: name,
		image: image
	})

	const createdCategory = await category.save()
	res.status(201).json(createdCategory)
})
// @desc    Update a category
// @route   PUT /api/categories/:id
// @access  Private/Admin
const updateCategory = asyncHandler(async (req, res) => {
	const {
		name, 
		image
	} = req.body

	const category = await Category.findById(req.params.id)
	if (category) {
		category.name = name
		category.image = image || category.image

		const updatedCategory = await category.save()
		res.status(201).json(updatedCategory)
	} else {
		res.status(404)
		throw new Error('Category not found')
	}
})

export {
	getCategories,
	getCategoryById,
	deleteCategory,
	createCategory,
	updateCategory
}

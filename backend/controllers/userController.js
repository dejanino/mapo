"use strict"
import asyncHandler from 'express-async-handler'
import generateToken from '../utils/generateToken.js'
import User from '../models/userModel.js'

// @desc    Auth user & get token
// @route   POST /api/users/login
// @access  Public
const authUser = asyncHandler(async (req, res) => {
	const { email, password } = req.body

	const user = await User.findOne({ email })

	if (user && (await user.matchPassword(password))) {
		res.json({
			_id: user._id,
			name: user.name,
			email: user.email,
			isAdmin: user.isAdmin,
			referralCode: user.referralCode,
			personalCode: user.personalCode,
			referralPoints: user.referralPoints,
			token: generateToken(user._id),
		})
	} else {
		res.status(401)
		throw new Error('Invalid email or password')
	}
})
// @desc    Register a new user
// @route   POST /api/users
// @access  Public
const registerUser = asyncHandler(async (req, res) => {
	const { name, address, city, postcode, phone, email, password, referralCode, personalCode, referralPoints } = req.body

	const userExists = await User.findOne({ email })

	if (userExists) {
		res.status(400)
		throw new Error('User already exists')
	}

	const user = await User.create({
		name,
		address,
		city,
		postcode,
		phone,
		email,
		password,
		referralCode,
		personalCode,
		referralPoints
	})

	if (user) {
		res.status(201).json({
			_id: user._id,
			name: user.name,
			address: user.address,
			city: user.city,
			postcode: user.postcode,
			phone: user.phone,
			email: user.email,
			isAdmin: user.isAdmin,
			referralCode: user.referralCode,
			personalCode: user.personalCode,
			referralPoints: user.referralPoints,
			token: generateToken(user._id),
		})
	} else {
		res.status(400)
		throw new Error('Invalid user data')
	}
})

// @desc    Get user profile
// @route   GET /api/users/profile
// @access  Private
const getUserProfile = asyncHandler(async (req, res) => {
	const user = await User.findById(req.user._id)
	if (user) {
		res.json({
			_id: user._id,
			name: user.name,
			address: user.address,
			city: user.city,
			postcode: user.postcode,
			phone: user.phone,
			email: user.email,
			referralCode: user.referralCode,
			personalCode: user.personalCode,
			referralPoints: user.referralPoints,
			isAdmin: user.isAdmin,
		})
	} else {
		res.status(404)
		throw new Error('User Not Found')
	}
})
// @desc    Update user profile
// @route   PUT /api/users/profile
// @access  Private
const updateUserProfile = asyncHandler(async (req, res) => {
	const user = await User.findById(req.user._id)
	if (user) {
		user.name = req.body.name || user.name
		user.email = req.body.email || user.email
		user.address = req.body.address || user.address
		user.city = req.body.city || user.city
		user.postcode = req.body.postcode || user.postcode
		user.phone = req.body.phone || user.phone
		user.referralCode = req.body.referralCode || user.referralCode
		user.personalCode = req.body.personalCode || user.personalCode
		user.referralPoints = req.body.referralPoints || user.referralPoints
		if (req.body.password) {
			user.password = req.body.password
		}

		const updatedUser = await user.save()

		res.json({
			_id: updatedUser._id,
			name: updatedUser.name,
			email: updatedUser.email,
			address: updatedUser.address,
			city: updatedUser.city,
			postcode: updatedUser.postcode,
			phone: updatedUser.phone,
			isAdmin: updatedUser.isAdmin,
			referralCode: updatedUser.referralCode,
			personalCode: updatedUser.personalCode,
			referralPoints: updatedUser.referralPoints,
			token: generateToken(user._id),
		})
	} else {
		res.status(404)
		throw new Error('User Not Found')
	}
})
// @desc    Get all users
// @route   GET /api/users
// @access  Private/Admin
const getUsers = asyncHandler(async (req, res) => {

	const keyword = req.query.keyword
		? // TODO Fuzzy Search
		{
			name: {
				$regex: req.query.keyword,
				$options: 'i', // case insensitive
			},
		}
		: {}

	const users = await User.find({ ...keyword })
	res.json(users)
})
// @desc    Get top 5 users
// @route   GET /api/users/top5
// @access  Private/Admin
const getTop5Users = asyncHandler(async (req, res) => {

	console.log("top 5")

	const users = await User.find().sort({ referralPoints: -1 }).limit(5)
	res.json(users)
})
// @desc    Delete user
// @route   DELETE /api/users/:id
// @access  Private/Admin
const deleteUser = asyncHandler(async (req, res) => {
	const user = await User.findById(req.params.id)
	if (user) {
		await user.remove()
		res.json({ message: 'User removed' })
	} else {
		res.status(404)
		throw new Error('User not found')
	}
})
// @desc    Get user bu ID
// @route   GET /api/users/:id
// @access  Private/Admin
const getUserById = asyncHandler(async (req, res) => {
	const user = await User.findById(req.params.id).select('-password')
	if (user) {
		res.json(user)
	} else {
		res.status(404)
		throw new Error('User not found')
	}
})
// @desc    Get user by personal code
// @route   GET /api/users/:personalCode
// @access  Private
const getUserByPersonalCode = asyncHandler(async (req, res) => {
	const user = await User.findOne({ personalCode: req.params.code })
	console.log(user)
	if (user) {
		res.json({
			_id: user._id,
			name: user.name,
			address: user.address,
			city: user.city,
			postcode: user.postcode,
			phone: user.phone,
			email: user.email,
			referralCode: user.referralCode,
			personalCode: user.personalCode,
			referralPoints: user.referralPoints,
			isAdmin: user.isAdmin,
		})
	} else {
		res.status(404)
		throw new Error('User Not Found')
	}
})
// @desc    Update user
// @route   PUT /api/users/:id
// @access  Private/Admin
const updateUser = asyncHandler(async (req, res) => {
	const user = await User.findById(req.params.id)

	if (user) {
		user.name = req.body.name || user.name
		user.email = req.body.email || user.email
		user.address = req.body.address || user.address,
			user.city = req.body.city || user.city,
			user.postcode = req.body.postcode || user.postcode,
			user.phone = req.body.phone || user.phone,
			user.isAdmin = req.body.isAdmin || user.isAdmin
		user.referralCode = req.body.referralCode || user.referralCode
		user.personalCode = req.body.personalCode || user.personalCode
		user.referralPoints = req.body.referralPoints || user.referralPoints

		const updatedUser = await user.save()

		res.json({
			_id: updatedUser._id,
			name: updatedUser.name,
			address: updatedUser.address,
			city: updatedUser.city,
			postcode: updatedUser.postcode,
			phone: updatedUser.phone,
			email: updatedUser.email,
			referralCode: updatedUser.referralCode,
			personalCode: updatedUser.personalCode,
			referralPoints: updatedUser.referralPoints,
			isAdmin: updatedUser.isAdmin,
		})
	} else {
		res.status(404)
		throw new Error('User Not Found')
	}
})

// @desc    Update user
// @route   PUT /api/users/:id
// @access  Private/Admin
const updateMultipleUsers = asyncHandler(async (req, res) => {
	const users = req.body //await User.findById(req.params.id)

	users.forEach(async u => {
		const user = await User.findById(u._id)
		console.log(u.referralPoints)
		user.referralPoints = u.referralPoints
		try {
			const updatedUser = await user.save()
			console.log(updatedUser)
		}
		catch (error) {
			console.log(error)
		}
	});

	// if (users.length > 0) {
	// 	const user = await User.findById(users[0]._id)
	// 	update(user, 0)
	// }

})

const update = (user, points) => {
	user.referralPoints = points
	const updatedUser = user.save()
}

export {
	authUser,
	registerUser,
	getUserProfile,
	updateUserProfile,
	getUsers,
	deleteUser,
	getUserById,
	getUserByPersonalCode,
	updateUser,
	updateMultipleUsers,
	getTop5Users
}

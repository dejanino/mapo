import asyncHandler from 'express-async-handler'
import Order from '../models/orderModel.js'
import Product from '../models/productModel.js'

// @desc    Create new order
// @route   POST /api/orders
// @access  Private
const addOrderItems = asyncHandler(async (req, res) => {
	const {
		orderItems,
		shippingAddress,
		paymentMethod,
		itemsPrice,
		taxPrice,
		shippingPrice,
		totalPrice,
		orderNo
	} = req.body

	console.log(orderNo)

	// Check if there are order items
	if (orderItems && orderItems.length === 0) {
		res.status(400)
		throw new Error('No order items')
		return
	} else {
		const order = new Order({
			orderItems,
			user: req.user._id,
			shippingAddress,
			paymentMethod,
			itemsPrice,
			taxPrice,
			shippingPrice,
			totalPrice,
			orderNo
		})

		// Save new order to the database
		const createdOrder = await order.save()
		res.status(201).json(createdOrder)
	}
})
// @desc    Get order by id
// @route   GET /api/orders/:id
// @access  Private
const getOrderById = asyncHandler(async (req, res) => {
	const order = await Order.findById(req.params.id).populate(
		'user',
		'name email referralCode'
	)

	if (order) {
		res.json(order)
	} else {
		res.status(404)
		throw new Error('Order not found')
	}
})
// @desc    Update order to paid
// @route   PUT /api/orders/:id/pay
// @access  Private
const updateOrderToPaid = asyncHandler(async (req, res) => {
	const order = await Order.findById(req.params.id)

	if (order) {
		order.isPaid = true
		order.paidAt = Date.now()
		order.paymentResult = {
			id: req.body.id,
			status: req.body.status,
			update_time: req.body.update_time,
			email_address: req.body.payer.email_address,
		}

		const updatedOrder = await order.save()
		res.json(updatedOrder)
	} else {
		res.status(404)
		throw new Error('Order not found')
	}
})
const updateOrderToManuallyPaid = asyncHandler(async (req, res) => {
	const order = await Order.findById(req.params.id)
	
	if (order) {
		order.isPaid = true
		order.paidAt = Date.now()

		const updatedOrder = await order.save()
		res.json(updatedOrder)
	} else {
		res.status(404)
		throw new Error('Order not found')
	}
})
// @desc    Get logged in user orders
// @route   GET /api/orders/myorders
// @access  Private
const getMyOrders = asyncHandler(async (req, res) => {
	const orders = await Order.find({ user: req.user._id })
	res.json(orders)
})
// @desc    Get specific user orders
// @route   GET /api/orders/:id/userorders
// @access  Private
const getUserOrders = asyncHandler(async (req, res) => {
	const orders = await Order.find({ user: req.params.userid })
	res.json(orders)
})
// @desc    Get all orders
// @route   GET /api/orders
// @access  Private/Admin
const getOrders = asyncHandler(async (req, res) => {
	const orders = await Order.find({}).populate('user', 'id name')
	res.json(orders)
})

// @desc    Get all orders for the current month
// @route   GET /api/monthorders
// @access  Private/Admin
const getMonthOrders = asyncHandler(async (req, res) => {
	var date = new Date();
	const month = +req.query.month
	const year = date.getFullYear()

	var firstDay = new Date(year, (month - 1), 2);
	var lastDay = new Date(year, month, 1);

	console.log(firstDay)
	console.log(lastDay)

	let total = await Order.aggregate([{
		$match:
		{
			$and: [
				{
					"createdAt": {
						"$gte": firstDay
					}
				},
				{
					"createdAt": {
						"$lt": lastDay
					}
				}
			]
		}
	}, {
		$group:
			{ _id: null, sum: { $sum: "$totalPrice" }, total: { $count: {} } }
	}])
		.then(res => {
			res = {
				sum: res[0].sum,
				total: res[0].total
			}
			return res
		});
	res.json(total)
})

// @desc    Get latest order no
// @route   GET /api/order/lastOrderNo
// @access  Public
const getLastOrderNo = asyncHandler(async (req, res) => {
	const order = await Order.findOne({}, {}, { sort: { 'createdAt': -1 } })
	console.log(order)
	res.json(order.orderNo)
})

// @desc    Get all orders for the current month
// @route   GET /api/monthorders
// @access  Private/Admin
const getYearOrders = asyncHandler(async (req, res) => {
	const year = +req.query.year

	console.log(year)

	var firstDay = new Date(year - 1, 12, 2);
	var lastDay = new Date(year, 12, 1);

	let total = await Order.aggregate([
		{
			$match:
			{
				$and: [
					{
						"createdAt": {
							"$gte": firstDay
						}
					},
					{
						"createdAt": {
							"$lt": lastDay
						}
					}
				]
			}
		}, {
			$group:
			{
				_id: null,
				sum: { $sum: "$totalPrice" },
				total: { $count: {} }
			}
		}])
		.then(res => {
			res = {
				sum: res[0].sum,
				total: res[0].total
			}
			return res
		});

	res.json(total)
})

// @desc    Get all orders for the current year by product
// @route   GET /api/monthorders
// @access  Private/Admin
const getYearOrdersByProduct = asyncHandler(async (req, res) => {
	var date = new Date();
	const year = +req.query.year

	var firstDay = new Date(year - 1, 12, 2);
	var lastDay = new Date(year, 12, 1);

	let total = await Order.aggregate([
		{
			$match:
			{
				$and: [
					{
						"createdAt": {
							"$gte": firstDay
						}
					},
					{
						"createdAt": {
							"$lt": lastDay
						}
					}
				]
			}
		},
		{
			$unwind: "$orderItems"
		},
		{
			$group:
			{
				_id: "$orderItems._id",
				//name: "$orderItems.product.name",
				//totalUnpaid: "$orderItems.isPaid",
				count: { $count: {} }
			}
		}
	])
		.then(res => {
			console.log(res)
			res = {
				name: res[0].name,
				count: res[0].count
			}
			return res
		});

	console.log("res: " + total.name)
	res.json(total)
})

// @desc    Update order to delivered
// @route   PUT /api/orders/:id/deliver
// @access  Private/Admin
const updateOrderToDelivered = asyncHandler(async (req, res) => {
	const order = await Order.findById(req.params.id)

	if (order) {
		order.isDelivered = true
		order.deliveredAt = Date.now()

		const updatedOrder = await order.save()

		order.orderItems.forEach(async item => {
			await updateProductStock(item.product, item.qty)
		});

		res.json(updatedOrder)
	} else {
		res.status(404)
		throw new Error('Order not found')
	}
})

const updateProductStock = async (id, quantity) => {
	const product = await Product.findById(id)
	
	if (product) {
		const countInStock = product.countInStock - quantity
		console.log(countInStock)
		product.countInStock = countInStock

		const updatedProduct = await product.save()
		//res.status(201).json(updatedProduct)
	} else {
		res.status(404)
		throw new Error('Product not found')
	}
}

export {
	addOrderItems,
	getOrderById,
	updateOrderToPaid,
	updateOrderToManuallyPaid,
	getMyOrders,
	getUserOrders,
	getOrders,
	getMonthOrders,
	getYearOrders,
	getYearOrdersByProduct,
	updateOrderToDelivered,
	getLastOrderNo
}
